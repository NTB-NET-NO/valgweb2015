Imports System.Text
Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO

Module ModuleFunctions

    Public txtEncoding As Encoding = Encoding.GetEncoding(1252)

    Public deadline As Date = New Date(2015, 9, 14, 21, 0, 0)

    Public Function DoXstlTransform(ByVal xmlFile As String, ByVal xsltFile As String, Optional ByVal xsltArgs As System.Xml.Xsl.XsltArgumentList = Nothing, Optional ByVal xmlText As Boolean = False) As String
        Dim xmlDoc As New System.Xml.XmlDocument()
        Dim xslTransform As New System.Xml.Xsl.XslTransform()
        Dim s As New IO.MemoryStream()

        If xmlText Then
            xmlDoc.LoadXml(xmlFile)
        Else
            xmlDoc.Load(xmlFile)
        End If

        xslTransform.Load(xsltFile)
        xslTransform.Transform(xmlDoc, xsltArgs, s)

        s.Position = 0
        Dim sr As New IO.StreamReader(s, Encoding.GetEncoding(1252))
        Return sr.ReadToEnd
    End Function

    Public Function DoXsltTransform(ByVal xmlMainFile As String, ByVal xmlSubFiles As SortedList, ByVal xsltMainFile As String, ByVal xsltSubFile As String, ByVal Srv As System.Web.HttpServerUtility,
                                    Optional ByVal xsltArgs As System.Xml.Xsl.XsltArgumentList = Nothing,
                                    Optional ByVal xsltSubArgs As System.Xml.Xsl.XsltArgumentList = Nothing,
                                    Optional ByVal xmlText As Boolean = False) As String
        Dim xmlDocMain As New System.Xml.XmlDocument()
        Dim xmlDocSub As System.Xml.XmlDocument
        Dim xslTransform As New System.Xml.Xsl.XslTransform()
        Dim s As New IO.MemoryStream()
        Dim body As New IO.MemoryStream()

        If xsltSubArgs Is Nothing Then
            xsltSubArgs = xsltArgs
        End If

        'Last inn hovedfil
        If xmlText Then
            xmlDocMain.LoadXml(xmlMainFile)
        Else
            xmlDocMain.Load(xmlMainFile)
        End If

        'Last inn XSLT-fil
        xslTransform.Load(xsltMainFile)
        'Transformer xmldokument med XSLT angitt og skriv til s (MemoryStream)
        xslTransform.Transform(xmlDocMain, xsltArgs, s)


        For Each Key As String In xmlSubFiles.Keys
            Try
                xslTransform = New XslTransform()
                xslTransform.Load(xsltSubFile)
                xmlDocSub = New XmlDocument()

                xmlDocSub.Load(Srv.MapPath(xmlSubFiles(Key).ToString()))
                xslTransform.Transform(xmlDocSub, xsltSubArgs, body)

            Catch ex As Exception

            End Try
        Next

        'Sett filposisjon til index 0
        s.Position = 0
        body.Position = 0


        Dim sr As New IO.StreamReader(s, Encoding.GetEncoding(1252))
        Dim srBody As New IO.StreamReader(body, Encoding.GetEncoding(1252))
        Return sr.ReadToEnd().Replace("ERSTATT", srBody.ReadToEnd())

    End Function

    Public Function DoXsltTransform(ByVal xmlMainFile As String, ByVal xmlSubFiles As SortedList, ByVal xsltFile As String, ByVal Srv As System.Web.HttpServerUtility, Optional ByVal xsltArgs As System.Xml.Xsl.XsltArgumentList = Nothing, Optional ByVal xmlText As Boolean = False) As String
        Dim xmlDocMain As New System.Xml.XmlDocument()
        Dim xmlDocSub As System.Xml.XmlDocument
        Dim xslTransform As New System.Xml.Xsl.XslTransform()
        Dim s As New IO.MemoryStream()
        Dim body As New IO.MemoryStream()


        'Last inn hovedfil
        If xmlText Then
            xmlDocMain.LoadXml(xmlMainFile)
        Else
            xmlDocMain.Load(xmlMainFile)
        End If

        'Last inn XSLT-fil
        xslTransform.Load(xsltFile)
        xsltArgs.AddParam("aggregate", "", "MAIN")

        'Transformer xmldokument med XSLT angitt og skriv til s (MemoryStream)
        xslTransform.Transform(xmlDocMain, xsltArgs, s)


        xsltArgs.RemoveParam("aggregate", "")
        xsltArgs.AddParam("aggregate", "", "BODY")
        For Each Key As String In xmlSubFiles.Keys
            Try
                'xslTransform = New XslTransform()
                'xslTransform.Load(xsltSubFile)
                xmlDocSub = New XmlDocument()

                xmlDocSub.Load(Srv.MapPath(xmlSubFiles(Key).ToString()))
                xslTransform.Transform(xmlDocSub, xsltArgs, body)

            Catch ex As Exception

            End Try
        Next

        'Sett filposisjon til index 0
        s.Position = 0
        body.Position = 0


        Dim sr As New IO.StreamReader(s, Encoding.GetEncoding(1252))
        Dim srBody As New IO.StreamReader(body, Encoding.GetEncoding(1252))
        Return sr.ReadToEnd().Replace("ERSTATT", srBody.ReadToEnd())

    End Function

    Public Function Replace2ITPC(ByVal strInput As String) As String
        Dim sb As New StringBuilder(strInput)
        sb.Replace(Chr(13) & Chr(10), "")
        sb.Replace("{crlf}", Chr(13) & Chr(10))
        sb.Replace(Chr(196), "&#200;A")
        sb.Replace(Chr(195), Chr(196) & "A")
        sb.Replace(Chr(194), Chr(195) & "A")
        sb.Replace(Chr(193), Chr(194) & "A")
        sb.Replace(Chr(200), Chr(193) & "E")
        sb.Replace("&#200;", Chr(193))
        sb.Replace(Chr(225), Chr(194) & "a")
        sb.Replace(Chr(201), Chr(194) & "E")
        sb.Replace(Chr(233), Chr(194) & "e")
        sb.Replace(Chr(211), Chr(194) & "O")
        sb.Replace(Chr(243), Chr(194) & "o")
        sb.Replace(Chr(218), Chr(194) & "U")
        sb.Replace(Chr(250), Chr(194) & "u")
        sb.Replace(Chr(221), Chr(194) & "Y")
        sb.Replace(Chr(253), Chr(194) & "y")
        sb.Replace(Chr(192), Chr(193) & "A")
        sb.Replace(Chr(224), Chr(193) & "a")
        sb.Replace(Chr(232), Chr(193) & "e")
        sb.Replace(Chr(210), Chr(193) & "O")
        sb.Replace(Chr(242), Chr(193) & "o")
        sb.Replace(Chr(217), Chr(193) & "U")
        sb.Replace(Chr(249), Chr(193) & "u")
        sb.Replace(Chr(226), Chr(195) & "a")
        sb.Replace(Chr(202), Chr(195) & "E")
        sb.Replace(Chr(234), Chr(195) & "e")
        sb.Replace(Chr(238), Chr(195) & "i")
        sb.Replace(Chr(206), Chr(195) & "I")
        sb.Replace(Chr(212), Chr(195) & "O")
        sb.Replace(Chr(244), Chr(195) & "o")
        sb.Replace(Chr(219), Chr(195) & "U")
        sb.Replace(Chr(251), Chr(195) & "u")
        sb.Replace(Chr(227), Chr(196) & "a")
        sb.Replace(Chr(209), Chr(196) & "N")
        sb.Replace(Chr(241), Chr(196) & "n")
        sb.Replace(Chr(213), Chr(196) & "O")
        sb.Replace(Chr(245), Chr(196) & "o")
        sb.Replace(Chr(228), Chr(200) & "a")
        sb.Replace(Chr(207), Chr(200) & "I")
        sb.Replace(Chr(239), Chr(200) & "i")
        sb.Replace(Chr(214), Chr(200) & "O")
        sb.Replace(Chr(246), Chr(200) & "o")
        sb.Replace(Chr(220), Chr(200) & "U")
        sb.Replace(Chr(252), Chr(200) & "u")
        sb.Replace(Chr(255), Chr(200) & "y")
        sb.Replace(Chr(36), "&#164;")
        sb.Replace(Chr(164), Chr(36))
        sb.Replace("&#164;", Chr(36))
        sb.Replace(Chr(91), Chr(154))
        sb.Replace(Chr(93), Chr(159))
        sb.Replace(Chr(127), Chr(0))
        sb.Replace(Chr(146), Chr(39))
        sb.Replace(Chr(148), Chr(34))
        sb.Replace(Chr(150), Chr(45))
        sb.Replace(Chr(170), Chr(65))
        sb.Replace(Chr(176), Chr(0))
        sb.Replace(Chr(184), Chr(44))
        sb.Replace(Chr(199), Chr(67))
        sb.Replace(Chr(215), Chr(180))
        sb.Replace(Chr(229), Chr(166))
        sb.Replace(Chr(231), Chr(99))
        sb.Replace(Chr(247), Chr(184))
        sb.Replace(Chr(153), Chr(212))
        sb.Replace(Chr(168), Chr(200))
        sb.Replace(Chr(169), Chr(211))
        sb.Replace(Chr(174), Chr(210))
        sb.Replace(Chr(186), Chr(202))
        sb.Replace(Chr(197), Chr(168))
        sb.Replace(Chr(198), Chr(225))
        sb.Replace(Chr(208), Chr(226))
        sb.Replace(Chr(216), Chr(233))
        sb.Replace(Chr(223), Chr(251))
        sb.Replace(Chr(230), Chr(241))
        sb.Replace(Chr(240), Chr(243))
        sb.Replace(Chr(248), Chr(249))
        sb.Replace(Chr(254), Chr(252))
        sb.Replace("&#x99;", Chr(153))
        sb.Replace("&#x01;", Chr(1))
        sb.Replace("&#x02;", Chr(2))
        sb.Replace("&#x03;", Chr(3))
        sb.Replace("&#x04;", Chr(4))
        sb.Replace("&#x80;", Chr(128))
        sb.Replace("&#x90;", Chr(144))
        sb.Replace("&#x92;", Chr(146))
        sb.Replace("&#x94;", Chr(148))
        sb.Replace("&#x9E;", Chr(158))
        sb.Replace("&#x98;", Chr(152))
        sb.Replace("&#xB6;", Chr(182))
        sb.Replace("&#x0D;", Chr(13))
        sb.Replace("&#x0A;", Chr(10))
        sb.Replace("&#128;", Chr(128))
        sb.Replace("&#132;", Chr(132))
        sb.Replace("&#134;", Chr(134))
        sb.Replace("&#133;", Chr(133))
        sb.Replace("&#136;", Chr(136))
        sb.Replace("&#135;", Chr(135))
        sb.Replace("&#148;", Chr(148))
        sb.Replace("&#149;", Chr(149))
        sb.Replace("&#154;", Chr(154))
        sb.Replace("&#159;", Chr(159))
        sb.Replace("&#208;", Chr(208))
        Return sb.ToString
    End Function

    Public Sub WriteFile(ByRef strFileName As String, ByRef strContent As String, Optional ByVal append As Boolean = False)
        Dim w As New StreamWriter(strFileName, append, txtEncoding)
        w.WriteLine(strContent)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

End Module
