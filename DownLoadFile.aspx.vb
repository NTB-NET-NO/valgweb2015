Imports System.Text

Public Class DownLoadFile
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim fileType As String

        'fileType = Request("button")
        fileType = Request.QueryString("fileType")

        Dim xmlFile As String = Session("xmlFile")
        Dim xsltFile As String = Session("xsltFile")
        Dim slFiles As SortedList = Session("xmlFilesSorted")

        'Response.Write(fileType & ": " & xmlFile)
        xmlFile = Server.MapPath(xmlFile) '& "\" & xmlFile
        'Response.Write(fileType & ": " & xmlFile)

        'Dim xmlDoc As New System.Xml.XmlDocument()
        'Dim xslTransform As New System.Xml.Xsl.XslTransform()
        Dim xsltArgs As System.Xml.Xsl.XsltArgumentList = Session("argList")


        'xmlDoc.Load(xmlFile)
        'Dim streamOut As IO.Stream
        'Dim tw As IO.TextWriter

        Response.ContentEncoding = System.Text.Encoding.GetEncoding("iso-8859-1")
        Select Case fileType
            Case "html" '"Vis kun tabell"
                Response.ContentType = "text/html"

                'xslTransform.Load(Server.MapPath(xsltFile))

                'Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>" & vbCrLf)
                'Response.Write("<html>" & vbCrLf)

                'Response.Write("<head>" & vbCrLf)
                'Response.Write("<link href='Styles.css' type='text/css' rel='stylesheet'/>" & vbCrLf)
                'Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'/>" & vbCrLf)
                'ScriptFocus()
                'Response.Write("</head>" & vbCrLf)
                'Response.Write("<body>" & vbCrLf)

                If Not xsltArgs.GetParam("head", "") Is Nothing Then
                    xsltArgs.RemoveParam("head", "")
                End If
                If Not xsltArgs.GetParam("aggregate", "") Is Nothing Then
                    xsltArgs.RemoveParam("aggregate", "")
                End If

                Dim xsltSubArgs As System.Xml.Xsl.XsltArgumentList = New System.Xml.Xsl.XsltArgumentList()
                xsltArgs.AddParam("head", "", "yes")
                xsltSubArgs.AddParam("head", "", "no")
                xsltSubArgs.AddParam("Partikategori", "", xsltArgs.GetParam("Partikategori", ""))

                If slFiles Is Nothing Then
                    Response.Write(DoXstlTransform(xmlFile, Server.MapPath(xsltFile), xsltArgs))
                Else
                    xsltArgs.AddParam("aggregate", "", "yes")
                    xsltSubArgs.AddParam("aggregate", "", "no")
                    Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath(xsltFile), Server.MapPath(xsltFile), Server, xsltArgs, xsltSubArgs))
                End If

                'Response.Write("</body>" & vbCrLf)
                'Response.Write("</html>" & vbCrLf)
            Case "xml" '"Last ned XML"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".xml"
                Response.ContentType = "text/xml"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")
                Dim xmlDoc As New System.Xml.XmlDocument()
                xmlDoc.Load(xmlFile)
                Response.Write(xmlDoc.InnerXml)
            Case "ind" '"Last ned InDesign"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".txt"
                Response.ContentType = "text/plain"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")

                If slFiles Is Nothing Then
                    Response.Write(DoXstlTransform(xmlFile, Server.MapPath("xslt\stortingsvalg-InDesign.xsl"), xsltArgs))
                Else
                    Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath("xslt\stortingsvalg-InDesign.xsl"), Server, xsltArgs))
                End If

            Case "xtg" '"Last ned XTG"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".txt"
                Response.ContentType = "text/plain"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")

                If slFiles Is Nothing Then
                    Response.Write(DoXstlTransform(xmlFile, Server.MapPath("xslt/stortingsvalg-XTG.xsl"), xsltArgs))
                Else
                    Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath("xslt\stortingsvalg-XTG.xsl"), Server, xsltArgs))
                    'Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath("xslt\Valg-XTG.xsl"), Server.MapPath("xslt\stortingsvalg-XTGBody.xsl"), Server, xsltArgs))
                    'Session("xmlFilesSorted") = Nothing
                End If

            Case "xtgbt" '"Last ned XTG for Bergens Tidene"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".xtg"
                Response.ContentType = "text/plain"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")

                If slFiles Is Nothing Then
                    Response.Write(DoXstlTransform(xmlFile, Server.MapPath("xslt/stortingsvalg-XTG-bt.xsl"), xsltArgs))
                Else
                    Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath("xslt/stortingsvalg-XTG-bt.xsl"), Server, xsltArgs))
                    'Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath("xslt\stortingsvalg-XTG-bt-Main.xsl"), Server.MapPath("xslt\stortingsvalg-XTG-bt-Body.xsl"), Server, xsltArgs))
                    'Session("xmlFilesSorted") = Nothing
                End If
            Case "iptc" '"Last ned IPTC"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".txt"
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("Windows-1252")
                Response.ContentType = "text/html"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")

                Dim xsltArgsIptc As System.Xml.Xsl.XsltArgumentList = Session("argList")
                xsltArgsIptc.AddParam("enable-fip-header", "", "no")

                Dim strTemp2 As String = DoXstlTransform(xmlFile, Server.MapPath("xslt/Valg-IPTC.xsl"), xsltArgsIptc)
                Dim strTemp3 As String = Replace2ITPC(strTemp2)

                WriteFile(Server.MapPath("writeaccess\" & outFile), strTemp3)
                Response.Write(strTemp3)

            Case "xhtml" '"Last ned XHTML"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".xml"
                Response.ContentType = "text/xml"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")

                Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>" & vbCrLf)

                If Not xsltArgs.GetParam("head", "") Is Nothing Then
                    xsltArgs.RemoveParam("head", "")
                End If
                If Not xsltArgs.GetParam("aggregate", "") Is Nothing Then
                    xsltArgs.RemoveParam("aggregate", "")
                End If

                Dim xsltSubArgs As System.Xml.Xsl.XsltArgumentList = New System.Xml.Xsl.XsltArgumentList()
                xsltArgs.AddParam("head", "", "yes")
                xsltSubArgs.AddParam("head", "", "no")
                xsltSubArgs.AddParam("Partikategori", "", xsltArgs.GetParam("Partikategori", ""))

                If slFiles Is Nothing Then
                    Response.Write(DoXstlTransform(xmlFile, Server.MapPath(xsltFile), xsltArgs))
                Else
                    xsltArgs.AddParam("aggregate", "", "yes")
                    xsltSubArgs.AddParam("aggregate", "", "no")
                    Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath(xsltFile), Server.MapPath(xsltFile), Server, xsltArgs, xsltSubArgs))
                End If

            Case "xhtml_old" '"Last ned XHTML"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".xml"
                Response.ContentType = "text/xml"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")
                'xslTransform.Load(Server.MapPath(xsltFile))

                Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>" & vbCrLf)
                Response.Write("<html>" & vbCrLf)

                Response.Write("<head>" & vbCrLf)
                Response.Write("<link href='Styles.css' type='text/css' rel='stylesheet'/>" & vbCrLf)

                Dim fileName As String = IO.Path.GetFileNameWithoutExtension(xmlFile)
                'Dim rapport
                Response.Write("<meta name='rapportnavn' content='" & fileName & "' />" & vbCrLf)
                Response.Write("<meta name='fylkenr' content='' />" & vbCrLf)
                Response.Write("<meta name='fylkenavn' content='' />" & vbCrLf)
                Response.Write("<meta name='kommunenr' content='' />" & vbCrLf)
                Response.Write("<meta name='kommunenavn' content='' />" & vbCrLf)
                Response.Write("<meta name='kretsnr' content='' />" & vbCrLf)
                Response.Write("<meta name='kretsnavn' content='' />" & vbCrLf)

                Response.Write("</head>" & vbCrLf)

                Response.Write("<body>" & vbCrLf)
                'xslTransform.Transform(xmlDoc, xsltArgs, Response.OutputStream)
                Response.Write(DoXstlTransform(xmlFile, Server.MapPath(xsltFile), xsltArgs))
                Response.Write("</body>" & vbCrLf)
                Response.Write("</html>" & vbCrLf)

            Case "nitf" '"Last ned NITF"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".xml"
                Response.ContentType = "text/xml"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")

                'Response.Write(DoXstlTransform(xmlFile, Server.MapPath("xslt\Valg2NITF.xsl"), xsltArgs))
                'Response.Write(DoXstlTransform(xmlFile, Server.MapPath("xslt\stortingsvalg-NITF.xsl"), xsltArgs))

                If slFiles Is Nothing Then
                    Response.Write(DoXstlTransform(xmlFile, Server.MapPath("xslt\fylkekommune-NITF.xsl"), xsltArgs))
                Else
                    Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath("xslt\fylkekommune-NITFMain.xsl"), _
                        Server.MapPath("xslt\fylkekommune-NITFBody.xsl"), Server, xsltArgs))
                    'Session("xmlFilesSorted") = Nothing
                End If


        End Select

    End Sub

    Sub ScriptFocus()
        Response.Write("<script language='JavaScript'>" & vbCrLf)
        Response.Write("<!--" & vbCrLf)
        Response.Write("this.focus();" & vbCrLf)
        Response.Write("-->" & vbCrLf)
        Response.Write("</script>" & vbCrLf)
    End Sub

End Class
