﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="login.aspx.vb" ValidateRequest="false" Inherits="ValgWeb2015.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Valget 2015 - Logg på kommentator-løsning</title>
  <style>
	body, html { font-family: verdana; font-size: small; }
 </style>
</head>
<body>
    <form id="form1" runat="server">
    <h1 style="COLOR: #003366;">Valget 2015</h1>

        <table width="450" cellpadding="0"  border="0" cellspacing="2">
            <tr>
                <td colspan="2">
                <p>Her finner du kontinuerlig oppdaterte valgresultater tilrettelagt for nedlasting i NTBs standardformat. </p>
                <p>For å benytte løsningen må du være kunde på NTBs nyhetstjeneste. Brukernavn distribueres til NTBs kunder, dessuten får du dette ved å kontakt NTB.</p>
                <p>Hvis du er usikker på om du kan benytte tjenesten, ta kontakt med NTBs markedsavdeling: <a href=mailto:marked@ntb.no">marked@ntb.no</a> eller ring 22 03 44 00.</p> 
                <p>Merk at på valgdagen mandag 14. september er løsningen sperret fram til valglokalene stenger kl 21.00.</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p><asp:Label ID="lblStatus" runat="server" Text=""></asp:Label></p>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserName" runat="server" Text="Brukernavn"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPassword" runat="server" Text="Passord"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:CheckBox ID="RememberMe" runat="server" Text="Husk meg på denne maskinen" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td><br />
                    <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Logg på" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
