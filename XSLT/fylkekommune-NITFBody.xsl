<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- Valget IPTC format -->

  <xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="yes" standalone="no"/>
  <xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>
  <xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>
  <xsl:decimal-format name="pros" decimal-separator=","/>
  <xsl:strip-space elements="*"/>

  <xsl:param name="distribusjon">ALL</xsl:param>
  <xsl:variable name="fylke">
    <xsl:value-of select="/respons/rapport/data[@navn='FylkeNavn']"/>
  </xsl:variable>

  <xsl:variable name="kommune">
    <xsl:value-of select="/respons/rapport/data[@navn='KommNavn']"/>
  </xsl:variable>

  <xsl:variable name="kommunenr">
    <xsl:value-of select="/respons/rapport/data[@navn='KommNr']"/>
  </xsl:variable>

  <xsl:variable name="bydel">
    <xsl:value-of select="/respons/rapport/data[@navn='BydelNavn']"/>
  </xsl:variable>

  <xsl:variable name="bydelnr">
    <xsl:value-of select="/respons/rapport/data[@navn='BydelNr']"/>
  </xsl:variable>
  <xsl:variable name="krets">
    <xsl:value-of select="/respons/rapport/data[@navn='KretsNavn']"/>
  </xsl:variable>

  <xsl:variable name="kretsnr">
    <xsl:value-of select="/respons/rapport/data[@navn='KretsNr']"/>
  </xsl:variable>

  <!-- Main template: -->
  <xsl:template match="respons">
    <!--
	<xsl:text>@HEADER:APO;INN;;;;</xsl:text>
-->
    
       
        <!--
		<xsl:call-template name="body"/>
		<table border="1" cellspacing="0" cellpadding="5">
			<xsl:call-template name="TableHeader"/>
			<xsl:apply-templates select="rapport"/>
			<xsl:call-template name="sum"/>
		</table>
		<xsl:apply-templates select="rapport" mode="status"/>
		-->

        <!-- Topptekster -->
        <xsl:choose>
          <xsl:when test="$rapport='4'">
            <p>
              <hl2>
              <xsl:choose>
                <xsl:when test="$rapportnavn='F04' and (rapport/data[@navn='TotAntKomm'] - rapport/data[@navn='AntKommVtstOpptalt'] - rapport/data[@navn='AntKommAltOpptalt'])!=0">
                  <xsl:text>Fylkesoversikt, prognose, kl. </xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>Fylkesoversikt, kl. </xsl:text>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:value-of select="$tid"/>
              </hl2>
            </p>
            <p>
              <hl2>
                <xsl:value-of select="$fylke"/>
              </hl2>
            </p>
          </xsl:when>
          <xsl:when test="$rapport='5'">
            <p>
              <hl2>
                <xsl:choose>
                  <xsl:when test="$rapportnavn='F05' and (rapport/data[@navn='TotAntKomm'] - rapport/data[@navn='AntKommVtstOpptalt'] - rapport/data[@navn='AntKommAltOpptalt'])!=0">
                    <xsl:text>Landsoversikt, prognose, kl. </xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>Landsoversikt, kl. </xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select="$tid"/>
              </hl2>
            </p>
          </xsl:when>
          <xsl:when test="$rapportnavn='K02'">
            <p>
              <hl2>
              <xsl:value-of select="$fylke"/>
              </hl2>
            </p>
            <p>
              <hl2>
                <xsl:value-of select="$kommune"/>
              </hl2>
            </p>
          </xsl:when>
          <xsl:when test="$rapport='2'">
            <p>
              <hl2>
              <xsl:value-of select="$fylke"/>
              </hl2>
            </p>
            <p>
              <hl2>
              <xsl:value-of select="$kommune"/>
              </hl2>
            </p>
          </xsl:when>
          <xsl:when test="$rapport='3'">
            <p>
              <hl2>
                <xsl:value-of select="$kommune"/>
              </hl2>
            </p>
            <p>
              <hl2>
                <xsl:value-of select="$krets"/>
              </hl2>
            </p>
          </xsl:when>
          <xsl:when test="$rapport='8'">
            <p lede="true" class="lead">
              <xsl:value-of select="$bydel"/>
            </p>
          </xsl:when>
          <xsl:otherwise>
          </xsl:otherwise>
        </xsl:choose>

        <xsl:if test="$tabelltype='ov' and $rapportnavn!='K02'">
          <xsl:apply-templates select="rapport" mode="status"/>
        </xsl:if>

        <!-- Valg Tabellen -->
        <table>
          <xsl:call-template name="table_header"/>
          <xsl:apply-templates select="rapport"/>
          <xsl:if test="$tabelltype='ov' and $rapportnavn!='K02' and $rapportnavn!='F04'">
            <xsl:call-template name="sumandre"/>
          </xsl:if>
          <xsl:call-template name="sum"/>
        </table>

        <xsl:if test="$tabelltype='se'">
          <xsl:apply-templates select="rapport" mode="status-bunn"/>
        </xsl:if>

        <xsl:if test="$rapportnavn='K02'">
          <xsl:apply-templates select="rapport" mode="status-bunn"/>
        </xsl:if>

   
  </xsl:template>

  <!-- Template for tabellen -->
  <xsl:template match="rapport">
    <xsl:choose>
      <!-- K02 F04 -->
      <xsl:when test="$rapportnavn='K02' or $rapportnavn='F04'">
        <xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] != 0]" mode="K02"/>
      </xsl:when>
      <!-- F05 -->
      <xsl:when test="$rapportnavn='F05'">
        <xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] &lt; 3 and data[@navn='Partikategori'] != 0]" mode="F05"/>
      </xsl:when>
      <!-- F02 F03 K03 -->
      <xsl:when test="$tabelltype='se'">
        <xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] != 0]" mode="se"/>
      </xsl:when>
      <!-- K05 K04 -->
      <xsl:otherwise>
        <xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] &lt; 3 and data[@navn='Partikategori'] != 0]" mode="K05"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Template for kolonner i tabellen K02-->
  <xsl:template match="liste" mode="K02">
    <!-- K02 F04 -->
    <tr>
      <td>
        <xsl:value-of select="data[@navn='Partikode']"/>
      </td>
      <td align="right">
        <xsl:if test="data[@navn='AntStemmer']!=''">
          <xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
        </xsl:if>
      </td>
      <xsl:choose>
        <xsl:when test="$rapportnavn='K02'">
          <td align="right">
            <xsl:value-of select="data[@navn='ProSt']"/>
          </td>
          <td align="right">
            <xsl:value-of select="data[@navn='DiffPropFKsv']"/>
          </td>
          <td align="right">
            <xsl:value-of select="data[@navn='DiffPropFStv']"/>
          </td>
          <td align="right">
            <xsl:value-of select="data[@navn='ProgAntMndt']"/>
            <xsl:if test="data[@navn='DiffProgAntMndt']!=''">
              <xsl:text> (</xsl:text>
              <xsl:value-of select="data[@navn='DiffProgAntMndt']"/>
              <xsl:text>)</xsl:text>
            </xsl:if>
          </td>
        </xsl:when>
        <xsl:otherwise>
          <!-- F04 -->
          <td align="right">
            <xsl:value-of select="data[@navn='ProgProSt']"/>
          </td>
          <td align="right">
            <xsl:value-of select="data[@navn='DiffProgPropFFtv']"/>
          </td>
          <td align="right">
            <xsl:value-of select="data[@navn='DiffProgPropFStv']"/>
          </td>
          <td align="right">
            <xsl:value-of select="data[@navn='ProgAntMndtFtv']"/>
            <xsl:if test="data[@navn='DiffProgAntMndtFFtv']!=''">
              <xsl:text> (</xsl:text>
              <xsl:value-of select="data[@navn='DiffProgAntMndtFFtv']"/>
              <xsl:text>)</xsl:text>
            </xsl:if>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </tr>
  </xsl:template>

  <!-- Template for kolonner i tabellen F05-->
  <xsl:template match="liste" mode="F05">
    <!-- F05 -->
    <tr>
      <td>
        <xsl:value-of select="data[@navn='Partikode']"/>
      </td>
      <td align="right">
        <xsl:if test="data[@navn='AntStemmer']!=''">
          <xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
        </xsl:if>
      </td>
      <td align="right">
        <xsl:value-of select="data[@navn='ProgProSt']"/>
      </td>
      <td align="right">
        <xsl:value-of select="data[@navn='DiffProgPropFFtv']"/>
      </td>
      <td align="right">
        <xsl:value-of select="data[@navn='DiffProgPropFStv']"/>
      </td>
      <!--
	<xsl:value-of select="$tab"/>
	<xsl:value-of select="data[@navn='ProgAntMndtStv']"/>
	<xsl:if test="data[@navn='DiffProgAntMndtStv']!=''">
		<xsl:text> (</xsl:text>
		<xsl:value-of select="data[@navn='DiffProgAntMndtStv']"/>
		<xsl:text>)</xsl:text>
	</xsl:if>		
-->
    </tr>
  </xsl:template>

  <!-- Template for kolonner i tabellen se-->
  <xsl:template match="liste" mode="se">
    <!-- F02 F03 K03 -->
    <tr>
      <td>
        <xsl:value-of select="data[@navn='Partikode']"/>
      </td>
      <td align="right">
        <xsl:if test="data[@navn='AntStemmer']!=''">
          <xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
        </xsl:if>
      </td>
      <td align="right">
        <xsl:value-of select="data[@navn='ProSt']"/>
      </td>

      <xsl:choose>
        <xsl:when test="$rapportnavn='K03'">
          <td align="right">
            <xsl:value-of select="data[@navn='DiffPropFKsv']"/>
          </td>
          <td align="right">
            <xsl:value-of select="data[@navn='DiffPropFStv']"/>
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td align="right">
            <xsl:value-of select="data[@navn='DiffPropFFtv']"/>
          </td>
          <td align="right">
            <xsl:value-of select="data[@navn='DiffPropFStv']"/>
          </td>
        </xsl:otherwise>
      </xsl:choose>
    </tr>
  </xsl:template>

  <!-- Template for kolonner i tabellen K05-->
  <xsl:template match="liste" mode="K05">
    <!-- K05 K04-->
    <tr>
      <td>
        <xsl:value-of select="data[@navn='Partikode']"/>
      </td>
      <td align="right">
        <xsl:if test="data[@navn='AntStemmer']!=''">
          <xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
        </xsl:if>
      </td>
      <td align="right">
        <xsl:value-of select="data[@navn='ProSt']"/>
      </td>
      <td align="right">
        <xsl:value-of select="data[@navn='DiffPropFKsv']"/>
      </td>
      <td align="right">
        <xsl:value-of select="data[@navn='DiffPropFStv']"/>
      </td>
    </tr>
  </xsl:template>

  <!-- Template for tabellhode i tabellen -->
  <xsl:template name="table_header">
    <tr class="header">
      <td>
        <xsl:text>Parti</xsl:text>
      </td>
      <td align="right">
        <xsl:text>Stemmer</xsl:text>
      </td>
      <td align="right">
        <xsl:text>Andel %</xsl:text>
      </td>
      <xsl:choose>
        <xsl:when test="$rapportnavn='K05' or $rapportnavn='K04' or $rapportnavn='K08'">
          <td align="right">
            <xsl:text>11-15</xsl:text>
          </td>
          <td align="right">
            <xsl:text>13-15</xsl:text>
          </td>
        </xsl:when>
        <xsl:when test="$rapportnavn='F05' or $rapportnavn='F02'">
          <td align="right">
            <xsl:text>11-15</xsl:text>
          </td>
          <td align="right">
            <xsl:text>13-15</xsl:text>
          </td>
          <!--
			<xsl:value-of select="$tab"/>
			<xsl:text>Mand. tenkt St.valg</xsl:text>
			-->
        </xsl:when>
        <xsl:when test="$rapportnavn='K02' or $rapportnavn='K03' or $rapportnavn='F04'">
          <td align="right">
            <xsl:text>11-15</xsl:text>
          </td>
          <td align="right">
            <xsl:text>13-15</xsl:text>
          </td>
          <xsl:if test="$tabelltype='ov'">
            <td align="right">
              <xsl:text>Mand.</xsl:text>
            </td>
          </xsl:if>
        </xsl:when>
        <xsl:otherwise>
          <td align="right">
            <xsl:text>11-15</xsl:text>
          </td>
          <xsl:if test="$tabelltype='ov'">
            <td align="right">
              <xsl:text>Mand.</xsl:text>
            </td>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </tr>
  </xsl:template>

  <!-- Template for sum i tabellen -->
  <xsl:template name="sum">
    <tr class="sum">
      <td>
        <xsl:text>Sum</xsl:text>
      </td>

      <td align="right">
        <!--<xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikode'] != 'Andre']/data[@navn='AntStemmer']), '### ##0', 'no')"/>-->
        <xsl:value-of select="format-number(/respons/rapport/data[@navn='AntFrammotte'], '### ##0', 'no')"/>
      </td>

      <td></td>
      <td></td>
      <xsl:if test="$tabelltype='ov'">
        <td></td>
      </xsl:if>
      <xsl:if test="$rapportnavn='K02' or $rapportnavn='K03' or $rapportnavn='F04' or $rapportnavn='F02'">
        <td></td>
      </xsl:if>
    </tr>
  </xsl:template>

  <!-- Template for sum i tabellen -->
  <xsl:template name="sumandre">
    <tr>
      <td>
        <xsl:text>Andre</xsl:text>
      </td>
      <td align="right">
        <xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikategori'] = '3']/data[@navn='AntStemmer']), '### ##0', 'no')"/>
      </td>
      <td></td>
      <td></td>
      <xsl:if test="$tabelltype='ov'">
        <td></td>
      </xsl:if>
      <xsl:if test="$rapportnavn='K02' or $rapportnavn='K03' or $rapportnavn='F04' or $rapportnavn='F02'">
        <td></td>
      </xsl:if>

    </tr>
  </xsl:template>

  <!--
<xsl:variable name="crlf">
	<xsl:text>&#13;&#10;</xsl:text>
</xsl:variable>

<xsl:variable name="tab">
	<xsl:text>&#9;</xsl:text>
</xsl:variable>
-->

  <xsl:template match="rapport" mode="status">
    <!--
		435 av 435 kommuner (435 ferdig opptalt).€
		100,0 prosent av ant. stemmeberettigede.€
		2521781 opptalte stemmer.€
		Frammøte: 39,3 prosent.€
	-->
    <xsl:if test="data[@navn='TotAntKomm']">
      <p>
        <xsl:value-of select="data[@navn='AntKommFhstOpptalt'] + data[@navn='AntKommVtstOpptalt'] + data[@navn='AntKommAltOpptalt']"/>
        <xsl:text> av </xsl:text>
        <xsl:value-of select="data[@navn='TotAntKomm']"/>
        <xsl:text> kommuner (</xsl:text>
        <xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
        <xsl:text> ferdig opptalt).</xsl:text>
      </p>
    </xsl:if>

    <p>
      <xsl:text>Omfatter </xsl:text>
      <xsl:if test="data[@navn='AntStBerett']">
        <xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStBerett'] * 100), '#0,0', 'pros')"/>
      </xsl:if>
      <xsl:if test="data[@navn='AntStberett']">
        <xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStberett'] * 100), '#0,0', 'pros')"/>
      </xsl:if>
      <xsl:text> prosent av </xsl:text>
      <xsl:choose>
        <xsl:when test="data[@navn='AntStBerett']">
          <xsl:value-of select="format-number(data[@navn='AntStBerett'], '### ### ###', 'no')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="format-number(data[@navn='AntStberett'], '### ### ###', 'no')"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text> stemmeberettigede.</xsl:text>
    </p>
    <p>
      <xsl:value-of select="format-number(data[@navn='AntFrammotte'], '### ### ###', 'no')"/>
      <xsl:text> opptalte stemmer.</xsl:text>
    </p>
    <p>
      <xsl:text>Frammøte: </xsl:text>
      <xsl:value-of select="data[@navn='ProFrammotte']"/>
      <xsl:text> prosent.</xsl:text>
    </p>

  </xsl:template>

  <xsl:template match="rapport" mode="status-bunn">

    <p>
      <xsl:text>Antall st.ber.: </xsl:text>
      <xsl:if test="data[@navn='AntStBerett']">
        <xsl:value-of select="data[@navn='AntStBerett']"/>
      </xsl:if>
      <xsl:if test="data[@navn='AntStberett']">
        <xsl:value-of select="data[@navn='AntStberett']"/>
      </xsl:if>
      <xsl:text>.</xsl:text>
    </p>
    <p>
      <xsl:text>Frammøte: </xsl:text>
      <xsl:value-of select="data[@navn='ProFrammotte']"/>
      <xsl:text> prosent.</xsl:text>
    </p>
    <p>
      <xsl:choose>
        <xsl:when test="$statusind = '5'">
          <xsl:text>Foreløpig resultat.</xsl:text>
        </xsl:when>
        <xsl:when test="$statusind = '6'">
          <xsl:text>Alle forhåndsstemmer opptalt.</xsl:text>
        </xsl:when>
        <xsl:when test="$statusind = '7'">
          <xsl:text>Alt opptalt untatt enkelte forhåndsstemmer.</xsl:text>
        </xsl:when>
        <xsl:when test="$statusind = '8'">
          <xsl:text>Alt opptalt.</xsl:text>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </p>

  </xsl:template>

  <xsl:variable name="kanal">
    <xsl:choose>
      <!--<xsl:when test="/respons/rapport/rapportnavn[.='F05' or .='K05' or .='F04' or .='K04']">A</xsl:when>-->
      <xsl:when test="substring-before(/respons/rapport/rapportnavn, '0')='F'">A</xsl:when>
      <xsl:otherwise>C</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:template name="NitfHead">
    <head>
      <title>
        <xsl:value-of select="$infolinje"/>
      </title>
      <meta name="ntb-folder" content="Ut-Satellitt" />
      <meta name="ntb-date">
        <xsl:attribute name="content">
          <xsl:value-of select="$ntbdato"/>
        </xsl:attribute>
      </meta>

      <meta name="sistereg">
        <xsl:attribute name="content">
          <xsl:value-of select="$normdate"/>
        </xsl:attribute>
      </meta>
      <meta name="rapportnavn">
        <xsl:attribute name="content">
          <xsl:value-of select="/respons/rapport/rapportnavn"/>
        </xsl:attribute>
      </meta>
      <meta name="status-ind">
        <xsl:attribute name="content">
          <xsl:value-of select="/respons/rapport/data[@navn='StatusInd']"/>
        </xsl:attribute>
      </meta>
      <meta name="fylkenr">
        <xsl:attribute name="content">
          <xsl:value-of select="/respons/rapport/data[@navn='FylkeNr']"/>
        </xsl:attribute>
      </meta>
      <meta name="fylkenavn">
        <xsl:attribute name="content">
          <xsl:value-of select="$fylke"/>
        </xsl:attribute>
      </meta>
      <meta name="kommunenr">
        <xsl:attribute name="content">
          <xsl:value-of select="/respons/rapport/data[@navn='KommNr']"/>
        </xsl:attribute>
      </meta>
      <meta name="kommunenavn">
        <xsl:attribute name="content">
          <xsl:value-of select="$kommune"/>
        </xsl:attribute>
      </meta>
      <meta name="kretsnr">
        <xsl:attribute name="content">
          <xsl:value-of select="/respons/rapport/data[@navn='KretsNr']"/>
        </xsl:attribute>
      </meta>
      <meta name="kretsnavn">
        <xsl:attribute name="content">
          <xsl:value-of select="$krets"/>
        </xsl:attribute>
      </meta>
      <meta name="bydel">
        <xsl:attribute name="content">
          <xsl:value-of select="$bydel"/>
        </xsl:attribute>
      </meta>
      <meta name="bydelnr">
        <xsl:attribute name="content">
          <xsl:value-of select="$bydelnr"/>
        </xsl:attribute>
      </meta>
      <meta name="tabelltype">
        <xsl:attribute name="content">
          <xsl:value-of select="$tabelltype"/>
        </xsl:attribute>
      </meta>

      <!--<meta name="ntb-filename" content="3216E7CAF1.xml" />-->
      <meta name="ntb-distribusjonsKode" content="ALL" />
      <meta name="ntb-kanal" content="">
        <xsl:attribute name="content">
          <xsl:value-of select="$kanal"/>
        </xsl:attribute>
      </meta>
      <meta name="ntb-meldingsSign" content="valget" />
      <meta name="ntb-meldingsType" content="Valg" />
      <meta name="ntb-id">
        <xsl:attribute name="content">
          <xsl:value-of select="$nitfdate"/>_<xsl:value-of select="$rapportnavn"/>
        </xsl:attribute>
      </meta>
      <meta name="ntb-meldingsVersjon" content="0" />
      <tobject tobject.type="Innenriks">
        <tobject.property tobject.property.type="Nyheter" />
        <tobject.subject tobject.subject.code="POL" tobject.subject.refnum="11000000" tobject.subject.type="Politikk" />
      </tobject>
      <docdata>
        <evloc state-prov="Norge" />
        <evloc state-prov="Norge" county-dist="Riksnyheter"/>
        <doc-id regsrc="NTB" id-string="RED030807_111432_as_00">
          <xsl:attribute name="id-string">
            <xsl:value-of select="$nitfdate"/>_<xsl:value-of select="$rapportnavn"/>
          </xsl:attribute>
        </doc-id>
        <urgency>
          <xsl:attribute name="ed-urg">
            <xsl:value-of select="$urgency"/>
          </xsl:attribute>
        </urgency>
        <date.issue norm="200308">
          <xsl:attribute name="norm">
            <xsl:value-of select="$date"/>
          </xsl:attribute>
        </date.issue>
        <ed-msg info="" />
        <du-key version="1" key="valget">
          <xsl:attribute name="key">
            <xsl:value-of select="$stikkord"/>
          </xsl:attribute>
        </du-key>
        <doc.copyright year="2015" holder="NTB" />
        <key-list>
          <keyword key="valget">
            <xsl:attribute name="key">
              <xsl:value-of select="$stikkord"/>
            </xsl:attribute>
          </keyword>
        </key-list>
      </docdata>
      <pubdata date.publication="" item-length="0" unit-of-measure="character">
        <xsl:attribute name="date.publication">
          <xsl:value-of select="$nitfdate"/>
        </xsl:attribute>
        <!--
	<xsl:attribute name="item-length">
		<xsl:value-of select="string-length(/respons/rapport/tabell)"/>
	</xsl:attribute>
	-->
      </pubdata>
      <revision-history name="valget" />
    </head>
  </xsl:template>


  <xsl:variable name="rapportnavn">
    <xsl:value-of select="/respons/rapport/rapportnavn"/>
  </xsl:variable>

  <xsl:variable name="rapport">
    <xsl:value-of select="substring-after(/respons/rapport/rapportnavn, '0')"></xsl:value-of>
  </xsl:variable>

  <xsl:variable name="rapporttype">
    <xsl:choose>
      <xsl:when test="$rapport='2'">KOM</xsl:when>
      <xsl:when test="$rapport='3'">KRE</xsl:when>
      <xsl:otherwise>OVS</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="date">
    <xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
  </xsl:variable>

  <xsl:variable name="time">
    <xsl:value-of select="/respons/rapport/data[@navn='SisteRegTid']"/>
  </xsl:variable>

  <xsl:variable name="dato">
    <xsl:value-of select="substring($date, 7, 2)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 5, 2)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 1, 4)"/>
  </xsl:variable>

  <xsl:variable name="tid">
    <xsl:value-of select="substring($time, 1, 5)"/>
  </xsl:variable>

  <xsl:variable name="nitfdate">
    <xsl:value-of select="$date"/>
    <xsl:text>T</xsl:text>
    <xsl:value-of select="substring($time, 1, 2)"/>
    <xsl:value-of select="substring($time, 4, 2)"/>
    <xsl:value-of select="substring($time, 7, 2)"/>
  </xsl:variable>

  <xsl:variable name="valgtype">
    <xsl:choose>
      <xsl:when test="substring-before($rapportnavn, '0') = 'F'">
        <xsl:text>Fylkestingsvalg</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Kommunestyrevalg</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:param name="ntbdato">
    <xsl:value-of select="$dato"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 1, 5)"/>
  </xsl:param>

  <xsl:param name="normdate">
    <xsl:value-of select="substring($date, 1, 4)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 5, 2)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 7, 2)"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$time"/>
  </xsl:param>

  <xsl:variable name="stikkord">
    <!-- stikkord -->
    <xsl:text>VLG-</xsl:text>
    <xsl:value-of select="$rapportnavn"/>-<xsl:value-of select="$rapport-type"/>
  </xsl:variable>

  <xsl:variable name="urgency">
    <xsl:choose>
      <xsl:when test="$rapport ='5' or $rapport ='4'">4</xsl:when>
      <xsl:otherwise>6</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="statusind">
    <xsl:value-of select="/respons/rapport/data[@navn='StatusInd']"/>
  </xsl:variable>

  <xsl:variable name="stedsnavn">
    <!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
    <xsl:choose>
      <xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
        <xsl:value-of select="$kommune"/>
        <xsl:text> i </xsl:text>
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
        <xsl:value-of select="$krets"/>
        <xsl:text> i </xsl:text>
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='ST05' or $rapportnavn = 'K08'">
        <!--<xsl:text>krets-</xsl:text>-->
        <xsl:value-of select="$kommune"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$bydel"/>
      </xsl:when>
      <xsl:otherwise>Landsoversikt</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="rapport-type">
    <!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
    <xsl:choose>
      <xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
        <!--<xsl:text>komm-</xsl:text>-->
        <xsl:value-of select="$kommunenr"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
        <!--<xsl:text>fylke-</xsl:text>-->
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
        <!--<xsl:text>krets-</xsl:text>-->
        <xsl:value-of select="$kommune"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$krets"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'K08'">
        <!--<xsl:text>krets-</xsl:text>-->
        <xsl:value-of select="$kommune"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$bydel"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K05' or $rapportnavn='F05'">
        <xsl:text>landsoversikt</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="tabelltype">
    <xsl:choose>
      <xsl:when test="$rapportnavn = 'K02'">
        <xsl:text>ov</xsl:text>
      </xsl:when>
      <xsl:when test="substring($rapportnavn, 2, 2) = '02' or substring($rapportnavn, 2, 2) = '03'">
        <xsl:text>se</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>ov</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="infolinje">
    <xsl:value-of select="$valgtype"/>
    <xsl:text>: </xsl:text>
    <xsl:choose>
      <!--
		<xsl:when test="$rapportnavn = 'F01' or $rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner</xsl:text>
		</xsl:when>
		-->
      <xsl:when test="$rapportnavn = 'F02' or $rapportnavn = 'K02'">
        <xsl:text></xsl:text>
        <xsl:value-of select="$fylke"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'F03' or $rapportnavn = 'K03'">
        <xsl:value-of select="$kommune"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="$krets"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'F04' or $rapportnavn = 'K04'">
        <xsl:text>Fylkesoversikt, </xsl:text>
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'F05' or $rapportnavn = 'K05'">
        <xsl:text>Landsoversikt</xsl:text>
      </xsl:when>
      <!--
		<xsl:when test="$rapportnavn = 'F07' or $rapportnavn = 'K07'">
			<xsl:text>Landsoversikt pr. fylke</xsl:text>
		</xsl:when>		-->
		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo</xsl:text>
		</xsl:when>

      <xsl:when test="$rapportnavn = 'K09'">
        <xsl:text>Bystyreoversikt</xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:text>, kl. </xsl:text>
    <xsl:value-of select="$tid"/>
    <xsl:if test="$statusind &gt; 5">
      <xsl:text>. Korrigert resultat</xsl:text>
    </xsl:if>
  </xsl:variable>

</xsl:stylesheet>