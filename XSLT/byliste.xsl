<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="no"/>

  <xsl:param name="FNr"></xsl:param>
  <xsl:param name="ByNr"></xsl:param>
  <xsl:param name="valgtype">K</xsl:param>

  <xsl:variable name="Form">Default.aspx</xsl:variable>

  <xsl:key name="kretsitems_by_kommnr" match="rapport" use="data[@navn='KommNr']" />

  <xsl:variable name="count_01" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '01'])" />
  <xsl:variable name="max_01" select="'6'"/>
  <xsl:variable name="count_02" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '02'])" />
  <xsl:variable name="max_02" select="'20'"/>
  <xsl:variable name="count_04" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '04'])" />
  <xsl:variable name="max_04" select="'9'"/>
  <xsl:variable name="count_05" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '05'])" />
  <xsl:variable name="max_05" select="'4'"/>
  <xsl:variable name="count_06" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '06'])" />
  <xsl:variable name="max_06" select="'9'"/>
  <xsl:variable name="count_07" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '07'])" />
  <xsl:variable name="max_07" select="'8'"/>
  <xsl:variable name="count_08" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '08'])" />
  <xsl:variable name="max_08" select="'6'"/>
  <xsl:variable name="count_09" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '09'])" />
  <xsl:variable name="max_09" select="'5'"/>
  <xsl:variable name="count_10" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '10'])" />
  <xsl:variable name="max_10" select="'9'"/>
  <xsl:variable name="count_11" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '11'])" />
  <xsl:variable name="max_11" select="'15'"/>
  <xsl:variable name="count_12" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '12'])" />
  <xsl:variable name="max_12" select="'17'"/>
  <xsl:variable name="count_14" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '14'])" />
  <xsl:variable name="max_14" select="'2'"/>
  <xsl:variable name="count_15" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '15'])" />
  <xsl:variable name="max_15" select="'12'"/>
  <xsl:variable name="count_16" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '16'])" />
  <xsl:variable name="max_16" select="'12'"/>
  <xsl:variable name="count_17" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '17'])" />
  <xsl:variable name="max_17" select="'6'"/>
  <xsl:variable name="count_18" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '18'])" />
  <xsl:variable name="max_18" select="'9'"/>
  <xsl:variable name="count_19" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '19'])" />
  <xsl:variable name="max_19" select="'7'"/>
  <xsl:variable name="count_20" select="count(/respons/rapport[generate-id(.) = generate-id(key('kretsitems_by_kommnr', data[@navn='KommNr'])[1]) and substring(data[@navn='KommNr'],1,2) = '20'])" />
  <xsl:variable name="max_20" select="'5'"/>


  <xsl:template match="respons">
    <table width="100%" border="0" cellpadding="1" cellspacing="0">

      <!--<xsl:choose>
		<xsl:when test="$valgtype='F'">
			<xsl:call-template name="oslo"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="/respons/rapport[data[@navn='KommNr']='0301'][1]" mode="by"/>
		</xsl:otherwise>
	</xsl:choose>-->

      <xsl:apply-templates select="/respons/rapport[data[@navn='KommNr']='0301']" mode="by">
        <xsl:with-param name="FBNr" select="'03'"/>
      </xsl:apply-templates>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'01' != $FNr">
                <xsl:text>01</xsl:text>
              </xsl:if>
              <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Østfold</xsl:text>
            </xsl:attribute>
            <!--xsl:value-of select="$KommNavn"/-->
            <xsl:text>Østfold</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_01"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_01"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_01"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_01"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='01'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'02' != $FNr">
                <xsl:text>02</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Akershus</xsl:text>
            </xsl:attribute>
            <xsl:text>Akershus</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_02"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_02"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_02"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_02"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='02'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'04' != $FNr">
                <xsl:text>04</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Hedmark</xsl:text>
            </xsl:attribute>
            <xsl:text>Hedmark</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_04"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_04"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_04"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_04"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='04'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'05' != $FNr">
                <xsl:text>05</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Oppland</xsl:text>
            </xsl:attribute>
            <xsl:text>Oppland</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_05"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_05"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_05"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_05"/>
          </abbr>
        </td>
      </tr>
      <xsl:if test="$FNr='05'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'06' != $FNr">
                <xsl:text>06</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Buskerud</xsl:text>
            </xsl:attribute>
            <xsl:text>Buskerud</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_06"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_06"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_06"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_06"/>
          </abbr>
        </td>
      </tr>
      <xsl:if test="$FNr='06'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'07' != $FNr">
                <xsl:text>07</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Vestfold</xsl:text>
            </xsl:attribute>
            <xsl:text>Vestfold</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_07"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_07"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_07"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_07"/>
          </abbr>
        </td>
      </tr>
      <xsl:if test="$FNr='07'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'08' != $FNr">
                <xsl:text>08</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Telemark</xsl:text>
            </xsl:attribute>
            <xsl:text>Telemark</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_08"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_08"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_08"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_08"/>
          </abbr>
        </td>
      </tr>
      <xsl:if test="$FNr='08'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'09' != $FNr">
                <xsl:text>09</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Aust-Agder</xsl:text>
            </xsl:attribute>
            <xsl:text>Aust-Agder</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_09"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_09"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_09"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_09"/>
          </abbr>
        </td>
      </tr>
      <xsl:if test="$FNr='09'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'10' != $FNr">
                <xsl:text>10</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Vest-Agder</xsl:text>
            </xsl:attribute>
            <xsl:text>Vest-Agder</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_10"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_10"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_10"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_10"/>
          </abbr>
        </td>
      </tr>
      <xsl:if test="$FNr='10'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'11' != $FNr">
                <xsl:text>11</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Rogaland</xsl:text>
            </xsl:attribute>
            <xsl:text>Rogaland</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_11"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_11"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_11"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_11"/>
          </abbr>
        </td>
      </tr>
      <xsl:if test="$FNr='11'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'12' != $FNr">
                <xsl:text>12</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Hordaland</xsl:text>
            </xsl:attribute>
            <xsl:text>Hordaland</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_12"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_12"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_12"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_12"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='12'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'14' != $FNr">
                <xsl:text>14</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Sogn og Fjordane</xsl:text>
            </xsl:attribute>
            <xsl:text>Sogn og Fjordane</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_14"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_14"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_14"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_14"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='14'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'15' != $FNr">
                <xsl:text>15</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Møre og Romsdal</xsl:text>
            </xsl:attribute>
            <xsl:text>Møre og Romsdal</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_15"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_15"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_15"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_15"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='15'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'16' != $FNr">
                <xsl:text>16</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Sør-Trøndelag</xsl:text>
            </xsl:attribute>
            <xsl:text>Sør-Trøndelag</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_16"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_16"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_16"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_16"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='16'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'17' != $FNr">
                <xsl:text>17</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Nord-Trøndelag</xsl:text>
            </xsl:attribute>
            <xsl:text>Nord-Trøndelag</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_17"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_17"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_17"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_17"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='17'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'18' != $FNr">
                <xsl:text>18</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Nordland</xsl:text>
            </xsl:attribute>
            <xsl:text>Nordland</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_18"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_18"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_18"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_18"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='18'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'19' != $FNr">
                <xsl:text>19</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Troms</xsl:text>
            </xsl:attribute>
            <xsl:text>Troms</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_19"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_19"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_19"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_19"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='19'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

      <tr>
        <td colspan="2">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?FByNr=</xsl:text>
              <xsl:if test="'20' != $FNr">
                <xsl:text>20</xsl:text>
              </xsl:if>
              <xsl:text>&amp;KommuneNr=&amp;ByNr=</xsl:text>
              <xsl:text>&amp;Navn=Finnmark</xsl:text>
            </xsl:attribute>
            <xsl:text>Finnmark</xsl:text>
          </a>
        </td>
        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Kretsrapporter mottatt for </xsl:text>
              <xsl:value-of select="$count_20"/>
              <xsl:text> av totalt </xsl:text>
              <xsl:value-of select="$max_20"/>
              <xsl:text> kretsrapporterende kommuner</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="$count_20"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$max_20"/>
          </abbr>
        </td>

      </tr>
      <xsl:if test="$FNr='20'">
        <xsl:apply-templates select="/respons/rapport[substring(data[@navn='KommNr'],1,2) = $FNr]" mode="by">
          <xsl:with-param name="FBNr" select="$FNr"/>
        </xsl:apply-templates>
      </xsl:if>

    </table>
    <p/>
    <table width="100%" cellpadding="1" cellspacing="0" border="0">
      <tr>
        <td class='status' valign='top'>Tallene på fylkesnivå viser:</td>
        <td class='status'> Antall kommuner som har rapportert kresultater/totalt antall kretsrapporterende kommuner</td>
      </tr>
      <tr>
        <td class='status' valign='top'>Tallene på kommunenivå viser:</td>
        <td class='status'>Antall kretser som har rapportert resultater/totalt antall kretser i kommunen</td>
      </tr>
    </table>

  </xsl:template>

  <xsl:template name="oslo">
    <tr>
      <td>
        <a>
          <xsl:attribute name="href">
            <xsl:value-of select="$Form"/>
            <xsl:text>?liste=oslo</xsl:text>
            <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
          </xsl:attribute>Oslo bystyreoversikt
        </a>
      </td>
      <td class="status" align="right">103</td>

    </tr>
  </xsl:template>

  <xsl:template match="/respons/rapport" mode="by">
    <xsl:param name="FBNr" />
    <xsl:variable name="KommNr">
      <xsl:value-of select="data[@navn='KommNr']"/>
    </xsl:variable>
    <xsl:variable name="KommNavn">
      <xsl:value-of select="data[@navn='KommNavn']"/>
    </xsl:variable>

    <xsl:if test ="count(. | key('kretsitems_by_kommnr', $KommNr)[1]) = 1">

      <tr>
        <xsl:if test="$FBNr != '03'">
          <td width="5"></td>
        </xsl:if>

        <td>
          <xsl:if test="$FBNr = '03'">
            <xsl:attribute name="colspan">2</xsl:attribute>
          </xsl:if>
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$Form"/>
              <xsl:text>?ByNr=</xsl:text>
              <xsl:if test="$KommNr != $ByNr">
                <xsl:value-of select="$KommNr"/>
              </xsl:if>
              <xsl:text>&amp;FByNr=</xsl:text>
              <xsl:value-of select="$FBNr"/>
              <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
              <xsl:text>&amp;KommuneNr=</xsl:text>
              <xsl:value-of select="$KommNr"/>

              <xsl:text>&amp;Navn=</xsl:text>
              <xsl:value-of select="$KommNavn"/>
            </xsl:attribute>
            <xsl:value-of select="$KommNavn"/>
          </a>
        </td>

        <td class="status" align="right">
          <abbr>
            <xsl:attribute name="title">
              <xsl:text>Rapporter mottatt for </xsl:text>
              <xsl:value-of select="count(key('kretsitems_by_kommnr', $KommNr)[data[@navn='StatusInd'] != 0])"/>
              <xsl:text> av </xsl:text>
              <xsl:value-of select="data[@navn='TotAntKretser']"/>
              <xsl:text> kretser</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="count(key('kretsitems_by_kommnr', $KommNr)[data[@navn='StatusInd'] != 0])"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="data[@navn='TotAntKretser']"/>
          </abbr>
        </td>
      </tr>
      <xsl:if test="$KommNr=$ByNr">
        <tr>
          <td width="5"></td>
          <td align="left">
            <a>
              <xsl:attribute name="href">
                <xsl:value-of select="$Form"/>
                <xsl:text>?ByNr=</xsl:text>
                <xsl:value-of select="$KommNr"/>
                <xsl:text>&amp;FByNr=</xsl:text>
                <xsl:value-of select="$FBNr"/>
                <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
                <xsl:text>&amp;KommuneNr=</xsl:text>
                <xsl:value-of select="$KommNr"/>
                <xsl:text>&amp;AlleKretser=1</xsl:text>
                <xsl:text>&amp;Navn=</xsl:text>
                <xsl:value-of select="$KommNavn"/>
              </xsl:attribute>
              - Alle
            </a>
          </td>
          <td width="20"></td>

        </tr>
      </xsl:if>
      <xsl:if test="$KommNr=$ByNr">
        <tr>
          <td width="5"></td>
          <td colspan="2">
            <table width="100%"  border="0" cellpadding="1" cellspacing="0">
              <xsl:apply-templates select="/respons/rapport[data[@navn='KommNr'] = $KommNr and data[@navn='KretsNr'] != '0000' and data[@navn='AntFrammotte'] != 0]" mode="krets">
                <xsl:with-param name="FBNr" select="$FNr"/>
                <xsl:sort select="data[@navn='KretsNavn']"></xsl:sort>
              </xsl:apply-templates>
              <xsl:apply-templates select="/respons/rapport[data[@navn='KommNr'] = $KommNr and data[@navn='KretsNr'] = '0000' and data[@navn='AntFrammotte'] != 0]" mode="krets">
                <xsl:with-param name="FBNr" select="$FNr"/>
              </xsl:apply-templates>
            </table>
          </td>
        </tr>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template match="*">
  </xsl:template>

  <xsl:template match="/respons/rapport" mode="krets">
    <xsl:param name="FBNr" />
    <tr>
      <td width="5"></td>
      <td>
        <a>
          <xsl:attribute name="href">
            <xsl:value-of select="$Form"/>
            <xsl:text>?ByNr=</xsl:text>
            <xsl:value-of select="$ByNr"/>
            <xsl:text>&amp;FByNr=</xsl:text>
            <xsl:value-of select="$FBNr"/>
            <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
            <xsl:text>&amp;KretsNr=</xsl:text>
            <xsl:value-of select="data[@navn='KretsNr']"/>
            <xsl:text>&amp;Navn=</xsl:text>
            <xsl:value-of select="data[@navn='KretsNavn']"/>
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="data[@navn='KretsNr'] = 'VIRT'">
              <xsl:text>Forhåndsstemmer</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="data[@navn='KretsNavn']"/>
            </xsl:otherwise>
          </xsl:choose>
        </a>
      </td>
      <!--
	<td class="status">
			(<xsl:value-of select="data[@navn='StatusInd']"/>)
	</td>
	-->
    </tr>
  </xsl:template>


</xsl:stylesheet>