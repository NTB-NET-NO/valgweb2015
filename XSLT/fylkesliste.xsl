<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="no"/>

<xsl:param name="FylkeNr">00</xsl:param>
<xsl:variable name="Form">Default.aspx</xsl:variable>
<xsl:param name="Expand"></xsl:param>
  <xsl:param name="Valgtype"></xsl:param>

  <xsl:param name="xmlpath">valg-xml-inn</xsl:param>
  <xsl:param name="xmlpathkrets">..\<xsl:value-of select="$xmlpath"/>\<xsl:value-of select="$Valgtype"/>03.XML</xsl:param>


  <xsl:template match="respons">
<table width="100%" cellpadding="0" cellspacing="2" border="0">
<tr><td class='status'>Fylkesoversikt</td><td align="right" colspan="4" class='status'>Samleoversikt</td></tr>
  <tr>
    <td class='status'></td>
    <td align="center" class='status' title="Antall kommuner i fylket">Tot</td>
    <td align="center" class='status' title="Bare forhåndsstemmer opptalt">FS</td>
    <td align="center" class='status' title="Bare valgtingsstemmer opptalt">VS</td>
    <td align="center" class='status' title="Alle stemmer opptalt">AS</td>
  </tr>
  <xsl:apply-templates select="rapport"/>
</table>
  <p/>
  <table width="100%" cellpadding="2" cellspacing="0" border="0">
    <tr>
      <td class='status' valign='top'>Tot:</td>
      <td class='status'>Totalt ant kommuner i fylket</td>
    </tr>
    <tr>
      <td class='status' valign='top'>FS:</td>
      <td class='status'>Antall kommuner der bare forhåndsstemmer er opptalt</td>
    </tr>
    <tr>
      <td class='status' valign='top'>VS:</td>
      <td class='status'>Antall kommuner der bare valgtingsstemmer er opptalt</td>
    </tr>
    <tr>
      <td class='status' valign='top'>AS:</td>
      <td class='status'>Antall kommuner der alle stemmer er opptalt</td>
    </tr>
  </table>

</xsl:template>

<!--
<xsl:template match="rapport/liste/data[@navn='FylkeNavn']">

<xsl:template match="rapport/data[@navn='FylkeNavn']">
-->
<xsl:template match="rapport">
<tr><td>
	<a>
	<xsl:attribute name="href">
		<xsl:value-of select="$Form"/>
    <xsl:text>?FylkeNr=</xsl:text>
    <xsl:value-of select="data[@navn='FylkeNr']"/>
    <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
    <xsl:if test="data[@navn='FylkeNr']=$FylkeNr and $Expand=''">
			<xsl:text>&amp;Expand=-1</xsl:text>
		</xsl:if>
		<!--
		<xsl:if test="data[@navn='FylkeNr']!=$FylkeNr">
			<xsl:value-of select="data[@navn='FylkeNr']"/>
		</xsl:if>
		-->

		<xsl:text>&amp;Navn=</xsl:text>
		<xsl:value-of select="data[@navn='FylkeNavn']"/>
	</xsl:attribute>
	<xsl:value-of select="data[@navn='FylkeNavn']"/>
	</a>
  </td>
  
	<!--<td class="status" align="right" title="Klikk for fylkesoversikt pr. kommune! (Antall kommuner i fylket: fhst. opptalt + vtst. opptalt + alt opptalt)">
	<a>
	<xsl:attribute name="href">
		<xsl:value-of select="$Form"/>
		<xsl:text>?OversiktFylkeNr=</xsl:text>
		<xsl:value-of select="data[@navn='FylkeNr']"/>		
	</xsl:attribute>
	<xsl:value-of select="data[@navn='TotAntKomm']"/>
	<xsl:text>:</xsl:text
	<xsl:value-of select="data[@navn='AntKommFhstOpptalt']"/>
	<xsl:text>+</xsl:text>
	<xsl:value-of select="data[@navn='AntKommVtstOpptalt']"/>
	<xsl:text>+</xsl:text>
	<xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
	</a>
</td>-->

  <td align="center" title="Ant. kommuner i fylket. Klikk for samleoversikt!">
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="$Form"/>
        <xsl:text>?OversiktFylkeNr=</xsl:text>
        <xsl:value-of select="data[@navn='FylkeNr']"/>
      </xsl:attribute>
      <xsl:value-of select="data[@navn='TotAntKomm']"/>
    </a>
  </td>
  <td align="center" title="Antall kommuner i fylket der bare forhåndsstemmer er opptalt"><xsl:value-of select="data[@navn='AntKommFhstOpptalt']"/></td>
  <td align="center" title="Antall kommuner i fylket der bare valgtingsstemmer er opptalt"><xsl:value-of select="data[@navn='AntKommVtstOpptalt']"/></td>
  <td align="center" title="Antall kommuner i fylket der alle stemmer er opptalt"><xsl:value-of select="data[@navn='AntKommAltOpptalt']"/></td>

</tr>

  
  
  <xsl:if test="data[@navn='FylkeNr']=$FylkeNr and $Expand=''">
    
    <tr>
      <td colspan="5">
        <table width="100%">
          <tr>
            <td colspan="2" align="left">
              <a>
              <xsl:attribute name="href">
                <xsl:value-of select="$Form"/>
                <xsl:text>?FylkeNr=</xsl:text>
                <xsl:value-of select="$FylkeNr"/>
                <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
                <xsl:text>&amp;Alle=1</xsl:text>
                 <xsl:text>&amp;Navn=</xsl:text>
                <xsl:value-of select="data[@navn='FylkeNavn']"/>
              </xsl:attribute>
              - Alle
            </a>
             </td>
            <td width="20"></td>

          </tr>
            <xsl:choose>
              <xsl:when test="$FylkeNr=03">
                <xsl:call-template name="oslo"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:apply-templates select="tabell/liste">
                  <xsl:sort select="data[@navn='KommNavn']"></xsl:sort>
                </xsl:apply-templates>
                <!--<xsl:apply-templates select="tabell[@navn='ST04tabell3']/liste[string-length(data[@navn = 'KretsNr']) = 0]">
                  <xsl:sort select="data[@navn='KommNavn']"></xsl:sort>
                </xsl:apply-templates>-->
              </xsl:otherwise>
            </xsl:choose>
  </table>
	</td>
</tr>
  

</xsl:if>	
</xsl:template>


  <xsl:template match="tabell/liste[string-length(data[@navn = 'KretsNr']) = 0]">
    <xsl:variable name="komm">
      <xsl:value-of select ="data[@navn = 'KommNr']" />
        </xsl:variable>
    <xsl:if test ="data[@navn = 'StatusInd'] != 0 or document($xmlpathkrets)/respons/rapport[data[@navn='KommNr']=$komm]">
       
    <tr>
	<td width="20">
    
  </td>
	<td>
		<a>
      
		<xsl:attribute name="href">
			<xsl:value-of select="$Form"/>
			<xsl:text>?FylkeNr=</xsl:text>
			<xsl:value-of select="$FylkeNr"/>
      <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
      <xsl:text>&amp;KommuneNr=</xsl:text>
			<xsl:value-of select="data[@navn='KommNr']"/>
			<xsl:text>&amp;Navn=</xsl:text>
			<xsl:value-of select="data[@navn='KommNavn']"/>
		</xsl:attribute>
			<xsl:value-of select="data[@navn='KommNavn']"/>
		</a>
	</td>
	<td class="status" align="center">
    <xsl:attribute name="title">
      <xsl:call-template name="status_text">
        <xsl:with-param name="status" select="data[@navn='StatusInd']" />
      </xsl:call-template>
    </xsl:attribute>
    <xsl:if test="data[@navn='StatusInd'] != ''">
			(<xsl:value-of select="data[@navn='StatusInd']"/>)
    </xsl:if>
    <xsl:if test="data[@navn='StatusInd'] = ''">(-)</xsl:if>
  </td>
	</tr>
    </xsl:if>

  </xsl:template>

<xsl:template name="oslo">
	<tr>
	<td width="20"></td>
	<td>
		<a>
		<xsl:attribute name="href">
			<xsl:value-of select="$Form"/>
			<xsl:text>?FylkeNr=03</xsl:text>
      <!--<xsl:text>&amp;VisningIndeks=3</xsl:text>-->
      <xsl:text>&amp;KommuneNr=0301</xsl:text>
			<xsl:text>&amp;Navn=Oslo</xsl:text>
		</xsl:attribute>
		<xsl:text>Oslo kommune</xsl:text>
		</a>
	</td>
	<td class="status">
			<!--(<xsl:value-of select="data[@navn='StatusInd']"/>)-->
	</td></tr>
</xsl:template>


  <xsl:template name="status_text">
    <xsl:param name="status"/>

    <!--
    Statusindikator for innrapportering har følgende verdier
    0.	Ingen resultater innsendt
    1.	Bare foreløpige forhåndsstemmer innsendt
    2.	Endelige fhst. innsendt
    3.	Bare foreløpige valgtingsstemmer innsendt
    4.	Endelige vtst. innsendt
    5.	Foreløpige fhst. og foreløpige vtst. innsendt
    6.	Endelige fhst. og foreløpige vtst. innsendt
    7.	Foreløpige fhst. og endelige vtst. innsendt
    8.	Endelige fhst. og endelige vtst. Innsendt
    -->

    <xsl:choose>
      <xsl:when test="$status = 0">
        <xsl:text>Resultater mangler for en eller flere kretser i kommunen</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 1">
        <xsl:text>Bare foreløpige forhåndsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 2">
        <xsl:text>Bare endelige forhåndsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 3">
        <xsl:text>Bare foreløpige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 4">
        <xsl:text>Bare endelige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 5">
        <xsl:text>Foreløpige forhåndsstemmer og foreløpige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 6">
        <xsl:text>Endelige forhåndsstemmer og foreløpige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 7">
        <xsl:text>Foreløpige forhåndsstemmer og endelige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:when test="$status = 8">
        <xsl:text>Endelige forhåndsstemmer og endelige valgtingsstemmer registrert</xsl:text>
      </xsl:when>
      <xsl:otherwise>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



</xsl:stylesheet>