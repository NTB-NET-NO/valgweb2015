<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:user="http://ntb.no/mynamespace">

  <!-- Valget IPTC format -->

  <xsl:output encoding="ISO-8859-1" method="text" omit-xml-declaration="yes" indent="no"/>

  <xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>
  <xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>
  <xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>
  <xsl:decimal-format name="pros" decimal-separator="."/>
  <xsl:strip-space elements="*"/>

  <xsl:param name="distribusjon">ALL</xsl:param>
  <xsl:param name="aggregate"></xsl:param>

  <xsl:variable name="rapportnavn">
    <xsl:value-of select="/respons/rapport/rapportnavn"/>
  </xsl:variable>

  <xsl:param name="ntbdato">
    <xsl:value-of select="$dato"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 1, 5)"/>
  </xsl:param>

  <!-- Main template: -->
  <xsl:template match="respons">

    <xsl:if test="$aggregate != 'BODY'">
    <xsl:text>&lt;ASCII-WIN&gt;</xsl:text>
    <xsl:value-of select="$crlf"/>
    </xsl:if>

    <!-- Infolinje -->
    <xsl:text>&lt;ParaStyle:NTB-TITTEL&gt;</xsl:text>
    <xsl:value-of select="$infolinje"/>
    <xsl:value-of select="$crlf"/>

    <!-- Topptekster -->
    
    <!--<xsl:choose>
      <xsl:when test="$rapport='4'">
        <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
        <xsl:choose>
          <xsl:when test="$prognose = 'true'">
            <xsl:text>Fylkesoversikt, prognose, kl. </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>Fylkesoversikt, kl. </xsl:text>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$tid"/>
        <xsl:value-of select="$crlf"/>

        <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
        <xsl:value-of select="$fylke"/>
        <xsl:value-of select="$crlf"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='ST05' or $rapport='8'">
        <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
        <xsl:value-of select="$bydel"/>
        <xsl:value-of select="$crlf"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='ST06' or $rapport='5'">
        <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
        <xsl:choose>
          <xsl:when test="(rapport/data[@navn='TotAntKomm'] - rapport/data[@navn='AntKommVtstOpptalt'] - rapport/data[@navn='AntKommAltOpptalt'])!=0">
            <xsl:text>Landsoversikt, prognose, kl. </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>Landsoversikt, kl. </xsl:text>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$tid"/>
        <xsl:value-of select="$crlf"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K02'">
        <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
        <xsl:value-of select="$fylke"/>
        <xsl:value-of select="$crlf"/>

        <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
        <xsl:value-of select="$kommune"/>
        <xsl:value-of select="$crlf"/>
      </xsl:when>
      <xsl:when test="$rapport='2'">
        <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
        <xsl:value-of select="$fylke"/>
        <xsl:value-of select="$crlf"/>

        <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
        <xsl:value-of select="$kommune"/>
        <xsl:value-of select="$crlf"/>
      </xsl:when>
      <xsl:when test="$rapport='3'">
        <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
        <xsl:value-of select="$kommune"/>
        <xsl:value-of select="$crlf"/>

        <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
        <xsl:value-of select="$krets"/>
        <xsl:value-of select="$crlf"/>
      </xsl:when>
      <xsl:otherwise>
      </xsl:otherwise>
    </xsl:choose>-->

    <!-- Valg Tabellen -->
    <xsl:call-template name="table_header"/>
    <xsl:apply-templates select="rapport"/>
    <xsl:if test="$tabelltype='ov' and $rapportnavn!='K02' and $rapportnavn!='F04' and $rapportnavn!='ST02' and $rapportnavn !='ST03' and $rapportnavn != 'ST04'and $rapportnavn != 'ST05'and $rapportnavn != 'ST06'">
      <xsl:call-template name="sumandre"/>
    </xsl:if>

    <xsl:call-template name="sum"/>

    <xsl:if test="$tabelltype='ov' and $rapportnavn!='K02' and $rapportnavn!='ST02' and $rapportnavn != 'ST03'">
      <xsl:apply-templates select="rapport" mode="status"/>
    </xsl:if>

    <xsl:if test="$tabelltype='se'">
      <xsl:apply-templates select="rapport" mode="status-bunn"/>
    </xsl:if>

    <xsl:if test="$rapportnavn='K02' or $rapportnavn='ST02' or $rapportnavn='ST03'">
      <xsl:apply-templates select="rapport" mode="status-bunn"/>
    </xsl:if>

    <xsl:if test="$aggregate = 'MAIN'">
      <xsl:text>ERSTATT</xsl:text>
    </xsl:if>

    <xsl:if test="$aggregate != 'BODY'">

      <xsl:text>&lt;ParaStyle:NTB-PRI&gt;</xsl:text>
      <xsl:value-of select="$urgency"/>
      <xsl:value-of select="$crlf"/>

      <xsl:text>&lt;ParaStyle:NTB-KAT&gt;385</xsl:text>
      <xsl:value-of select="$crlf"/>

    </xsl:if>

  </xsl:template>

  <!-- Template for tabellen -->
  <xsl:template match="rapport">
    <xsl:choose>
      <!-- K02 F04 -->
      <xsl:when test="$rapportnavn='K02' or $rapportnavn='F04'">
        <xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] != 0]" mode="K02"/>
      </xsl:when>
      <!-- F05 -->
      <xsl:when test="$rapportnavn='F05'">
        <xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] &lt; 3 and data[@navn='Partikategori'] != 0]" mode="F05"/>
      </xsl:when>
      <!-- F02 F03 K03 -->
      <xsl:when test="$tabelltype='se'">
        <xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] != 0]" mode="se"/>
      </xsl:when>
      <!-- ST02 ST03 ST04 ST06-->
      <xsl:when test="$rapportnavn='ST02' or $rapportnavn='ST03' or $rapportnavn='ST04' or $rapportnavn='ST06' or $rapportnavn='ST05'">
        <xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] &lt; 2 and data[@navn='Partikode'] != 'Andre2']" mode="ST02"/>
      </xsl:when>
      <!-- K05 K04 -->
      <xsl:otherwise>
        <xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] &lt; 3 and data[@navn='Partikategori'] != 0]" mode="K05"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Template for kolonner i tabellen K02-->
  <xsl:template match="liste" mode="K02">
    <!-- K02 F04 -->
    <xsl:text>&lt;ParaStyle:NTB-TAB&gt;</xsl:text>
    <xsl:value-of select="data[@navn='Partikode']"/>
    <xsl:value-of select="$tab"/>
    <xsl:if test="data[@navn='AntStemmer']!=''">
      <xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
    </xsl:if>
    <xsl:value-of select="$tab"/>
    <xsl:choose>
      <xsl:when test="$rapportnavn='K02'">
        <xsl:value-of select="data[@navn='ProSt']"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="data[@navn='DiffPropFKsv']"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="data[@navn='DiffPropFStv']"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="data[@navn='ProgAntMndt']"/>
        <xsl:if test="data[@navn='DiffProgAntMndt']!=''">
          <xsl:text> (</xsl:text>
          <xsl:value-of select="data[@navn='DiffProgAntMndt']"/>
          <xsl:text>)</xsl:text>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <!-- F04 -->
        <xsl:value-of select="data[@navn='ProgProSt']"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="data[@navn='DiffProgPropFFtv']"/>
        <xsl:value-of select="$tab"/>

        <xsl:value-of select="data[@navn='DiffProgPropFStv']"/>
        <xsl:value-of select="$tab"/>

        <xsl:value-of select="data[@navn='ProgAntMndtFtv']"/>
        <xsl:if test="data[@navn='DiffProgAntMndtFFtv']!=''">
          <xsl:text> (</xsl:text>
          <xsl:value-of select="data[@navn='DiffProgAntMndtFFtv']"/>
          <xsl:text>)</xsl:text>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <!-- Template for kolonner i tabellen F05-->
  <xsl:template match="liste" mode="F05">
    <!-- F05 -->
    <xsl:text>&lt;ParaStyle:NTB-TAB&gt;</xsl:text>
    <xsl:value-of select="data[@navn='Partikode']"/>
    <xsl:value-of select="$tab"/>
    <xsl:if test="data[@navn='AntStemmer']!=''">
      <xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
    </xsl:if>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="data[@navn='ProgProSt']"/>
    <xsl:value-of select="$tab"/>

    <xsl:value-of select="data[@navn='DiffProgPropFFtv']"/>
    <xsl:value-of select="$tab"/>

    <xsl:value-of select="data[@navn='DiffProgPropFStv']"/>
    <!--
	<xsl:value-of select="$tab"/>
	<xsl:value-of select="data[@navn='ProgAntMndtStv']"/>
	<xsl:if test="data[@navn='DiffProgAntMndtStv']!=''">
		<xsl:text> (</xsl:text>
		<xsl:value-of select="data[@navn='DiffProgAntMndtStv']"/>
		<xsl:text>)</xsl:text>
	</xsl:if>		
-->
    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <!-- Template for kolonner i tabellen se-->
  <xsl:template match="liste" mode="se">
    <!-- F02 F03 K03 -->
    <xsl:text>&lt;ParaStyle:NTB-TAB&gt;</xsl:text>
    <xsl:value-of select="data[@navn='Partikode']"/>
    <xsl:value-of select="$tab"/>
    <xsl:if test="data[@navn='AntStemmer']!=''">
      <xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
    </xsl:if>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="data[@navn='ProSt']"/>
    <xsl:value-of select="$tab"/>

    <xsl:choose>
      <xsl:when test="$rapportnavn='K03'">
        <xsl:value-of select="data[@navn='DiffPropFKsv']"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="data[@navn='DiffPropFStv']"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="data[@navn='DiffPropFFtv']"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="data[@navn='DiffPropFStv']"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <!-- Template for kolonner i tabellen K05-->
  <xsl:template match="liste" mode="K05">
    <!-- K05 K04-->
    <xsl:text>&lt;ParaStyle:NTB-TAB&gt;</xsl:text>
    <xsl:value-of select="data[@navn='Partikode']"/>
    <xsl:value-of select="$tab"/>
    <xsl:if test="data[@navn='AntStemmer']!=''">
      <xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
    </xsl:if>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="data[@navn='ProSt']"/>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="data[@navn='DiffPropFKsv']"/>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="data[@navn='DiffPropFStv']"/>
    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <!-- Template for kolonner i tabellen ST02 ST04 ST06-->
  <xsl:template match="liste" mode="ST02">
    <!-- ST02 -->
    <xsl:text>&lt;ParaStyle:NTB-TAB&gt;</xsl:text>
    <xsl:value-of select="data[@navn='Partikode']"/>
    <xsl:value-of select="$tab"/>
    <xsl:if test="data[@navn='AntStemmer']!=''">
      <xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
    </xsl:if>
    <xsl:value-of select="$tab"/>
    <xsl:value-of select="data[@navn='ProSt']"/>
    <xsl:value-of select="$tab"/>

    <xsl:choose>
      <xsl:when test="$rapportnavn='ST02' or $rapportnavn='ST03'">
        <xsl:value-of select="data[@navn='DiffPropFStv']"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="data[@navn='DiffPropFFtv']"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='ST05'">
        <xsl:value-of select="data[@navn='DiffPropFStv']"/>
        <xsl:value-of select="$tab"/>
        <xsl:value-of select="data[@navn='DiffPropFKsv']"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$prognose = 'false'">
            <xsl:value-of select="data[@navn='DiffPropFStv']"/>
            <xsl:value-of select="$tab"/>
            <xsl:value-of select="data[@navn='DiffPropFFtv']"/>
            <xsl:value-of select="$tab"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="data[@navn='DiffProgPropFStv']"/>
            <xsl:value-of select="$tab"/>
            <xsl:value-of select="data[@navn='DiffProgPropFFtv']"/>
            <xsl:value-of select="$tab"/>
          </xsl:otherwise>
        </xsl:choose>

        <xsl:if test="data[@navn='ProgAntMndtStv'] != ''">
              <xsl:value-of select="data[@navn='ProgAntMndtStv']"/>
              <xsl:text>(</xsl:text>
              <xsl:choose>
                <xsl:when test="data[@navn='DiffProgAntMndtFStv'] = '+0'">
                  <xsl:text>-</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="data[@navn='DiffProgAntMndtFStv']"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:text>)</xsl:text>
            </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <!-- Template for tabellhode i tabellen -->
  <xsl:template name="table_header">
    <xsl:text>&lt;ParaStyle:NTB-TABHODE&gt;</xsl:text>
    <xsl:text>Parti</xsl:text>
    <xsl:value-of select="$tab"/>
    <xsl:text>Stemmer</xsl:text>
    <xsl:value-of select="$tab"/>
    <xsl:text>Andel %</xsl:text>
    <xsl:value-of select="$tab"/>

    <xsl:choose>
      <xsl:when test="$rapportnavn='K05' or $rapportnavn='K04'">
        <xsl:text>15-11</xsl:text>
        <xsl:value-of select="$tab"/>
        <xsl:text>15-13</xsl:text>
      </xsl:when>
      <xsl:when test="$rapportnavn='F05' or $rapportnavn='F02'">
        <xsl:text>15-11</xsl:text>
        <xsl:value-of select="$tab"/>
        <xsl:text>15-13</xsl:text>
        <!--
			<xsl:value-of select="$tab"/>
			<xsl:text>Mand. tenkt St.valg</xsl:text>
			-->
      </xsl:when>
      <xsl:when test="$rapportnavn='K02' or $rapportnavn='K03' or $rapportnavn='F04'">
        <xsl:text>15-11</xsl:text>
        <xsl:value-of select="$tab"/>
        <xsl:text>15-13</xsl:text>
        <xsl:if test="$tabelltype='ov'">
          <xsl:value-of select="$tab"/>
          <xsl:text>Mand.</xsl:text>
        </xsl:if>
      </xsl:when>
      <xsl:when test="$rapportnavn='ST02' or $rapportnavn='ST03' or $rapportnavn='ST05'">
        <xsl:text>13-09</xsl:text>
        <xsl:value-of select="$tab"/>
        <xsl:text>13-11</xsl:text>
      </xsl:when>
      <xsl:when test="$rapportnavn='ST04' or $rapportnavn='ST06'">
        <xsl:text>13-09</xsl:text>
        <xsl:value-of select="$tab"/>
        <xsl:text>13-11</xsl:text>
        <xsl:value-of select="$tab"/>
        <xsl:text>Mand.</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>15-13</xsl:text>
        <xsl:if test="$tabelltype='ov'">
          <xsl:value-of select="$tab"/>
          <xsl:text>Mand.</xsl:text>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <!-- Template for sum i tabellen -->
  <xsl:template name="sum">
    <xsl:text>&lt;ParaStyle:NTB-TAB&gt;</xsl:text>
    <xsl:text>Sum</xsl:text>
    <xsl:value-of select="$tab"/>

    <!--<xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikode'] != 'Andre']/data[@navn='AntStemmer']), '### ##0', 'no')"/>-->
    <xsl:value-of select="format-number(/respons/rapport/data[@navn='AntFrammotte'], '### ##0', 'no')"/>

    <xsl:value-of select="$tab"/>

    <xsl:value-of select="$tab"/>
    <xsl:if test="$tabelltype='ov'">
      <xsl:value-of select="$tab"/>
    </xsl:if>
    <xsl:if test="$rapportnavn='K02' or $rapportnavn='K03' or $rapportnavn='F04' or $rapportnavn='F02'">
      <xsl:value-of select="$tab"/>
    </xsl:if>
    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <!-- Template for sum i tabellen -->
  <xsl:template name="sumandre">
    <xsl:text>&lt;ParaStyle:NTB-TAB&gt;</xsl:text>
    <xsl:text>Andre</xsl:text>
    <xsl:value-of select="$tab"/>

    <xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikategori'] = '3']/data[@navn='AntStemmer']), '### ##0', 'no')"/>

    <xsl:value-of select="$tab"/>

    <xsl:value-of select="$tab"/>
    <xsl:if test="$tabelltype='ov'">
      <xsl:value-of select="$tab"/>
    </xsl:if>
    <xsl:if test="$rapportnavn='K02' or $rapportnavn='K03' or $rapportnavn='F04' or $rapportnavn='F02'">
      <xsl:value-of select="$tab"/>
    </xsl:if>

    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <xsl:variable name="crlf">
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:variable>

  <xsl:variable name="tab">
    <xsl:text>&#9;</xsl:text>
  </xsl:variable>

  <xsl:variable name="date">
    <xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
  </xsl:variable>

  <xsl:variable name="time">
    <xsl:value-of select="/respons/rapport/data[@navn='SisteRegTid']"/>
  </xsl:variable>

  <xsl:variable name="dato">
    <xsl:value-of select="substring($date, 7, 2)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 5, 2)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 1, 4)"/>
  </xsl:variable>

  <xsl:variable name="tid">
    <xsl:value-of select="substring($time, 1, 5)"/>
  </xsl:variable>

  <xsl:variable name="nitfdate">
    <xsl:value-of select="$date"/>
    <xsl:text>T</xsl:text>
    <xsl:value-of select="substring($time, 1, 2)"/>
    <xsl:value-of select="substring($time, 4, 2)"/>
    <xsl:value-of select="substring($time, 7, 2)"/>
  </xsl:variable>

  <xsl:variable name="rapport">
    <xsl:value-of select="substring-after(/respons/rapport/rapportnavn, '0')"></xsl:value-of>
  </xsl:variable>

  <xsl:variable name="rapporttype">
    <xsl:choose>
      <xsl:when test="$rapport='2'">KOM</xsl:when>
      <xsl:when test="$rapport='3'">KRE</xsl:when>
      <xsl:otherwise>OVS</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="urgency">
    <xsl:choose>
      <xsl:when test="$rapport ='5' or $rapport ='4' or $rapport = '6'">4</xsl:when>
      <xsl:otherwise>6</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="fylke">
    <xsl:value-of select="/respons/rapport/data[@navn='FylkeNavn']"/>
  </xsl:variable>

  <xsl:variable name="kommune">
    <xsl:value-of select="/respons/rapport/data[@navn='KommNavn']"/>
  </xsl:variable>

  <xsl:variable name="kommunenr">
    <xsl:value-of select="/respons/rapport/data[@navn='KommNr']"/>
  </xsl:variable>

  <xsl:variable name="kretsnr">
    <xsl:value-of select="/respons/rapport/data[@navn='KretsNr']"/>
  </xsl:variable>

  <xsl:variable name="bydelnr">
    <xsl:value-of select="/respons/rapport/data[@navn='BydelNr']"/>
  </xsl:variable>

  <xsl:variable name="bydel">
    <xsl:choose>
      <xsl:when test="/respons/rapport/data[@navn='BydelNr'] = '00'">
        <xsl:text>Forhåndsstemmer</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="/respons/rapport/data[@navn='BydelNavn']"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="krets">
    <xsl:choose>
      <xsl:when test="/respons/rapport/data[@navn='KretsNr'] = 'VIRT'">
        <xsl:text>Forhåndsstemmer</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="/respons/rapport/data[@navn='KretsNavn']"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="statusind">
    <xsl:value-of select="/respons/rapport/data[@navn='StatusInd']"/>
  </xsl:variable>

  <xsl:variable name="valgtype">
    <xsl:choose>
      <xsl:when test="substring-before($rapportnavn, '0') = 'F'">
        <xsl:text>Fylkestingsvalg</xsl:text>
      </xsl:when>
      <xsl:when test="substring-before($rapportnavn, '0') = 'ST'">
        <xsl:text>Stortingsvalg</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Kommunestyrevalg</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="stedsnavn">
    <!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
    <xsl:choose>
      <xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
        <xsl:value-of select="$kommune"/>
        <xsl:text> i </xsl:text>
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
        <xsl:value-of select="$krets"/>
        <xsl:text> i </xsl:text>
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:otherwise>Landsoversikt</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="rapport-type">
    <!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
    <xsl:choose>
      <xsl:when test="$rapportnavn='K02' or $rapportnavn='F02' or $rapportnavn='ST02'">
        <!--<xsl:text>komm-</xsl:text>-->
        <xsl:value-of select="$kommunenr"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K04' or $rapportnavn='F04' or $rapportnavn = 'ST04'">
        <!--<xsl:text>fylke-</xsl:text>-->
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K03' or $rapportnavn='F03' or $rapportnavn='ST03'">
        <!--<xsl:text>krets-</xsl:text>-->
        <xsl:value-of select="$kommune"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$krets"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K05' or $rapportnavn='F05' or $rapportnavn='ST06'">
        <xsl:text>landsoversikt</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="tabelltype">
    <xsl:choose>
      <xsl:when test="$rapportnavn = 'K02'">
        <xsl:text>ov</xsl:text>
      </xsl:when>
      <xsl:when test="substring($rapportnavn, 2, 2) = '02' or substring($rapportnavn, 2, 2) = '03'">
        <xsl:text>se</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>ov</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="infolinje">
    <!--<xsl:value-of select="$valgtype"/>
    <xsl:text>: </xsl:text>-->
    <xsl:choose>
      <!--
		<xsl:when test="$rapportnavn = 'F01' or $rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner</xsl:text>
		</xsl:when>
		-->
      <xsl:when test="$rapportnavn = 'F02' or $rapportnavn = 'K02' or $rapportnavn = 'ST02'">
        <!--<xsl:text></xsl:text>
        <xsl:value-of select="$fylke"/>
        <xsl:text>, </xsl:text>-->
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'F03' or $rapportnavn = 'K03' or $rapportnavn='ST03'">
        <xsl:value-of select="$krets"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'K08' or $rapportnavn = 'ST05'">
        <xsl:value-of select="$bydel"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'F04' or $rapportnavn = 'K04' or $rapportnavn = 'ST04'">
        <!--<xsl:text>Fylkesoversikt </xsl:text>-->
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'F05' or $rapportnavn = 'K05' or $rapportnavn = 'ST06'">
        <xsl:text>Landsoversikt</xsl:text>
        <xsl:text>, kl. </xsl:text>
        <xsl:value-of select="$tid"/>
      </xsl:when>
      <!--
		<xsl:when test="$rapportnavn = 'F07' or $rapportnavn = 'K07'">
			<xsl:text>Landsoversikt pr. fylke</xsl:text>
		</xsl:when>
      <xsl:when test="$rapportnavn = 'K08' or $rapportnavn = 'ST05'">
        <xsl:text>Bydelsresultater i Oslo</xsl:text>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'K09'">
        <xsl:text>Bystyreoversikt</xsl:text>
      </xsl:when>
    -->
    </xsl:choose>
<!--
    <xsl:if test="$prognose = 'true'">
      <xsl:text>, prognose</xsl:text>
    </xsl:if>


    <xsl:text>, kl. </xsl:text>
    <xsl:value-of select="$tid"/>
    <xsl:if test="$statusind &gt; 5">
      <xsl:text>. Korrigert resultat</xsl:text>
    </xsl:if>
		-->
  </xsl:variable>

  <xsl:variable name="prognose">
    <xsl:value-of select=
		"/respons/rapport/tabell/liste[data[@navn='Partikode'] = 'A']/data[@navn='ProgProSt'] != '' and
		(/respons/rapport/data[@navn='TotAntKomm'] - /respons/rapport/data[@navn='AntKommVtstOpptalt'] - /respons/rapport/data[@navn='AntKommAltOpptalt']) != 0"/>
  </xsl:variable>

  <xsl:template match="rapport" mode="status">
    <!--
		435 av 435 kommuner (435 ferdig opptalt).€
		100,0 prosent av ant. stemmeberettigede.€
		2521781 opptalte stemmer.€
		Frammøte: 39,3 prosent.€
	-->
    <xsl:if test="data[@navn='TotAntKomm']">
      <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
      <xsl:value-of select="data[@navn='AntKommFhstOpptalt'] + data[@navn='AntKommVtstOpptalt'] + data[@navn='AntKommAltOpptalt']"/>
      <xsl:text> av </xsl:text>
      <xsl:value-of select="data[@navn='TotAntKomm']"/>
      <xsl:text> kommuner (</xsl:text>
      <xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
      <xsl:text> ferdig opptalt).</xsl:text>
      <xsl:value-of select="$crlf"/>
    </xsl:if>

    <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
    <xsl:text>Omfatter </xsl:text>
    <xsl:if test="data[@navn='AntStBerett']">
      <xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStBerett'] * 100), '0.0', 'pros')"/>
    </xsl:if>
    <xsl:if test="data[@navn='AntStberett']">
      <xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStberett'] * 100), '0.0', 'pros')"/>
    </xsl:if>
    <xsl:text> prosent av </xsl:text>
    <xsl:choose>
      <xsl:when test="data[@navn='AntStBerett']">
        <xsl:value-of select="format-number(data[@navn='AntStBerett'], '### ### ###', 'no')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number(data[@navn='AntStberett'], '### ### ###', 'no')"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text> stemmeberettigede.</xsl:text>
    <xsl:value-of select="$crlf"/>

    <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
    <xsl:value-of select="format-number(data[@navn='AntFrammotte'], '### ### ###', 'no')"/>
    <xsl:text> opptalte stemmer. </xsl:text>

    <xsl:text>Frammøte: </xsl:text>
    <xsl:value-of select="data[@navn='ProFrammotte']"/>
    <xsl:text> prosent.</xsl:text>
    <xsl:value-of select="$crlf"/>

  </xsl:template>

  <xsl:template match="rapport" mode="status-bunn">
    <!--
		Antall st.ber.: 1234.€
		Frammøte: 39,3 prosent.€
		Alle forhåndsstemmer fordelt.
	-->

    <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
    <xsl:text>Antall st.ber.: </xsl:text>
    <xsl:if test="data[@navn='AntStBerett']">
      <xsl:value-of select="data[@navn='AntStBerett']"/>
    </xsl:if>
    <xsl:if test="data[@navn='AntStberett']">
      <xsl:value-of select="data[@navn='AntStberett']"/>
    </xsl:if>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="$crlf"/>

    <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
    <xsl:text>Frammøte: </xsl:text>
    <xsl:value-of select="data[@navn='ProFrammotte']"/>
    <xsl:text> prosent.</xsl:text>
    <xsl:value-of select="$crlf"/>

    <xsl:text>&lt;ParaStyle:NTB-TXT&gt;</xsl:text>
    <xsl:choose>
      <!--
		0.	Ingen resultater innsendt
		1.	Bare foreløpige forhåndsstemmer innsendt
		* 2.	Bare endelige fhst. innsendt
		? 3.	Bare foreløpige valgtingsstemmer innsendt
		* 4.	Bare endelige vtst. innsendt
		5.	Foreløpige fhst. og foreløpige vtst. innsendt
		6.	Endelige fhst. og foreløpige vtst. innsendt
		7.	Foreløpige fhst. og endelige vtst. innsendt
		8.	Endelige fhst. og endelige vtst. innsendt
		
		Mulige tekster:
			11 forhåndsstemmer ikke fordelt.
			Alle forhåndsstemmer fordelt.
			Alt opptalt untatt noen forhåndsstemmer.
			Alt opptalt?
		-->

      <xsl:when test="$statusind = '5'">
        <xsl:text>Foreløpig resultat.</xsl:text>
      </xsl:when>
      <xsl:when test="$statusind = '6'">
        <xsl:text>Alle forhåndsstemmer opptalt.</xsl:text>
      </xsl:when>
      <xsl:when test="$statusind = '7'">
        <xsl:text>Alt opptalt untatt enkelte forhåndsstemmer.</xsl:text>
      </xsl:when>
      <xsl:when test="$statusind = '8'">
        <xsl:text>Alt opptalt.</xsl:text>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$crlf"/>

  </xsl:template>

</xsl:stylesheet>