<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="yes"/>
<xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>

<xsl:param name="navn"></xsl:param>
<xsl:param name="Partikategori">1</xsl:param>
<xsl:param name="status">0</xsl:param>
  <xsl:param name="head">no</xsl:param>

  <xsl:variable name="rapportnavn">
    <xsl:value-of select="/respons/rapport/rapportnavn"/>
  </xsl:variable>

  <xsl:variable name="date">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
</xsl:variable>
  <xsl:variable name="time">
    <xsl:value-of select="/respons/rapport/data[@navn='SisteRegTid']"/>
  </xsl:variable>

  <xsl:variable name="dato">
	<xsl:value-of select="substring($date, 7, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 5, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 1, 4)"/>
</xsl:variable>

  <xsl:param name="ntbdato">
    <xsl:value-of select="$dato"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 1, 5)"/>
  </xsl:param>

  <xsl:param name="normdate">
    <xsl:value-of select="substring($date, 1, 4)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 5, 2)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 7, 2)"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$time"/>
  </xsl:param>

  <xsl:variable name="fylke">
    <xsl:value-of select="/respons/rapport/data[@navn='FylkeNavn']"/>
  </xsl:variable>

  <xsl:variable name="kommune">
    <xsl:value-of select="/respons/rapport/data[@navn='KommNavn']"/>
  </xsl:variable>
  <xsl:variable name="kommunenr">
    <xsl:value-of select="/respons/rapport/data[@navn='KommNr']"/>
  </xsl:variable>

  <xsl:variable name="krets">
    <xsl:value-of select="/respons/rapport/data[@navn='KretsNavn']"/>
  </xsl:variable>


  <xsl:template match="respons">
	<xsl:choose>
	<xsl:when test="$navn=''">
    <xsl:choose>
      <xsl:when test="$head='yes'">
        <html>
          <head>
            <xsl:call-template name="title"/>
            <xsl:call-template name="htmlhead"/>
          </head>
          <body>
            <xsl:call-template name="rapporthode"/>
            <xsl:apply-templates />
          </body>
        </html>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="rapporthode"/>
        <xsl:apply-templates />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:when>
	<xsl:otherwise>
		<xsl:call-template name="error"/>
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

  <xsl:template name="error">
	<h2>Resultatet for <xsl:value-of select="$navn"/> er ikke klart ennå!</h2>
</xsl:template>

<xsl:template match="Status">
</xsl:template>

<!-- Template for to tabeller i rapporten  -->
<xsl:template match="rapport">
	
	<table class="listehoy" border="1"  cellspacing="0"  cellpadding="3">
		<tr><th colspan="5">Kommuner med høyest frammøte</th></tr>
		<xsl:call-template name="tabellhode"/>
		<xsl:apply-templates select="tabell[@navn='K07tabell1']/liste"/>
	</table>

	<p/>

	<table class="listelav" border="1"  cellspacing="0"  cellpadding="3">
		<tr><th colspan="5">Kommuner med lavest frammøte</th></tr>
		<xsl:call-template name="tabellhode"/>
		<xsl:apply-templates select="tabell[@navn='K07tabell2']/liste"/>
	</table>

	<p/>

  <xsl:choose>
    <xsl:when test="$status=1">
      <table class="statusouter" border="1"  cellspacing="0"  cellpadding="0">
		<tr>
		<td valign="top">

      <table class="status" border="0"  cellspacing="0"  cellpadding="5">
        <xsl:apply-templates select="/respons/rapport/data"/>
      </table>

      <!--<table class="status" border="0"  cellspacing="0"  cellpadding="5">
			<xsl:apply-templates select="/respons/rapport/data[(position() div 2) - round(position() div 2) != 0]"/>
		</table>
		</td>
		<td valign="top">
		<table class="status" border="0"  cellspacing="0"  cellpadding="5">
			<xsl:apply-templates select="/respons/rapport/data[(position() div 2) - round(position() div 2) = 0]"/>
		</table>-->
		</td>
		</tr>
	</table>
    </xsl:when>
    <xsl:otherwise>
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>

<!-- Template for status-rader i rapporten  -->
<xsl:template match="/respons/rapport/data">
	<tr>
	<td><xsl:value-of select="@navn"/>:</td>
	<td align="right">
	<xsl:attribute name="class">
		<xsl:value-of select="@navn"/>
	</xsl:attribute>
	<xsl:value-of select="."/>
	</td>
	</tr>
</xsl:template>

<!-- Template for rader i tabellen -->
<xsl:template match="liste">
	<tr><xsl:apply-templates/></tr>
</xsl:template>

<!-- Template for kolonner i tabell header-->
<xsl:template name="tabellhode">
<tr>
	<th>Kommune</th>
	<th>Fylke</th>
	<th>Frammøte %</th>
	<th>Endring fra<br/>forrige Ft.valg</th>
	<th>Endring fra<br/>forrige St.valg</th>
</tr>
</xsl:template>

<!-- Template for ubrukte kolonner i tabellen -->
<xsl:template match="liste/data">
</xsl:template>

<!-- Template for kolonner i tabellen -->
<!--<xsl:template match="liste/data[@navn='Partikode' or @navn='AntStemmer' or @navn='ProSt' or @navn='DiffPropFFtv' or @navn='DiffPropFKsv' or @navn='DiffPropFStv' or @navn='DiffStFKsv' or @navn='DiffStFStv' or @navn='DiffStFFtv' or @navn='ProgAntMndtStv']">-->
<!--<xsl:template match="liste/data[@navn!='Partikategori' and @navn!='AntFhst' and @navn!='DiffProgPropFFtv' and @navn!='DiffProgPropFStv' and @navn!='DiffProgPropFKsv']">-->
<xsl:template match="liste/data">
	<td>
	<xsl:attribute name="class">
		<xsl:value-of select="@navn"/>
	</xsl:attribute>
	<xsl:choose>
		<xsl:when test="position() &gt; 2">
			<xsl:attribute name="align">right</xsl:attribute>
			<xsl:value-of select="."/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
	
	<xsl:if test=".=''">&#160;</xsl:if>
	</td>
</xsl:template>

<xsl:template name="rapporthode">
	<xsl:variable name="rapportnavn">
		<xsl:value-of select="rapport/rapportnavn"/>
	</xsl:variable>
	
	<h1>
	<xsl:value-of select="$rapportnavn"/>
	<xsl:text>: </xsl:text>

	<xsl:choose>
		<xsl:when test="$rapportnavn = 'F01'">
			<xsl:text>Opptalte kommuner - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F02'">
			<xsl:text>Enkeltresultat pr. kommune - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F03'">
			<xsl:text>Enkeltoversikt pr. krets - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F04'">
			<xsl:text>Fylkesoversikt - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F05'">
			<xsl:text>Landsoversikt pr. parti - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F07'">
			<xsl:text>Landsoversikt pr. fylke - fylkestingsvalg</xsl:text>
		</xsl:when>
		
		<xsl:when test="$rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K02'">
			<xsl:text>Enkeltresultat pr. kommune - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K03'">
			<xsl:text>Enkeltoversikt pr. krets - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K04'">
			<xsl:text>Fylkesoversikt - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K05'">
			<xsl:text>Landsoversikt pr. parti - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K07'">
			<xsl:text>Frammøte – 10 høyeste og 10 laveste - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST14'">
			<xsl:text>Frammøte – 10 høyeste og 10 laveste - stortingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K09'">
			<xsl:text>Bystyreoversikt - kommunevalg</xsl:text>
		</xsl:when>
	</xsl:choose>
	</h1>

	<!-- Skriv Fylkesnavn, Kommunenavn og Kretsnavn -->
	<xsl:choose>
		<xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
			<h2>
			<xsl:value-of select="rapport/data[@navn='KommNavn']"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
			</h2>
		</xsl:when>
		<xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
			<h2>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
			</h2>
		</xsl:when>
		<xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
			<h2>
			<xsl:value-of select="rapport/data[@navn='KretsNavn']"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="rapport/data[@navn='KommNavn']"/>
			</h2>
		</xsl:when>
	</xsl:choose>
	
	<p class="regdato"><b>Siste registrering: <xsl:value-of select="$dato"/>&#160;<xsl:value-of select="substring(rapport/data[@navn='SisteRegTid'], 1, 5)"/></b></p>

</xsl:template>

  <!-- Template for status-rader i rapporten  -->
  <xsl:template match="/respons/rapport/data">
    <tr>
      <xsl:attribute name="class">
        <xsl:value-of select="@navn"/>
      </xsl:attribute>
      <td class="ledetekst">
        <xsl:call-template name="ledetekst">
          <xsl:with-param name="navn">
            <xsl:value-of select="@navn"/>
          </xsl:with-param>
        </xsl:call-template>
      </td>
      <td class="colon">:</td>
      <td align="right">
        <xsl:attribute name="class">
          <xsl:value-of select="@navn"/>
        </xsl:attribute>
        <xsl:value-of select="."/>
      </td>
    </tr>
  </xsl:template>


  <!-- Template for endring av ledetekster -->
  <xsl:template name="ledetekst">
    <xsl:param name="navn"/>
    <xsl:choose>
      <xsl:when test="$navn='SisteRegDato'">
        <xsl:text>Registert dato</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='SisteRegTid'">
        <xsl:text>Registert tid</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='StatusInd'">
        <xsl:text>Indikator for opptellingsstatus</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='FylkeNr'">
        <xsl:text>Fylkenummer</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='FylkeNavn'">
        <xsl:text>Fylkenavn</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='KretsNr'">
        <xsl:text>Kretsummer</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='KretsNavn'">
        <xsl:text>Kretsnavn</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='KommNr'">
        <xsl:text>Kommunenummer</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='KommNavn'">
        <xsl:text>Kommunenavn</xsl:text>
      </xsl:when>

      <xsl:when test="$navn='DiffFrammFFtv'">
        <xsl:text>Endring i antall frammøtte i forhold til forrige fylkestingsvalg</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='DiffPropFrammFFtv'">
        <xsl:text>Endring i prosentpoeng for antall frammøtte i forhold til forrige fylkestingsvalg</xsl:text>
      </xsl:when>

      <xsl:when test="$navn='DiffFrammFKsv'">
        <xsl:text>Endring i ant. frammøtte i forhold til forrige kommunestyrevalg</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='DiffPropFrammFKsv'">
        <xsl:text>Endring i prosentpoeng for ant. frammøtte i forhold til forrige kommunestyrevalg</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='DiffFrammFStv'">
        <xsl:text>Endring i ant. frammøtte i forhold til forrige stortingsvalg</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='DiffPropFrammFStv'">
        <xsl:text>Endring i prosentpoeng for ant. frammøtte i forhold til forrige stortingsvalg</xsl:text>
      </xsl:when>

      <xsl:when test="$navn='TotAntKomm'">
        <xsl:text>Totalt antall kommuner</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommFhstOpptalt'">
        <xsl:text>Antall kommuner hvor bare forhåndsstemmer er opptalt.</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommVtstOpptalt'">
        <xsl:text>Antall kommuner hvor bare valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommAltOpptalt'">
        <xsl:text>Antall kommuner hvor både forhånds- og valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='TotAntKretser'">
        <xsl:text>Totalt antall kretser</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserFhstOpptalt'">
        <xsl:text>Antall kretser hvor bare forhåndsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserVtstOpptalt'">
        <xsl:text>Antall kretser hvor bare valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserAltOpptalt'">
        <xsl:text>Antall kretser hvor både forhånds- og valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>

      <xsl:when test="$navn='AntStBerett' or $navn='AntStberett'">
        <xsl:text>Antall stemmeberettigede</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntFrammotte'">
        <xsl:text>Antall frammøtte</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='ProFrammotte'">
        <xsl:text>Frammøteprosent</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntFhstOpptalt'">
        <xsl:text>Antall forhåndsstemmer</xsl:text>
      </xsl:when>

      <!-- Added for Valg2005 -->

      <xsl:when test="$navn='AntRepKvinner'">
        <xsl:text>Antall kvinnelige representanter </xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntRepMenn'">
        <xsl:text>Antall mannlige representanter </xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntRepUkjKjonn'">
        <xsl:text>Antall representanter hvor kjønnet er ukjent</xsl:text>
      </xsl:when>

      <!-- Added for Valg2009 -->
      <xsl:when test="$navn='StDevFylke'">
        <xsl:text>Standardavvik for fylket</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='ProgProStOpptalt'">
        <xsl:text>Prognose for prosentandel av stemmene som er opptalt til nå</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommAltOppt8'">
        <xsl:text>Antall kommuner hvor alle stemmer er opptalt og kontrollert</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserAltOppt8'">
        <xsl:text>Antall kretser hvor alle stemmer er opptalt og kontrollert</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='PartikodeMiste'">
        <xsl:text>Partikode for partiet som er nærmest å miste sistemandatet i fylket</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='PartikodeVinne'">
        <xsl:text>Partikode for partiet som er nærmest å vinne sistemandatet i fylket</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntStMiste'">
        <xsl:text>Antall stemmer partiet kan miste uten å miste sistemandatet</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntStVinne'">
        <xsl:text>Antall stemmer partiet må ha for å vinne sistemandatet</xsl:text>
      </xsl:when>


      <xsl:otherwise>
        <xsl:value-of select="$navn"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="htmlhead">
    <link href="Styles.css" type="text/css" rel="stylesheet" />
    <meta name="ntb-date">
      <xsl:attribute name="content">
        <xsl:value-of select="$ntbdato"/>
      </xsl:attribute>
    </meta>
    <meta name="status-ind">
      <xsl:attribute name="content">
        <xsl:value-of select="/respons/rapport/data[@navn='StatusInd']"/>
      </xsl:attribute>
    </meta>
    <meta name="sistereg">
      <xsl:attribute name="content">
        <xsl:value-of select="$normdate"/>
      </xsl:attribute>
    </meta>
    <meta name="rapportnavn">
      <xsl:attribute name="content">
        <xsl:value-of select="/respons/rapport/rapportnavn"/>
      </xsl:attribute>
    </meta>
    <meta name="fylkenr">
      <xsl:attribute name="content">
        <xsl:value-of select="/respons/rapport/data[@navn='FylkeNr']"/>
      </xsl:attribute>
    </meta>
    <meta name="fylkenavn">
      <xsl:attribute name="content">
        <xsl:value-of select="$fylke"/>
      </xsl:attribute>
    </meta>
    <meta name="kommunenr">
      <xsl:attribute name="content">
        <xsl:value-of select="/respons/rapport/data[@navn='KommNr']"/>
      </xsl:attribute>
    </meta>
    <meta name="kommunenavn">
      <xsl:attribute name="content">
        <xsl:value-of select="$kommune"/>
      </xsl:attribute>
    </meta>
    <meta name="kretsnr">
      <xsl:attribute name="content">
        <xsl:value-of select="/respons/rapport/data[@navn='KretsNr']"/>
      </xsl:attribute>
    </meta>
    <meta name="kretsnavn">
      <xsl:attribute name="content">
        <xsl:value-of select="$krets"/>
      </xsl:attribute>
    </meta>
  </xsl:template>

  <xsl:template name="title">
    <title>
      <xsl:choose>
        <xsl:when test="$rapportnavn = 'F01' or $rapportnavn = 'K01'">
          <xsl:text>Opptalte kommuner</xsl:text>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'F02' or $rapportnavn = 'K02' or $rapportnavn = 'ST02'">
          <xsl:text>Enkeltresultat </xsl:text>
          <xsl:value-of select="$kommune"/>
          <xsl:text> kommune</xsl:text>
          <!-- i <xsl:value-of select="$fylke"/>-->
        </xsl:when>
        <xsl:when test="$rapportnavn = 'F03' or $rapportnavn = 'K03' or $rapportnavn = 'ST03'">
          <xsl:text>Kretsoversikt </xsl:text>
          <xsl:value-of select="$krets"/> i <xsl:value-of select="$kommune"/>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'F04' or $rapportnavn = 'K04' or $rapportnavn = 'ST04'">
          <xsl:text>Fylkesoversikt </xsl:text>
          <xsl:value-of select="$fylke"/>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'F05' or $rapportnavn = 'K05' or $rapportnavn = 'ST06'">
          <xsl:text>Landsoversikt pr. parti</xsl:text>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'F07' or $rapportnavn = 'K07' or $rapportnavn = 'ST07'">
          <xsl:text>Landsoversikt pr. fylke</xsl:text>
        </xsl:when>

        <xsl:when test="$rapportnavn = 'K08'">
          <xsl:text>Bydelsresultater i Oslo</xsl:text>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'K09'">
          <xsl:text>Bystyreoversikt</xsl:text>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'ST05'">
          <xsl:text>Bydelsoversikt</xsl:text>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'ST14'">
          <xsl:text>Frammøte</xsl:text>
        </xsl:when>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="substring-before($rapportnavn, '0') = 'F'">
          <xsl:text> - fylkestingsvalg</xsl:text>
        </xsl:when>
        <xsl:when test="substring-before($rapportnavn, '0') = 'K'">
          <xsl:text> - kommunevalg</xsl:text>
        </xsl:when>
        <xsl:when test="substring-before($rapportnavn, '0') = 'ST'">
          <xsl:text> - stortingsvalg</xsl:text>
        </xsl:when>
        <xsl:when test="substring-before($rapportnavn, '1') = 'ST'">
          <xsl:text> - stortingsvalg</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text> - UKJENT</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </title>
  </xsl:template>

</xsl:stylesheet>