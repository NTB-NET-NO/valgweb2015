﻿Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl


Public Class Opptellingsstatus
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim toNotabene As Boolean = Request.UserHostAddress = "127.0.0.1" Or Request.UserHostAddress = "127.0.0.2" _
    Or Request.UserHostAddress.IndexOf(ConfigurationManager.AppSettings("allowedIPRange")) > -1 _
    Or Request.UserHostAddress.IndexOf(ConfigurationManager.AppSettings("allowedIPRange2")) > -1 _
    Or Request.UserHostAddress.IndexOf(ConfigurationManager.AppSettings("allowedIPRange3")) > -1 _
    Or Request.UserHostAddress.IndexOf(ConfigurationManager.AppSettings("allowedIPRange4")) > -1

        Dim sperret As Boolean = Session("sperret")
        If Not toNotabene And sperret And Date.Now < deadline Then
            'Brukere utenfor NTBs domene
            Response.Redirect("sperrefrist.htm")
            Exit Sub
        End If


        Dim fylke As String = "00"
        Dim argList As XsltArgumentList = New XsltArgumentList()

        If Page.IsPostBack Then
            Dim f As String = selector.SelectedValue
            argList.AddParam("FylkeNr", "", f)

        ElseIf Not String.IsNullOrEmpty(Request.QueryString("fylke")) Then
            Dim f As String = Request.QueryString("fylke")
            selector.SelectedValue = f
            argList.AddParam("FylkeNr", "", f)
        End If

        xmlTransform.TransformArgumentList = argList

        link.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='Opptellingsstatus.aspx?fylke=" & argList.GetParam("FylkeNr", "") & "'>Direkte lenke til denne oversikten</a>"

    End Sub

End Class