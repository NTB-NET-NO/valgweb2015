<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Partikoder.aspx.vb" Inherits="ValgWeb2015.Partikoder" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Partier og statuskoder</title>
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
    <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
    <meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <script language='JavaScript'>
			<!--
        this.focus();
			-->
    </script>
</head>
<body ms_positioning="GridLayout">
    <h1>
        Partikoder</h1>
    <form id="Form1" method="post" runat="server">
    <asp:Xml ID="Xml1" runat="server" TransformSource="XSLT/spPartiliste.xsl" DocumentSource="partiliste-2015.xml"></asp:Xml>
    </form>

    <!--
    Statusindikator for innrapportering har f�lgende verdier
    0.	Ingen resultater innsendt
    1.	Bare forel�pige forh�ndsstemmer innsendt
    2.	Endelige fhst. innsendt
    3.	Bare forel�pige valgtingsstemmer innsendt
    4.	Endelige vtst. innsendt
    5.	Forel�pige fhst. og forel�pige vtst. innsendt
    6.	Endelige fhst. og forel�pige vtst. innsendt
    7.	Forel�pige fhst. og endelige vtst. innsendt
    8.	Endelige fhst. og endelige vtst. Innsendt
    -->

    <h2>Statuskoder - opptelling</h2>
    <TABLE BORDER="1" BGCOLOR="#ffffff" CELLSPACING="0" CELLPADDING="3">
        <tr>
            <th style="width: 2cm">
                <xsl:text>Statuskode</xsl:text>
            </th>
            <th style="width: 12cm">
                <xsl:text>Forklaring</xsl:text>
            </th>
        </tr>
        <tr>
            <td align="center">0</td>
            <td>Ingen resultater innsendt</td>
        </tr>
        <tr>
            <td align="center">1</td>
            <td>Bare forel�pige forh�ndsstemmer innsendt</td>
        </tr>
        <tr>
            <td align="center">2</td>
            <td>Endelige forh�ndsstemmer innsendt</td>
        </tr>
        <tr>
            <td align="center">3</td>
            <td>Bare forel�pige valgtingsstemmer innsendt</td>
        </tr>
        <tr>
            <td align="center">4</td>
            <td>Endelige valgtingsstemmer innsendt</td>
        </tr>
        <tr>
            <td align="center">5</td>
            <td>Forel�pige forh�ndsstemmer og forel�pige valgtingsstemmer innsendt</td>
        </tr>
        <tr>
            <td align="center">6</td>
            <td>Endelige forh�ndsstemmer og forel�pige valgtingsstemmer innsendt</td>
        </tr>
        <tr>
            <td align="center">7</td>
            <td>Forel�pige forh�ndsstemmer og endelige valgtingsstemmer innsendt</td>
        </tr>
        <tr>
            <td align="center">8</td>
            <td>Endelige forh�ndsstemmer og endelige valgtingsstemmer Innsendt</td>
        </tr>
    </table>
</body>
</html>
