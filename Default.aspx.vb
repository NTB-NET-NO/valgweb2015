Imports System.Web.Security
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports System.IO
Imports System.Configuration.ConfigurationSettings

Public Class WebForm1
    Inherits System.Web.UI.Page

    Protected WithEvents Xml1 As System.Web.UI.WebControls.Xml
    Protected WithEvents Xml2 As System.Web.UI.WebControls.Xml
    Protected WithEvents Xml3 As System.Web.UI.WebControls.Xml
    Protected WithEvents Xml4 As System.Web.UI.WebControls.Xml


    Const ERR_FILE As String = "EMPTY.xml"
    'Const XSL_DETAIL As String = "xslt\Valg-xhtml_v2.xsl"
    Const XSL_DETAIL As String = "xslt\stortingsvalg-xhtml.xsl"
    'Const XSL_FRAMMOTE As String = "xslt\Valg-xhtml-frammote.xsl"
    Const XSL_FRAMMOTE As String = "xslt\stortingsvalg-xhtml-frammote.xsl"
    'Const XSL_FYLKE As String = "xslt\Valg-fylke-xhtml.xsl"
    Const XSL_FYLKE As String = "xslt\stortingsvalg-fylke-xhtml.xsl"
    'Const XSL_LISTE_BY As String = "xslt\byliste.xsl"
    Const XSL_LISTE_BY As String = "xslt\Valg2005-byliste.xsl"
    'Const XSL_VG_LISTE As String = "xslt\Valg-fylke-kommune-VG-xhtml.xsl"
    Const XSL_VG_LISTE As String = "xslt\stortingsvalg-fylke-kommune-VG-xhtml.xsl"
    Private toNotabene As Boolean

    Public buttons As Boolean = True

    Private bydelOslo As String = "no"
    Private xmlpath As String
    Private xml_F01 As String
    Private xml_F03 As String 
    Private xml_F05 As String 

    Private xml_K01 As String 
    Private xml_K03 As String 
    Private xml_K07 As String 
    Protected WithEvents Checkbox2 As System.Web.UI.WebControls.CheckBox
    Private xml_K09 As String
    'Stortings rapporter
    Private xml_ST05 As String ' Bydelsoversikt storting
    Private xml_K08 As String ' Bydelsoversikt kommune
    Private xml_ST06 As String ' Landsoversikt storting


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Initier()
        xmlpath = Session("xmlpath")

        xml_F01 = xmlpath & "\F01.xml"
        'xml_F01 = xmlpath & "\ST04.xml"

        xml_F03 = xmlpath & "\F03.xml"
        'xml_F03 = xmlpath & "\ST03.xml"

        xml_F05 = xmlpath & "\F05.xml"
        xml_ST06 = xmlpath & "\ST06.xml" 'replaces F05

        xml_K01 = xmlpath & "\K01.xml"
        'xml_K01 = xmlpath & "\ST04.xml"

        xml_K03 = xmlpath & "\K03.xml"

        xml_K07 = xmlpath & "\K07.xml"
        'xml_K07 = xmlpath & "\ST14.xml"

        xml_K09 = xmlpath & "\K09.xml"

        xml_ST05 = xmlpath & "\ST05.xml"
        xml_K08 = xmlpath & "\K08.xml"

        Me.Xml1.DocumentSource = xml_F05
        'Me.Xml1.DocumentSource = xml_ST06

        Me.Xml2.DocumentSource = xml_F01
        Me.Xml3.DocumentSource = xml_F03

        Me.Xml4.DocumentSource = xml_K08
        'Me.Xml4.DocumentSource = xml_ST05

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not String.IsNullOrEmpty(Request.QueryString("logout")) Then
            FormsAuthentication.SignOut()
            Response.Redirect(FormsAuthentication.LoginUrl)
        End If


        If (Not Request.Cookies(FormsAuthentication.FormsCookieName) Is Nothing) Then
            Dim ticket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(Request.Cookies(FormsAuthentication.FormsCookieName).Value)

            lblUser.Text = ticket.Name()
            lblUser.Font.Bold = True
            lblDate.Text = ticket.IssueDate
            lblExp.Text = ticket.Expiration
            lblIP.Text = Request.UserHostAddress
        End If

        Initier()

        'Reset a session variable on load
        Session("xmlFilesSorted") = Nothing

        Dim xmlFile As String
        Dim argList2 As XsltArgumentList = New XsltArgumentList()
        Dim argList1 As XsltArgumentList = New XsltArgumentList()

        toNotabene = Request.UserHostAddress = "127.0.0.1" Or Request.UserHostAddress = "127.0.0.2" _
            Or Request.UserHostAddress.IndexOf(ConfigurationManager.AppSettings("allowedIPRange")) > -1 _
            Or Request.UserHostAddress.IndexOf(ConfigurationManager.AppSettings("allowedIPRange2")) > -1 _
            Or Request.UserHostAddress.IndexOf(ConfigurationManager.AppSettings("allowedIPRange3")) > -1 _
            Or Request.UserHostAddress.IndexOf(ConfigurationManager.AppSettings("allowedIPRange4")) > -1

        Dim sperret As Boolean = Session("sperret")
        If Not toNotabene And sperret And Date.Now < deadline Then
            'Brukere utenfor NTBs domene
            Response.Redirect("sperrefrist.htm")
            Exit Sub
        End If

        'debug:
        'Dim test
        'For Each test In Request.Form
        '    Response.Write(test & "; ")
        'Next

        Dim tabelltype As String = Request.QueryString("tabelltype")
        If tabelltype <> "" Then
            argList1.AddParam("tabelltype", "", tabelltype)
        End If

        If IsPostBack = True Then
            Session("status") = Me.CheckBox1.Checked
        Else
            Me.CheckBox1.Checked = Session("status")
        End If

        Dim status As Boolean = Session("status")
        argList1.AddParam("status", "", status)


        Dim valgType As String = Request.QueryString("valgType")
        If valgType = "" Then
            valgType = Session("valgType")
            If valgType = "" Then
                valgType = "K"
                'valgType = "St"
            End If
        Else
            Session("valgType") = valgType
        End If

        argList2.AddParam("Valgtype", "", valgType)

        If valgType = "F" Then
            Me.Xml2.DocumentSource = xml_F01
            Me.Xml3.DocumentSource = xml_F03
            Me.Label1.Text = "Fylkestingsvalget" 'Me.Label1.Text = "Fylkestingsvalget"
            Me.Label3.Text = "<a href='Default.aspx?VisningIndeks=1&valgType=F'>Landsoversikt</a>"
        ElseIf valgType = "St" Then
            Me.Xml2.DocumentSource = xml_F01
            Me.Xml3.DocumentSource = xml_F03
            Me.Label1.Text = "Stortingsvalget"
            Me.Label3.Text = "<a href='Default.aspx?valgType=St'>Landsoversikt</a>"
        Else
            'Me.Xml2.DocumentSource = xml_F01
            Me.Xml2.DocumentSource = xml_K01
            'Me.Xml3.DocumentSource = xml_F03
            Me.Xml3.DocumentSource = xml_K03

            'Me.Label1.Text = "Stortingsvalget" 
            Me.Label1.Text = "Kommunevalget"

            Me.Label3.Text = "<a href='Default.aspx?valgType=K'>Landsoversikt</a><br/><a href='Opptellingsstatus.aspx' target='_blank'>Opptellingsstatus</a>"
            'Me.Label3.Text = "<a href='Default.aspx?VisningIndeks=1&valgType=St'>Landsoversikt</a>"
        End If

        Dim alle As String = Request.QueryString("Alle")
        Dim alleKretser As String = Request.QueryString("AlleKretser")
        Dim alleBydeler As String = Request.QueryString("AlleBydeler")
        Dim fylkeNr As String = Request.QueryString("FylkeNr")
        Dim liste As String = Request.QueryString("liste")
        Dim kommuneNr As String = Request.QueryString("KommuneNr")
        Dim fbyNr As String = Request.QueryString("FbyNr")
        Dim byNr As String = Request.QueryString("byNr")
        Dim bydelNr As String = Request.QueryString("bydelNr")
        Dim bydelBy As String = Request.QueryString("bydelBy")
        Dim kretsNr As String = Request.QueryString("kretsNr")
        Dim oversiktFylkeNr As String = Request.QueryString("OversiktFylkeNr") & ""
        Dim expand As String = Request.QueryString("Expand") & ""
        argList2.AddParam("Expand", "", expand)

        'fylkeNr = "03" Or
        If kommuneNr = "0301" Or bydelBy = "0301" Or byNr = "0301" Then

            valgType = "K" 'change to kommunevalg
            'valgType = "St"
        End If

        'Dim partiKategori As Integer = Session("partiKategori")
        'Dim partiKategoriForm As Integer = Request.Form("DropDownList1")

        'partiKategori = Session("partiKategori")
        'partiKategoriForm = Request.Form("DropDownList1")
        'partiKategoriQS = Request.QueryString("VisningIndeks")

        'If partiKategoriForm <> 0 Then
        '    partiKategori = partiKategoriForm
        'End If

        'If partiKategori = 0 Then
        '    partiKategori = 1
        'End If

        'Session("partiKategori") = partiKategori
        'Me.DropDownList1.SelectedIndex = partiKategori - 1

        Dim partiKategori As Integer = Nothing
        Dim partiKategoriForm As Integer = Nothing
        Dim partiKategoriQS As Integer = Nothing

        partiKategori = Session("partiKategori")
        partiKategoriForm = Request.Form("DropDownList1")
        partiKategoriQS = Request.QueryString("VisningIndeks")


        'Page.IsPostBack = True
        If Page.IsPostBack = True Then
            partiKategori = Session("partiKategori")
            partiKategoriForm = Request.Form("DropDownList1")

            If partiKategoriForm <> 0 Then
                partiKategori = partiKategoriForm
            End If

            If partiKategori = 0 Then
                partiKategori = 1
            End If

            Session("partiKategori") = partiKategori
            Me.DropDownList1.SelectedIndex = partiKategori - 1
        Else

            Dim visningsIndeks As Integer = Nothing

            partiKategori = Session("partiKategori")
            partiKategoriForm = Request.Form("DropDownList1")

            ' If partiKategoriForm <> 0 Then
            ' partiKategori = partiKategoriForm
            'End If

            If partiKategori = 0 Then
                partiKategori = 1
            End If

            DropDownList1.SelectedIndex = partiKategori - 1

            visningsIndeks = Request.QueryString("VisningIndeks")
            If (visningsIndeks <> 0) Then
                Session("partiKategori") = visningsIndeks
                DropDownList1.SelectedIndex = visningsIndeks - 1
                partiKategori = visningsIndeks
            End If
        End If

        argList1.AddParam("Partikategori", "", partiKategori)
        argList1.AddParam("ntbdato", "", Format(Now, "dd.MM.yyyy HH:mm"))

        Dim navn As String = Request.QueryString("Navn")
        Me.Xml1.TransformSource = XSL_DETAIL
        Me.Xml1.TransformArgumentList = argList1

        Dim argList3 As XsltArgumentList = New XsltArgumentList
        argList3.AddParam("valgtype", "", valgType)
        If byNr <> "" Or fbyNr <> "" Then
            argList3.AddParam("ByNr", "", byNr)
            If Not fbyNr Is Nothing Then
                argList3.AddParam("FNr", "", fbyNr)
            End If
        End If
        Me.Xml3.TransformArgumentList = argList3

        Dim argList4 As XsltArgumentList = New XsltArgumentList
        argList4.AddParam("valgtype", "", valgType)
        If bydelBy <> "" Then
            argList4.AddParam("BydelBy", "", bydelBy)
        End If
        Me.Xml4.TransformArgumentList = argList4

        If oversiktFylkeNr <> "" Or valgType = "M" Or liste = "07" Then
            ' Skjuler buttons for nedlasting av spesialformater
            buttons = False
        End If

        'Response.Write(FylkeNr)
        If valgType = "M" Then
            Me.Xml1.DocumentSource = xml_K07
            Me.Xml1.TransformSource = XSL_FRAMMOTE
            Session("valgType") = "K"
            'Session("valgType") = "St"
        ElseIf liste = "oslo" Then
            Me.Xml1.DocumentSource = xml_K09
            Me.Xml1.TransformSource = XSL_DETAIL
            'Session("valgType") = "St"

        ElseIf oversiktFylkeNr <> "" Then
            'Dim xmlFile As String = XML_K01
            'CheckXmlDoc(xmlFile, navn)
            'Dim argList As XsltArgumentList = New XsltArgumentList()

            If oversiktFylkeNr = "03" Then
                Me.Checkbox2.Visible = True

                If Me.Checkbox2.Checked Then
                    argList1.AddParam("bydel", "", "yes")
                End If
            End If

            ' argList1.AddParam("bydel", "", "yes")
            argList1.AddParam("FylkeNr", "", oversiktFylkeNr)
            argList1.AddParam("xmlpath", "", xmlpath)

            Me.Xml1.DocumentSource = xml_K01
            Me.Xml1.TransformSource = XSL_VG_LISTE
            Me.Xml1.TransformArgumentList = argList1

        ElseIf liste = "07" Then
            xmlFile = xmlpath & "\" & valgType & liste & ".xml"
            CheckXmlDoc(xmlFile, navn)
            Me.Xml1.TransformSource = XSL_FYLKE
        ElseIf liste <> "" Then
            xmlFile = xmlpath & "\" & valgType & liste & ".xml"
            CheckXmlDoc(xmlFile, navn)
        ElseIf kretsNr <> "" Then
            xmlFile = xmlpath & "\" & valgType & "03-" & byNr & "-" & kretsNr & ".xml"
            CheckXmlDoc(xmlFile, navn)
        ElseIf bydelNr <> "" Then
            xmlFile = xmlpath & "\" & valgType & "08-" & bydelNr & ".xml"
            'xmlFile = xmlpath & "\" & valgType & "05-" & bydelNr & ".xml"
            CheckXmlDoc(xmlFile, navn)
        ElseIf kommuneNr <> "" Then
            xmlFile = xmlpath & "\" & valgType & "02-" & kommuneNr & ".xml"
            CheckXmlDoc(xmlFile, navn)
            If fylkeNr <> "" Then
                'Dim argList As XsltArgumentList = New XsltArgumentList()
                argList2.AddParam("FylkeNr", "", fylkeNr)
                Me.Xml2.TransformArgumentList = argList2
            End If
        ElseIf fylkeNr <> "" Then
            xmlFile = xmlpath & "\" & valgType & "04-" & fylkeNr & ".xml"
            CheckXmlDoc(xmlFile, navn)
            argList2.AddParam("FylkeNr", "", fylkeNr)
            Me.Xml2.TransformArgumentList = argList2
        Else
            If valgType = "K" And partiKategori = 3 Then
                partiKategori = 2
                argList1.RemoveParam("Partikategori", "")
                argList1.AddParam("Partikategori", "", partiKategori)
                Me.DropDownList1.SelectedIndex = partiKategori - 1
                'Me.DropDownList1.Enabled = False
            End If

            'endret fra valg2003 til valg2005
            'xmlFile = xmlpath & "\" & valgType & "06" & ".xml"
            xmlFile = xmlpath & "\" & valgType & "05" & ".xml"
            CheckXmlDoc(xmlFile, navn)
        End If

        If alleBydeler <> "" Then
            VisAlleBydeler(valgType)

        ElseIf alleKretser <> "" Then
            VisAlleKretser(kommuneNr, byNr, valgType)

        ElseIf alle <> "" Then
            VisAlle(fylkeNr, valgType)
        Else
            Session("xsltFile") = Me.Xml1.TransformSource
        End If

        If toNotabene Then
            Try
                Dim test As String = Path.GetFileNameWithoutExtension(xmlFile).Substring(0, 3)
                Dim test2 As String = Path.GetFileNameWithoutExtension(xmlFile)
                If test = "F04" Or test = "F05" Or test2 = "K02-0301" Then
                    Me.Button1.Visible = False
                End If
            Catch
            End Try
        End If

        Session("xmlFile") = Me.Xml1.DocumentSource
        Session("argList") = Me.Xml1.TransformArgumentList

        'Me.Label2.Text = Request.UserHostAddress

    End Sub

    Private Sub CheckXmlDoc(ByVal xmlFile As String, ByVal navn As String)
        Dim testFile As String = Server.MapPath(xmlFile)
        If IO.File.Exists(testFile) Then
            Me.Xml1.DocumentSource = xmlFile
        Else
            Dim argList As XsltArgumentList = New XsltArgumentList()
            argList.AddParam("navn", "", navn)
            Me.Xml1.TransformArgumentList = argList
            Me.Xml1.DocumentSource = ERR_FILE
            Me.Label2.Text = xmlFile
            toNotabene = False
        End If
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Error
        Response.Write("Error")
    End Sub

    Protected WithEvents lblUser As System.Web.UI.WebControls.Label
    Protected WithEvents lblDate As System.Web.UI.WebControls.Label
    Protected WithEvents lblExp As System.Web.UI.WebControls.Label
    Protected WithEvents lblIP As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents Button3 As System.Web.UI.WebControls.Button
    Protected WithEvents Button2 As System.Web.UI.WebControls.Button
    Protected WithEvents Button4 As System.Web.UI.WebControls.Button
    Protected WithEvents ltOutputAll As System.Web.UI.WebControls.Literal

    Protected WithEvents CheckBox1 As System.Web.UI.WebControls.CheckBox

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim xmlFile As String = Session("xmlFile")
        Dim xmlFile2 As String = Path.GetFileNameWithoutExtension(xmlFile)
        xmlFile = Server.MapPath(xmlFile)
        Dim strTemp1 As String = DoXstlTransform(xmlFile, Server.MapPath("xslt/Valg2notabene-HTML.xsl"))
        WriteFile(Server.MapPath("valg-til-notabene\" & xmlFile2 & ".htm"), strTemp1)
    End Sub


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub

    Private Sub VisAlleKretser(ByVal KommuneNr As Integer, ByVal ByNr As Integer, ByVal ValgType As String)
        Dim xpathDocObject As XmlDocument
        Dim searchCriteria As String
        Dim KretsNodes As XmlNodeList
        Dim Krets As XmlNode
        Dim slKretsNodes As SortedList
        Dim byNrInFile As String
        Dim teller As Integer

        If KommuneNr < 1000 Then
            searchCriteria = "0" + KommuneNr.ToString()
        Else
            searchCriteria = KommuneNr.ToString()
        End If

        If ByNr < 1000 Then
            byNrInFile = "0" + ByNr.ToString()
        Else
            byNrInFile = ByNr.ToString()
        End If

        xpathDocObject = New XmlDocument()

        If ValgType = "St" Then
            xpathDocObject.Load(Server.MapPath(xml_F03))
        ElseIf ValgType = "F" Then
            xpathDocObject.Load(Server.MapPath(xml_F03))
        Else
            xpathDocObject.Load(Server.MapPath(xml_K03))
        End If

        KretsNodes = xpathDocObject.SelectNodes("/respons/rapport[data[@navn='KommNr']='" + searchCriteria + "']")

        Xml1.Transform = New XslTransform()
        Xml1.Transform.Load(Server.MapPath(XSL_DETAIL))
        Session("xsltFile") = XSL_DETAIL

        Dim memStrm As MemoryStream
        memStrm = New MemoryStream()
        slKretsNodes = New SortedList()

        For teller = 0 To KretsNodes.Count - 1
            Krets = KretsNodes(teller).SelectSingleNode("data[@navn='KretsNr']")
            Dim filename As String
            filename = "valg-xml-inn/" + ValgType + "03-" + byNrInFile + "-" + Krets.InnerText + ".xml"

            slKretsNodes.Add(KretsNodes(teller).SelectSingleNode("data[@navn='KretsNavn']").InnerText, filename)
        Next

        For Each Key As String In slKretsNodes.Keys
            Try
                Xml1.Transform.Transform(New XPathDocument(Server.MapPath(slKretsNodes(Key).ToString())), Session("argList"), memStrm)
            Catch ex As Exception
            End Try
        Next
        ltOutputAll.Text = System.Text.Encoding.GetEncoding("ISO-8859-1").GetString(memStrm.ToArray())

        Session("xmlFilesSorted") = slKretsNodes
    End Sub


    Private Sub VisAlle(ByVal FylkeNr As Integer, ByVal ValgType As String)
        Dim xpathDocObject As XmlDocument
        Dim searchCriteria As String
        Dim FylkeNodes As XmlNodeList
        Dim KommuneNodes As XmlNodeList
        Dim Kommune As XmlNode
        Dim slKommuneNodes As SortedList
        Dim teller As Integer
        Dim UseIndex As Integer

        If FylkeNr < 10 Then
            searchCriteria = "0" + FylkeNr.ToString()
        Else
            searchCriteria = FylkeNr.ToString()

        End If

        xpathDocObject = New XmlDocument()

        If ValgType = "St" Then
            xpathDocObject.Load(Server.MapPath(xml_F01))
        ElseIf ValgType = "F" Then
            xpathDocObject.Load(Server.MapPath(xml_F01))
        Else
            xpathDocObject.Load(Server.MapPath(xml_K01))
        End If


        'Finn index til riktig fylkenode
        FylkeNodes = xpathDocObject.SelectNodes("/respons/rapport")

        For teller = 0 To FylkeNodes.Count - 1
            Dim node1 As XmlNode
            node1 = FylkeNodes(teller).SelectSingleNode("data[@navn='FylkeNr']")

            If node1.InnerText.Equals(searchCriteria) Then
                UseIndex = teller
            End If
        Next

        'Hent alle kommunene under fylket
        KommuneNodes = FylkeNodes(UseIndex).SelectNodes("tabell/liste")

        Xml1.Transform = New XslTransform()
        Xml1.Transform.Load(Server.MapPath(XSL_DETAIL))
        Session("xsltFile") = XSL_DETAIL

        Dim memStrm As MemoryStream

        memStrm = New MemoryStream()

        slKommuneNodes = New SortedList()

        For teller = 0 To KommuneNodes.Count - 1
            Dim status As String
            Dim filename As String
            Dim key As String

            status = KommuneNodes(teller).SelectSingleNode("data[@navn='StatusInd']").InnerText

            Kommune = KommuneNodes(teller).SelectSingleNode("data[@navn='KommNr']")
            filename = "valg-xml-inn/" + ValgType + "02-" + Kommune.InnerText + ".xml"

            key = KommuneNodes(teller).SelectSingleNode("data[@navn='KommNavn']").InnerText

            If (Not slKommuneNodes.ContainsKey(key) And Not String.IsNullOrEmpty(status)) And status > "0" Then
                slKommuneNodes.Add(key, filename)
            End If
        Next

        For Each Key As String In slKommuneNodes.Keys
            Try
                Xml1.Transform.Transform(New XPathDocument(Server.MapPath(slKommuneNodes(Key).ToString())), Session("argList"), memStrm)
            Catch ex As Exception
            End Try
        Next

        ltOutputAll.Text = System.Text.Encoding.GetEncoding("ISO-8859-1").GetString(memStrm.ToArray())

        Session("xmlFilesSorted") = slKommuneNodes
    End Sub

    Private Sub VisAlleBydeler(ByVal ValgType As String)
        Dim xpathDocObject As XmlDocument
        'Dim searchCriteria As String
        Dim BydelNodes As XmlNodeList
        Dim Bydel As XmlNode
        Dim slBydelNodes As SortedList
        Dim teller As Integer

        xpathDocObject = New XmlDocument()

        If ValgType = "St" Then
            xpathDocObject.Load(Server.MapPath(xml_ST05))
        Else
            xpathDocObject.Load(Server.MapPath(xml_K08))
        End If

        BydelNodes = xpathDocObject.SelectNodes("/respons/rapport")

        Xml1.Transform = New XslTransform()
        Xml1.Transform.Load(Server.MapPath(XSL_DETAIL))
        Session("xsltFile") = XSL_DETAIL

        Dim memStrm As MemoryStream
        memStrm = New MemoryStream()
        slBydelNodes = New SortedList()

        For teller = 0 To BydelNodes.Count - 1
            Bydel = BydelNodes(teller).SelectSingleNode("data[@navn='BydelNr']")
            Dim filename As String
            filename = "valg-xml-inn/" + ValgType + "08-" + Bydel.InnerText + ".xml"
            'filename = "valg-xml-inn/" + ValgType + "05-" + Bydel.InnerText + ".xml"

            slBydelNodes.Add(BydelNodes(teller).SelectSingleNode("data[@navn='BydelNavn']").InnerText, filename)
        Next

        For Each Key As String In slBydelNodes.Keys
            Try
                Xml1.Transform.Transform(New XPathDocument(Server.MapPath(slBydelNodes(Key).ToString())), Session("argList"), memStrm)
            Catch ex As Exception
            End Try
        Next
        ltOutputAll.Text = System.Text.Encoding.GetEncoding("ISO-8859-1").GetString(memStrm.ToArray())

        Session("xmlFilesSorted") = slBydelNodes
    End Sub

End Class
