<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Default.aspx.vb" Inherits="ValgWeb2015.WebForm1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>NTB - Valg 2015</title>
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta http-equiv="Refresh" content="300">
		<meta content="Microsoft Visual Studio.NET 7.0" name="GENERATOR">
		<meta content="Visual Basic 7.0" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<!-- <TABLE id="Table1" style="Z-INDEX: 101; LEFT: 5px; WIDTH: 866px; POSITION: absolute; TOP: 7px; HEIGHT: 502px" cellSpacing="1" cellPadding="1" width="866" border="1"> -->
		<TABLE>
			<TR>
				<TD>
					<form action="DownLoadFile.aspx?fileType=html" method="post" target="show">
						<INPUT type="submit" value="Vis kun tabell" name="button">
					</form>
				</TD>
				<TD>
					<form action="DownLoadFile.aspx?fileType=xhtml" method="post" target="download">
						<INPUT type="submit" value="Last ned XHTML" name="button">
					</form>
				</TD>
				<td>
					<form action="DownLoadFile.aspx?fileType=xml" method="post" target="download">
						<INPUT type="submit" value="Last ned Raw XML" name="button">
					</form>
				</td>
				<%if buttons then%>
				<TD>
					<form action="DownLoadFile.aspx?fileType=ind" method="post" target="download">
						<INPUT type="submit" value="Last ned InDesign" name="button">
					</form>
				</TD>
				<TD>
					<form action="DownLoadFile.aspx?fileType=xtg" method="post" target="download">
						<INPUT type="submit" value="Last ned XTG-A" name="button">
					</form>
				</TD>
				<TD>
					<form action="DownLoadFile.aspx?fileType=xtgbt" method="post" target="download">
						<INPUT type="submit" value="Last ned XTG-B" name="button">
					</form>
				</TD>
				<!--<TD>
					<form action="DownLoadFile.aspx?fileType=iptc" method="post" target="download">
						<INPUT type="submit" value="Last ned IPTC" name="button">
					</form>
				</TD>-->
				<td>
					<form action="DownLoadFile.aspx?fileType=nitf" method="post" target="download">
						<INPUT type="submit" value="Last ned NITF XML" name="button">
					</form>
				</td>
				<%else%>
				<td colSpan="4"></td>
				<%end if%>
			</TR>
		</TABLE>
		<form id="Form2" method="post" runat="server">
			<TABLE cellSpacing="0" cellPadding="5" border="1">
				<TR>
					<TD class="NTB" vAlign="top" align="center" width="200">
						<P><IMG alt="NTB Valget" src="Images/ntb_logo.png"><BR>
							&nbsp;Valg 2015</P>
					</TD>
					<TD width="800">

						<table width="100%">
							<tr>
								<td align="center"><b><A href="Default.aspx?valgType=F&amp;VisningIndeks=1">Fylkestingsvalget</A></b></td>
								<td align="center"><b><A href="Default.aspx?valgType=F&amp;liste=07&amp;tabelltype=1">Landsoversikt 
											pr. fylke</A></b></td>
								<td align="center"><b><A href="Default.aspx?valgType=K&amp;VisningIndeks=1">Kommunevalget</A></b></td>
								<td align="center"><asp:dropdownlist id="DropDownList1" runat="server" Width="150px" AutoPostBack="True">
										<asp:ListItem Value="1" Selected="True">Etablerte partier</asp:ListItem>
										<asp:ListItem Value="2">Vis ogs� sm�partier</asp:ListItem>
										<asp:ListItem Value="3">Vis ogs� valglister</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr>
								<td align="center"><b><A href="Partikoder.aspx" target='_blank'>Partier og Statuskoder</A></b></td>
								<td align="center"><b><A href="Default.aspx?valgType=F&amp;liste=07&amp;tabelltype=2">Prognoser</A></b></td>
								<td align="center"><b><A href="Default.aspx?valgType=M">Framm�te</A></b></td>
								<td class="checkbox" align="center" colSpan="1"><asp:checkbox id=CheckBox1 runat="server" AutoPostBack="True" Text="Vis detaljer" Checked='<%# session("status") %>'></asp:checkbox>
                                    <asp:checkbox id=Checkbox2 runat="server" AutoPostBack="True" 
                                        Text="Vis bydeler i Oslo" Checked='True' Visible="False">
									</asp:checkbox></td>
							</tr>
						</table>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" width="200">
						<h3><asp:label id="Label1" runat="server"></asp:label></h3>
						<p><asp:label id="Label3" runat="server"></asp:label></p>
						<h4>Fylkes- og kommuneoversikter:</h4>
						<asp:xml id="Xml2" runat="server" TransformSource="xslt\fylkesliste.xsl" 
                            DocumentSource="xml-docs\ST04.xml"></asp:xml>
						<h4>Bydelsoversikter:</h4>
						<asp:xml id="Xml4" runat="server" TransformSource="xslt\bydelsliste.xsl" DocumentSource="xml-docs\ST07.xml"></asp:xml>
						<h4>By- og kretsoversikter:</h4>
						<asp:xml id="Xml3" runat="server" TransformSource="xslt\byliste.xsl" DocumentSource="xml-docs\F03.xml"></asp:xml></TD>
					<TD vAlign="top" width="800">
						<asp:xml id="Xml1" runat="server" TransformSource="xslt\Valg-xhtml.xsl" DocumentSource="xml-docs\ST06.xml"></asp:xml>
                            <asp:Literal ID="ltOutputAll" runat="server"></asp:Literal>
                        
						<P><asp:label id="Label2" runat="server"></asp:label></P>
						<p><asp:button id="Button1" runat="server" Text="Send til valgdesken i Notabene" Visible="False"></asp:button></p>
					</TD>
				</TR>
			</TABLE>
		</form>
		<TABLE>
			<TR>
				<TD>
					<form action="DownLoadFile.aspx?fileType=html" method="post" target="show">
						<INPUT type="submit" value="Vis kun tabell" name="button">
					</form>
				</TD>
				<TD>
					<form action="DownLoadFile.aspx?fileType=xhtml" method="post" target="download">
						<INPUT type="submit" value="Last ned XHTML" name="button">
					</form>
				</TD>
				<td>
					<form action="DownLoadFile.aspx?fileType=xml" method="post" target="download">
						<INPUT type="submit" value="Last ned Raw XML" name="button">
					</form>
				</td>
				<%if buttons then%>
				<TD>
					<form action="DownLoadFile.aspx?fileType=ind" method="post" target="download">
						<INPUT type="submit" value="Last ned InDesign" name="button">
					</form>
				</TD>
				<TD>
					<form action="DownLoadFile.aspx?fileType=xtg" method="post" target="download">
						<INPUT type="submit" value="Last ned XTG-A" name="button">
					</form>
				</TD>
				<TD>
					<form action="DownLoadFile.aspx?fileType=xtgbt" method="post" target="download">
						<INPUT type="submit" value="Last ned XTG-B" name="button">
					</form>
				</TD>
				<td>
					<form action="DownLoadFile.aspx?fileType=nitf" method="post" target="download">
						<INPUT type="submit" value="Last ned NITF XML" name="button">
					</form>
				</td>
				<%else%>
				<td colSpan="4"></td>
				<%end if%>
			</TR>
		</TABLE>
       <div>Innlogget som: <asp:Label ID="lblUser" runat="server"></asp:Label> (IP: <asp:Label ID="lblIP" runat="server"></asp:Label>)</div>
       <div>P�logget siden: <asp:Label ID="lblDate" runat="server"></asp:Label></div>
       <div>Varer til: <asp:Label ID="lblExp" runat="server"></asp:Label></div>
        <div><a href="?logout=true">Logg ut</a></div>
	</body>
</HTML>
