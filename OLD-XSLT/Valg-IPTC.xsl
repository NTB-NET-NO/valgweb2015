<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:user="http://ntb.no/mynamespace">

<!-- Valget IPTC format -->

<xsl:output encoding="ISO-8859-1" method="text" omit-xml-declaration="yes" indent="yes"/>

<xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>
<xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>
<xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>
<xsl:decimal-format name="pros" decimal-separator=","/>
<xsl:strip-space elements="*"/>

<xsl:param name="enable-fip-header">yes</xsl:param>
<xsl:param name="distribusjon">ALL</xsl:param>
<xsl:param name="prognose">yes</xsl:param>

<xsl:param name="ntbdato">
	<xsl:value-of select="$dato"/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="substring($time, 1, 5)"/>
</xsl:param>

<xsl:param name="navn"></xsl:param>
<xsl:param name="Partikategori">1</xsl:param>

<!-- Main template: -->
<xsl:template match="respons">
	<xsl:if test="$enable-fip-header='yes'">
		<xsl:call-template name="fipheader"/>
	</xsl:if>
	<xsl:value-of select="$urgency"/>
	<xsl:text> VLG </xsl:text>
	<xsl:value-of select="format-number(string-length(//*), '00000')"/>
	<xsl:text> </xsl:text>
	<!--<xsl:call-template name="undergruppe"/>-->
	<xsl:value-of select="$rapporttype"/>
	<xsl:value-of select="$crlf"/>

	<!-- stikkord -->
	<xsl:text>VLG-</xsl:text>
	<xsl:value-of select="$rapportnavn"/>-<xsl:value-of select="$rapport-type"/>
	<!--<xsl:text>&#10;</xsl:text>-->
	<xsl:value-of select="$crlf"/>

	<!-- Infolinje -->
	<xsl:text>&amp;#x02;&amp;#x98;</xsl:text>
	<xsl:value-of select="$infolinje"/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#x94;</xsl:text>
	
	<!-- Brødtekst 
	<xsl:text>&amp;#x9E;&amp;#x94;Sist registrert: </xsl:text>
	<xsl:value-of select="$ntbdato"/>
	<xsl:text>&amp;#xB6;</xsl:text>
	<xsl:value-of select="$crlf"/>
	-->
	
	<!-- Topptekster -->
	<xsl:choose>
		<xsl:when test="$rapport='4'">
			<xsl:text>&amp;#154;ov01&amp;#159;</xsl:text>
			<xsl:value-of select="$crlf"/>
			<xsl:choose>
			
				<xsl:when test="$rapportnavn='F04' and (rapport/data[@navn='TotAntKomm'] - rapport/data[@navn='AntKommVtstOpptalt'] - rapport/data[@navn='AntKommAltOpptalt'])!=0">
					<xsl:text>Fylkesoversikt, prognose, kl. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Fylkesoversikt, kl. </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="$tid"/>
			<xsl:value-of select="$crlf"/>		

			<xsl:text>&amp;#154;ov0&amp;#159;</xsl:text>
			<xsl:value-of select="$crlf"/>
			<xsl:value-of select="$fylke"/>
			<xsl:value-of select="$crlf"/>
		</xsl:when>
		<xsl:when test="$rapport='5'">
			<xsl:text>&amp;#154;ov0&amp;#159;</xsl:text>
			<xsl:value-of select="$crlf"/>
			<xsl:choose>
				<xsl:when test="$rapportnavn='F05' and (rapport/data[@navn='TotAntKomm'] - rapport/data[@navn='AntKommVtstOpptalt'] - rapport/data[@navn='AntKommAltOpptalt'])!=0">
					<xsl:text>Landsoversikt, prognose, kl. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Landsoversikt, kl. </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="$tid"/>
			<xsl:value-of select="$crlf"/>		
		</xsl:when>
		<xsl:when test="$rapportnavn='K02'">
			<xsl:text>&amp;#154;ov01&amp;#159;</xsl:text>
			<xsl:value-of select="$crlf"/>
			<xsl:value-of select="$fylke"/>
			<xsl:value-of select="$crlf"/>		

			<xsl:text>&amp;#154;ov0&amp;#159;</xsl:text>
			<xsl:value-of select="$crlf"/>
			<xsl:value-of select="$kommune"/>
			<xsl:value-of select="$crlf"/>
		</xsl:when>
		<xsl:when test="$rapport='2'">
			<xsl:text>&amp;#154;se0&amp;#159;</xsl:text>
			<xsl:value-of select="$crlf"/>
			<xsl:value-of select="$fylke"/>
			<xsl:value-of select="$crlf"/>		

			<xsl:text>&amp;#154;se1&amp;#159;</xsl:text>
			<xsl:value-of select="$crlf"/>
			<xsl:value-of select="$kommune"/>
			<xsl:value-of select="$crlf"/>
		</xsl:when>
		<xsl:when test="$rapport='3'">
			<xsl:text>&amp;#154;se0&amp;#159;</xsl:text>
			<xsl:value-of select="$crlf"/>
			<xsl:value-of select="$kommune"/>
			<xsl:value-of select="$crlf"/>		

			<xsl:text>&amp;#154;se1&amp;#159;</xsl:text>
			<xsl:value-of select="$crlf"/>
			<xsl:value-of select="$krets"/>
			<xsl:value-of select="$crlf"/>
		</xsl:when>
		<xsl:otherwise>
		</xsl:otherwise>
	</xsl:choose>

	<xsl:if test="$tabelltype='ov' and $rapportnavn!='K02'">
		<xsl:text>&amp;#154;ov00&amp;#159;</xsl:text>
		<xsl:value-of select="$crlf"/>
		<xsl:apply-templates select="rapport" mode="status"/>
	</xsl:if>

	<!-- Valg Tabellen -->
	<xsl:call-template name="table_header"/>
	<xsl:apply-templates select="rapport"/>
 	<xsl:if test="$tabelltype='ov' and $rapportnavn!='K02' and $rapportnavn!='F04'">
		<xsl:call-template name="sumandre"/>
	</xsl:if>		
	<xsl:call-template name="sum"/>

 	<xsl:if test="$tabelltype='se'">
		<xsl:text>&amp;#154;se5&amp;#159;</xsl:text>
		<xsl:value-of select="$crlf"/>
		<xsl:apply-templates select="rapport" mode="status-bunn"/>
	</xsl:if>

	<xsl:if test="$rapportnavn='K02'">
		<xsl:text>&amp;#154;ov4&amp;#159;</xsl:text>
		<xsl:value-of select="$crlf"/>
		<xsl:apply-templates select="rapport" mode="status-bunn"/>
	</xsl:if>

	<xsl:value-of select="$crlf"/>

	<xsl:text>&amp;#x03;</xsl:text>
<!--	<xsl:value-of select="user:($nitfdate)"/>-->
	<xsl:call-template name="iptc_date"/>
	<xsl:text>&amp;#x0D;&amp;#x0D;&amp;#x0D;</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text>&amp;#x04;</xsl:text>
	<xsl:value-of select="$crlf"/>

</xsl:template>

<!-- Template for tabellen -->
<xsl:template match="rapport">
	<xsl:choose>
		<xsl:when test="$tabelltype='ov'">
			<xsl:text>&amp;#154;ov2&amp;#159;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>&amp;#154;se3&amp;#159;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:value-of select="$crlf"/>
	<xsl:choose>
		<!-- K02 F04 -->
		<xsl:when test="$rapportnavn='K02' or $rapportnavn='F04'">
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] != 0]" mode="K02"/>
		</xsl:when>
		<!-- F05 -->
		<xsl:when test="$rapportnavn='F05'">
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] &lt; 3 and data[@navn='Partikategori'] != 0]" mode="F05"/>
		</xsl:when>
		<!-- F02 F03 K03 -->
		<xsl:when test="$tabelltype='se'">
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] != 0]" mode="se"/>
		</xsl:when>
		<!-- K05 K04 -->
		<xsl:otherwise>
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] &lt; 3 and data[@navn='Partikategori'] != 0]" mode="K05"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Template for kolonner i tabellen K02-->
<xsl:template match="liste" mode="K02">
	<!-- K02 F04 -->
	<xsl:text>&#160;</xsl:text>
	<xsl:value-of select="data[@navn='Partikode']"/>
	<xsl:text>&amp;#135;&amp;#132;</xsl:text>
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
	</xsl:if>
	<xsl:text>&amp;#134;</xsl:text>
	<xsl:choose>
		<xsl:when test="$rapportnavn='K02'">
			<xsl:value-of select="data[@navn='ProSt']"/>
			<xsl:text>&amp;#134;</xsl:text>
			<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
			<xsl:text>&amp;#134;</xsl:text>
			<xsl:value-of select="data[@navn='ProgAntMndt']"/>
			<xsl:if test="data[@navn='DiffProgAntMndt']!=''">
				<xsl:text> (</xsl:text>
				<xsl:value-of select="data[@navn='DiffProgAntMndt']"/>
				<xsl:text>)</xsl:text>
			</xsl:if>
			<xsl:text>&amp;#134;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<!-- F04 -->
			<xsl:choose>
				<xsl:when test="$prognose='yes'">
					<xsl:value-of select="data[@navn='ProgProSt']"/>
					<xsl:text>&amp;#134;</xsl:text>
					<xsl:value-of select="data[@navn='DiffProgPropFFtv']"/>
					<xsl:text>&amp;#134;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="data[@navn='ProSt']"/>
					<xsl:text>&amp;#134;</xsl:text>
					<xsl:value-of select="data[@navn='DiffPropFFtv']"/>
					<xsl:text>&amp;#134;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="data[@navn='ProgAntMndtFtv']"/>
			<xsl:if test="data[@navn='DiffProgAntMndtFFtv']!=''">
				<xsl:text> (</xsl:text>
				<xsl:value-of select="data[@navn='DiffProgAntMndtFFtv']"/>
				<xsl:text>)</xsl:text>
			</xsl:if>
			<xsl:text>&amp;#134;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for kolonner i tabellen F05-->
<xsl:template match="liste" mode="F05">
	<!-- F05 -->
	<xsl:text>&#160;</xsl:text>
	<xsl:value-of select="data[@navn='Partikode']"/>
	<xsl:text>&amp;#135;&amp;#132;</xsl:text>
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
	</xsl:if>
	<xsl:text>&amp;#134;</xsl:text>

	<xsl:choose>
		<xsl:when test="$prognose!='no'">
			<xsl:value-of select="data[@navn='ProgProSt']"/>
			<xsl:text>&amp;#134;</xsl:text>
			<xsl:value-of select="data[@navn='DiffProgPropFFtv']"/>
			<xsl:text>&amp;#134;</xsl:text>
		</xsl:when>			
		<xsl:otherwise>
			<xsl:value-of select="data[@navn='ProSt']"/>
			<xsl:text>&amp;#134;</xsl:text>
			<xsl:value-of select="data[@navn='DiffPropFFtv']"/>
			<xsl:text>&amp;#134;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:value-of select="data[@navn='ProgAntMndtStv']"/>
	<xsl:if test="data[@navn='DiffProgAntMndtStv']!=''">
		<xsl:text> (</xsl:text>
		<xsl:value-of select="data[@navn='DiffProgAntMndtStv']"/>
		<xsl:text>)</xsl:text>
	</xsl:if>
	<xsl:text>&amp;#134;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for kolonner i tabellen se-->
<xsl:template match="liste" mode="se">
	<!-- F02 F03 K03 -->
	<xsl:text>&#160;</xsl:text>
	<xsl:value-of select="data[@navn='Partikode']"/>
	<xsl:text>&amp;#135;&amp;#132;</xsl:text>
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
	</xsl:if>
	<xsl:text>&amp;#134;</xsl:text>
	<xsl:value-of select="data[@navn='ProSt']"/>
	<xsl:text>&amp;#134;</xsl:text>

	<xsl:choose>
		<xsl:when test="$rapportnavn='K03'">
			<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
			<xsl:text>&amp;#134;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="data[@navn='DiffPropFFtv']"/>
			<xsl:text>&amp;#134;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for kolonner i tabellen K05-->
<xsl:template match="liste" mode="K05">
	<!-- K05 K04-->
	<xsl:text>&#160;</xsl:text>
	<xsl:value-of select="data[@navn='Partikode']"/>
	<xsl:text>&amp;#135;&amp;#132;</xsl:text>
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
	</xsl:if>
	<xsl:text>&amp;#134;</xsl:text>
	<xsl:value-of select="data[@navn='ProSt']"/>
	<xsl:text>&amp;#134;</xsl:text>
	<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
	<xsl:text>&amp;#134;</xsl:text>
	<xsl:value-of select="data[@navn='DiffPropFStv']"/>
	<xsl:text>&amp;#134;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for rader i tabellen -->
<!--
<xsl:template match="liste">
	<xsl:text>&#160;</xsl:text>
	<xsl:apply-templates select="data[@navn='Partikode' or @navn='AntStemmer' or @navn='ProSt' or @navn='DiffPropFKsv' or @navn='DiffPropFFtv' or @navn='DiffPropFStv']"/>
	<xsl:value-of select="$crlf"/>
</xsl:template>
-->

<!-- Template for kolonner i tabellen -->
<!--
<xsl:template match="data">
	<xsl:if test="@navn!='DiffPropFStv' or $tabelltype='ov'">
		<xsl:choose>
			<xsl:when test="position() = 2">
				<xsl:value-of select="format-number(., '### ##0', 'no')"/>
				<xsl:text>&amp;#134;</xsl:text>
			</xsl:when>
			<xsl:when test="position() &gt; 2">
				<xsl:value-of select="."/>
				<xsl:text>&amp;#134;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
				<xsl:text>&amp;#135;&amp;#132;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
</xsl:template>
-->

<!-- Template for tabellhode i tabellen -->
<xsl:template name="table_header">
	<xsl:choose>
		<xsl:when test="$tabelltype='ov'">
			<xsl:text>&amp;#154;ov1&amp;#159;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>&amp;#154;se2&amp;#159;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:value-of select="$crlf"/>

	<xsl:text>&#160;</xsl:text>
	<xsl:text>Parti&amp;#135;&amp;#132;</xsl:text>
	<xsl:text>Stemmer&amp;#134;</xsl:text>
	<xsl:text>Andel %&amp;#134;</xsl:text>

	<xsl:choose>
		<xsl:when test="$rapportnavn='K05' or $rapportnavn='K04'">
			<xsl:text>03-99&amp;#134;</xsl:text>
			<xsl:text>03-01&amp;#134;</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn='F05'">
			<xsl:text>03-01&amp;#134;</xsl:text>
			<xsl:text>Mand. tenkt St.valg&amp;#134;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>03-99&amp;#134;</xsl:text>
			<xsl:if test="$tabelltype='ov'">
				<xsl:text>Mand.&amp;#134;</xsl:text>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>

	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sum">
	<xsl:choose>
		<xsl:when test="$tabelltype='ov'">
			<xsl:text>&amp;#154;ov3&amp;#159;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>&amp;#154;se4&amp;#159;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:value-of select="$crlf"/>
	
	<xsl:text>&#160;</xsl:text>
	<xsl:text>Sum&amp;#135;&amp;#132;</xsl:text>

	<!--<xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikode'] != 'Andre']/data[@navn='AntStemmer']), '### ##0', 'no')"/>-->
	<xsl:value-of select="format-number(/respons/rapport/data[@navn='AntFrammotte'], '### ##0', 'no')"/>

	<xsl:text>&amp;#134;</xsl:text>

	<xsl:text>&amp;#134;</xsl:text>
	<xsl:text>&amp;#134;</xsl:text>
	<xsl:if test="$tabelltype='ov'">
		<xsl:text>&amp;#134;</xsl:text>
	</xsl:if>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sumandre">
	<xsl:text>&#160;</xsl:text>
	<xsl:text>Andre&amp;#135;&amp;#132;</xsl:text>

	<xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikategori'] = '3']/data[@navn='AntStemmer']), '### ##0', 'no')"/>
	
	<xsl:text>&amp;#134;</xsl:text>

	<xsl:text>&amp;#134;</xsl:text>
	<xsl:text>&amp;#134;</xsl:text>
	<xsl:if test="$tabelltype='ov'">
		<xsl:text>&amp;#134;</xsl:text>
	</xsl:if>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<xsl:variable name="crlf">
	<!--<xsl:text>&#13;&#10;</xsl:text>-->
<xsl:text>{crlf}
</xsl:text>
</xsl:variable>

<!--
<xsl:variable name="tab">
	<xsl:text>&#9;</xsl:text>
</xsl:variable>
-->

<xsl:variable name="date">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
</xsl:variable>

<xsl:variable name="time">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegTid']"/>
</xsl:variable>

<xsl:variable name="dato">
	<xsl:value-of select="substring($date, 7, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 5, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 1, 4)"/>
</xsl:variable>

<xsl:variable name="tid">
	<xsl:value-of select="substring($time, 1, 5)"/>
</xsl:variable>

<xsl:variable name="nitfdate">
	<xsl:value-of select="$date"/>
	<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($time, 1, 2)"/>
	<xsl:value-of select="substring($time, 4, 2)"/>
	<xsl:value-of select="substring($time, 7, 2)"/>
</xsl:variable>

<xsl:variable name="rapportnavn">
	<xsl:value-of select="/respons/rapport/rapportnavn"/>
</xsl:variable>

<xsl:variable name="rapport">
	<xsl:value-of select="substring-after(/respons/rapport/rapportnavn, '0')"></xsl:value-of>
</xsl:variable>

<xsl:variable name="rapporttype">
	<xsl:choose>
		<xsl:when test="$rapport='2'">KOM</xsl:when>
		<xsl:when test="$rapport='3'">KRE</xsl:when>
		<xsl:otherwise>OVS</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:template name="iptc_date">
	<xsl:variable name="month">
		<xsl:value-of select="substring($nitfdate, 5, 2)"/>
	</xsl:variable>

	<xsl:value-of select="substring($nitfdate, 7, 2)"/>
	<xsl:value-of select="substring($nitfdate, 10, 4)"/>
	<xsl:text> </xsl:text>

	<xsl:choose>
		<xsl:when test="$month='01'">
			<xsl:text>JAN</xsl:text>
		</xsl:when>
		<xsl:when test="$month='02'">
			<xsl:text>FEB</xsl:text>
		</xsl:when>
		<xsl:when test="$month='03'">
			<xsl:text>MAR</xsl:text>
		</xsl:when>
		<xsl:when test="$month='04'">
			<xsl:text>APR</xsl:text>
		</xsl:when>
		<xsl:when test="$month='05'">
			<xsl:text>MAY</xsl:text>
		</xsl:when>
		<xsl:when test="$month='06'">
			<xsl:text>JUN</xsl:text>
		</xsl:when>
		<xsl:when test="$month='07'">
			<xsl:text>JUL</xsl:text>
		</xsl:when>
		<xsl:when test="$month='08'">
			<xsl:text>AUG</xsl:text>
		</xsl:when>
		<xsl:when test="$month='09'">
			<xsl:text>SEP</xsl:text>
		</xsl:when>
		<xsl:when test="$month='10'">
			<xsl:text>OCT</xsl:text>
		</xsl:when>
		<xsl:when test="$month='11'">
			<xsl:text>NOV</xsl:text>
		</xsl:when>
		<xsl:when test="$month='12'">
			<xsl:text>DEC</xsl:text>
		</xsl:when>
	</xsl:choose>
	<xsl:text> </xsl:text>
	<xsl:value-of select="substring($nitfdate, 3, 2)"/>
</xsl:template>

<xsl:variable name="urgency">
	<xsl:choose>
		<xsl:when test="$rapportnavn ='F05' or $rapportnavn ='F04'">4</xsl:when>
		<xsl:when test="$rapportnavn ='K05' or $rapportnavn ='K04'">6</xsl:when>
		<xsl:otherwise>6</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="kanal">
	<xsl:choose>
		<xsl:when test="substring($rapportnavn, 1, 1) = 'F'">A</xsl:when>
		<xsl:otherwise>C</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:template name="fipheader">
	<xsl:text>~</xsl:text>
		<xsl:value-of select="$crlf"/>
	<xsl:text>SU:VANTAOUT</xsl:text>
		<xsl:value-of select="$crlf"/>
	<xsl:text>SN:VANTAOUT</xsl:text>
		<xsl:value-of select="substring($nitfdate, 11, 5)"/>
		<xsl:value-of select="$crlf"/>
	<xsl:text>DB:VANTAOUT</xsl:text>
		<xsl:value-of select="$crlf"/>
	<xsl:text>SA:</xsl:text>
		<xsl:value-of select="$crlf"/>
	<xsl:text>PC:</xsl:text>
		<xsl:value-of select="substring($nitfdate , 5, 4)"/>
		<xsl:value-of select="substring($nitfdate , 10, 6)"/>
		<xsl:value-of select="$crlf"/>
	<xsl:text>DI:</xsl:text>
		<xsl:value-of select="$distribusjon"/>
		<xsl:value-of select="$crlf"/>
	<xsl:text>DU:vaftpa</xsl:text>
		<xsl:value-of select="$crlf"/>
	<xsl:text>ZN:SU.NTAOUT.DU.vaftpa.SN.NTAOUT</xsl:text>
		<xsl:value-of select="substring($nitfdate , 11, 5)"/>
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>
	<xsl:text>DQ:vaftpa</xsl:text>
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>
	<xsl:text>DU:vaftpa</xsl:text>
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>
		<xsl:value-of select="$crlf"/>
	<xsl:text>~</xsl:text>
		<xsl:value-of select="$crlf"/>

<!-- Fipheader exsempel:
~
SU:NTAOUT
SN:NTAOUT16254
DB:NTAOUT
SA:
PC:0871E0190D
DI:ALL
DU:satftpa
ZN:SU.NTAOUT.DU.satftpa.SN.NTAOUT16254

DQ:satftpa

DU:satftpa


~
-->
</xsl:template>

<xsl:variable name="fylke">
	<xsl:value-of select="/respons/rapport/data[@navn='FylkeNavn']"/>
</xsl:variable>

<xsl:variable name="kommune">
	<xsl:value-of select="/respons/rapport/data[@navn='KommNavn']"/>
</xsl:variable>

<xsl:variable name="kommunenr">
	<xsl:value-of select="/respons/rapport/data[@navn='KommNr']"/>
</xsl:variable>

<xsl:variable name="krets">
	<xsl:value-of select="/respons/rapport/data[@navn='KretsNavn']"/>
</xsl:variable>

<xsl:variable name="kretsnr">
	<xsl:value-of select="/respons/rapport/data[@navn='KretsNr']"/>
</xsl:variable>

<xsl:variable name="statusind">
	<xsl:value-of select="/respons/rapport/data[@navn='StatusInd']"/>
</xsl:variable>

<xsl:variable name="valgtype">
	<xsl:choose>
		<xsl:when test="substring-before($rapportnavn, '0') = 'F'">
			<xsl:text>Fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>Kommunestyrevalg</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="stedsnavn">
	<!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
	<xsl:choose>
		<xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
			<xsl:value-of select="$kommune"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
			<xsl:value-of select="$krets"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:otherwise>Landsoversikt</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="rapport-type">
	<!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
	<xsl:choose>
		<xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
			<!--<xsl:text>komm-</xsl:text>-->
			<xsl:value-of select="$kommunenr"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
			<!--<xsl:text>fylke-</xsl:text>-->
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
			<!--<xsl:text>krets-</xsl:text>-->
			<xsl:value-of select="$kommune"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="$krets"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='K05' or $rapportnavn='F05'">
			<xsl:text>landsoversikt</xsl:text>
		</xsl:when>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="tabelltype">
	<xsl:choose>
		<xsl:when test="$rapportnavn = 'K02'">
			<xsl:text>ov</xsl:text>
		</xsl:when>
		<xsl:when test="substring($rapportnavn, 2, 2) = '02' or substring($rapportnavn, 2, 2) = '03'">
			<xsl:text>se</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>ov</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="infolinje">
	<xsl:value-of select="$valgtype"/>
	<xsl:text>: </xsl:text>
	<xsl:choose>
		<!--
		<xsl:when test="$rapportnavn = 'F01' or $rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner</xsl:text>
		</xsl:when>
		-->
		<xsl:when test="$rapportnavn = 'F02' or $rapportnavn = 'K02'">
			<xsl:text></xsl:text>
			<xsl:value-of select="$fylke"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F03' or $rapportnavn = 'K03'">
			<xsl:value-of select="$kommune"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="$krets"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F04' or $rapportnavn = 'K04'">
			<xsl:text>Fylkesoversikt, </xsl:text>
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F05' or $rapportnavn = 'K05'">
			<xsl:text>Landsoversikt</xsl:text>
		</xsl:when>
		<!--
		<xsl:when test="$rapportnavn = 'F07' or $rapportnavn = 'K07'">
			<xsl:text>Landsoversikt pr. fylke</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo</xsl:text>
		</xsl:when>
		-->
		<xsl:when test="$rapportnavn = 'K09'">
			<xsl:text>Bystyreoversikt</xsl:text>
		</xsl:when>
	</xsl:choose>
	<xsl:text>, kl. </xsl:text>
	<xsl:value-of select="$tid"/>
	<xsl:if test="$statusind &gt; 5">
		<xsl:text>. Korrigert resultat</xsl:text>
	</xsl:if>
</xsl:variable>

<xsl:template match="rapport" mode="status">
	<!--
		435 av 435 kommuner (435 ferdig opptalt).€
		100,0 prosent av ant. stemmeberettigede.€
		2521781 opptalte stemmer.€
		Frammøte: 39,3 prosent.€
	-->
	<xsl:if test="data[@navn='TotAntKomm']">
		<xsl:text></xsl:text>
		<xsl:value-of select="data[@navn='AntKommFhstOpptalt'] + data[@navn='AntKommVtstOpptalt'] + data[@navn='AntKommAltOpptalt']"/>
		<xsl:text> av </xsl:text>
		<xsl:value-of select="data[@navn='TotAntKomm']"/>
		<xsl:text> kommuner (</xsl:text>
		<xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
		<xsl:text> ferdig opptalt).</xsl:text>
		<xsl:text>&amp;#x80;</xsl:text>
		<xsl:value-of select="$crlf"/>
	</xsl:if>

	<xsl:text></xsl:text>
	<xsl:text>Omfatter </xsl:text>
	<xsl:if test="data[@navn='AntStBerett']">
		<xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStBerett'] * 100), '0.0', 'pros')"/>
	</xsl:if>
	<xsl:if test="data[@navn='AntStberett']">
		<xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStberett'] * 100), '0.0', 'pros')"/>
	</xsl:if>
	<xsl:text> prosent av </xsl:text>
	<xsl:choose>
		<xsl:when test="data[@navn='AntStBerett']">
			<xsl:value-of select="format-number(data[@navn='AntStBerett'], '### ### ###', 'no')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="format-number(data[@navn='AntStberett'], '### ### ###', 'no')"/>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text> stemmeberettigede.</xsl:text>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text></xsl:text>
	<xsl:value-of select="format-number(data[@navn='AntFrammotte'], '### ### ###', 'no')"/>
	<xsl:text> opptalte stemmer.</xsl:text>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text></xsl:text>
	<xsl:text>Frammøte: </xsl:text>
	<xsl:value-of select="data[@navn='ProFrammotte']"/>
	<xsl:text> prosent.</xsl:text>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>

</xsl:template>

<xsl:template match="rapport" mode="status-bunn">
	<!--
		Antall st.ber.: 1234.€
		Frammøte: 39,3 prosent.€
		Alle forhåndsstemmer fordelt.
	-->

	<xsl:text></xsl:text>
	<xsl:text>Antall st.ber.: </xsl:text>
	<xsl:if test="data[@navn='AntStBerett']">
		<xsl:value-of select="data[@navn='AntStBerett']"/>
	</xsl:if>
	<xsl:if test="data[@navn='AntStberett']">
		<xsl:value-of select="data[@navn='AntStberett']"/>
	</xsl:if>
	<xsl:text>.</xsl:text>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text>Frammøte: </xsl:text>
	<xsl:value-of select="data[@navn='ProFrammotte']"/>
	<xsl:text> prosent.</xsl:text>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
	
	<xsl:choose>
		<!--
		0.	Ingen resultater innsendt
		1.	Bare foreløpige forhåndsstemmer innsendt
		* 2.	Bare endelige fhst. innsendt
		? 3.	Bare foreløpige valgtingsstemmer innsendt
		* 4.	Bare endelige vtst. innsendt
		5.	Foreløpige fhst. og foreløpige vtst. innsendt
		6.	Endelige fhst. og foreløpige vtst. innsendt
		7.	Foreløpige fhst. og endelige vtst. innsendt
		8.	Endelige fhst. og endelige vtst. innsendt
		
		Mulige tekster:
			11 forhåndsstemmer ikke fordelt.
			Alle forhåndsstemmer fordelt.
			Alt opptalt untatt noen forhåndsstemmer.
			Alt opptalt?
		-->
		
		<!--
		<xsl:when test="data[@navn='StatusInd'] = '1'">
			<xsl:text>Bare foreløpige forhåndsstemmer innsendt.</xsl:text>
		</xsl:when>
		<xsl:when test="data[@navn='StatusInd'] = '2'">
			<xsl:text>Bare endelige forhåndsstemmer innsendt.</xsl:text>
		</xsl:when>
		<xsl:when test="data[@navn='StatusInd'] = '3'">
			<xsl:text>Bare foreløpige valgtingsstemmer innsendt.</xsl:text>
		</xsl:when>
		<xsl:when test="data[@navn='StatusInd'] = '4'">
			<xsl:text>Bare endelige valgtingsstemmer innsendt.</xsl:text>
		</xsl:when>
		<xsl:when test="data[@navn='StatusInd'] = '5'">
			<xsl:text>Foreløpige forhåndsstemmer og foreløpige valgtingsstemmer innsendt.</xsl:text>
		</xsl:when>
		<xsl:when test="data[@navn='StatusInd'] = '6'">
			<xsl:text>Endelige forhåndsstemmer og foreløpige valgtingsstemmer innsendt.</xsl:text>
		</xsl:when>
		<xsl:when test="data[@navn='StatusInd'] = '7'">
			<xsl:text>Foreløpige forhåndsstemmer og endelige valgtingsstemmer innsendt.</xsl:text>
		</xsl:when>
		<xsl:when test="data[@navn='StatusInd'] = '8'">
			<xsl:text>Endelige forhåndsstemmer og endelige valgtingsstemmer innsendt.</xsl:text>
		</xsl:when>
		-->
				
		<xsl:when test="$statusind = '5'">
			<xsl:text>Foreløpig resultat.</xsl:text>
		</xsl:when>
		<xsl:when test="$statusind = '6'">
			<xsl:text>Alle forhåndsstemmer opptalt.</xsl:text>
		</xsl:when>
		<xsl:when test="$statusind = '7'">
			<xsl:text>Alt opptalt untatt enkelte forhåndsstemmer.</xsl:text>
		</xsl:when>
		<xsl:when test="$statusind = '8'">
			<xsl:text>Alt opptalt.</xsl:text>
		</xsl:when>
		<xsl:otherwise></xsl:otherwise>		
	</xsl:choose>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>

</xsl:template>

</xsl:stylesheet>