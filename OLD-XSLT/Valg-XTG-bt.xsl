<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:user="http://ntb.no/mynamespace">

<!-- Valget IPTC format -->

<xsl:output encoding="ISO-8859-1" method="text" omit-xml-declaration="yes" indent="yes"/>

<xsl:decimal-format name="no" decimal-separator="," grouping-separator="."/>
<xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>
<xsl:decimal-format name="pros" decimal-separator=","/>
<xsl:strip-space elements="*"/>

<xsl:param name="distribusjon">ALL</xsl:param>

<xsl:variable name="rapportnavn">
	<xsl:value-of select="/respons/rapport/rapportnavn"/>
</xsl:variable>

<xsl:param name="ntbdato">
	<xsl:value-of select="$dato"/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="substring($time, 1, 5)"/>
</xsl:param>

<!-- Main template: -->
<xsl:template match="respons">
	<xsl:text>@HEADER:BT:INN;;</xsl:text>
	<xsl:value-of select="$date"/>;<xsl:value-of select="$tid"/>
	<xsl:text>;</xsl:text>
	<!-- stikkord -->
	<xsl:text>VLG-</xsl:text>
	<xsl:value-of select="$rapportnavn"/>-<xsl:value-of select="$rapport-type"/>
	<xsl:value-of select="$crlf"/>

	<!-- Infolinje -->
<!--
	<xsl:text>@TIT1:</xsl:text>
	<xsl:value-of select="$infolinje"/>	
	<xsl:value-of select="$crlf"/>
-->
	
	<!-- Topptekster -->
	<xsl:value-of select="$tabelltype-bt"/>
	<xsl:choose>
		<xsl:when test="$rapport='4'">
			<xsl:value-of select="$fylke"/>
			<xsl:choose>
				<xsl:when test="$rapportnavn='F04' and (rapport/data[@navn='TotAntKomm'] - rapport/data[@navn='AntKommVtstOpptalt'] - rapport/data[@navn='AntKommAltOpptalt'])!=0">
					<xsl:text>, prognose</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="$rapport='5'">
			<xsl:text>Landsoversikt</xsl:text>
			<xsl:choose>
				<xsl:when test="$rapportnavn='F05' and (rapport/data[@navn='TotAntKomm'] - rapport/data[@navn='AntKommVtstOpptalt'] - rapport/data[@navn='AntKommAltOpptalt'])!=0">
					<xsl:text>, prognose</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="$rapport='2'">
			<xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:when test="$rapport='3'">
			<xsl:value-of select="$krets"/>
		</xsl:when>
		<xsl:otherwise>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text>, kl. </xsl:text>
	<xsl:value-of select="$tid"/>
	<xsl:value-of select="$crlf"/>

	<xsl:if test="$tabelltype='ov' and $rapportnavn!='K02'">
		<xsl:apply-templates select="rapport" mode="status"/>
	</xsl:if>

	<!-- Valg Tabellen -->
	<xsl:call-template name="table_header"/>
	<xsl:apply-templates select="rapport"/>
 	<xsl:if test="$tabelltype='ov' and $rapportnavn!='K02' and $rapportnavn!='F04'">
		<xsl:call-template name="sumandre"/>
	</xsl:if>		
	<xsl:call-template name="sum"/>

 	<xsl:if test="$tabelltype='se'">
		<xsl:apply-templates select="rapport" mode="status-bunn"/>
	</xsl:if>

<!--
	<xsl:if test="$rapportnavn='K02'">
		<xsl:apply-templates select="rapport" mode="status-bunn"/>
	</xsl:if>

	<xsl:text>@PRI:</xsl:text>
	<xsl:value-of select="$urgency"/>
	<xsl:value-of select="$crlf"/>

	<xsl:text>@KAT:385</xsl:text>
	<xsl:value-of select="$crlf"/>
-->

</xsl:template>

<!-- Template for tabellen -->
<xsl:template match="rapport">
	<xsl:choose>
		<!-- F04 -->
		<xsl:when test="$rapportnavn='F04'">
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] != 0]" mode="F04"/>
		</xsl:when>
		<!-- F05 -->
		<xsl:when test="$rapportnavn='F05'">
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] &lt; 3 and data[@navn='Partikategori'] != 0]" mode="F05"/>
		</xsl:when>
		<!-- F02 F03 K03 K02 -->
		<xsl:when test="$tabelltype='se'">
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] != 0]" mode="se"/>
		</xsl:when>
		<!-- K05 K04 -->
		<xsl:otherwise>
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] &lt; 3 and data[@navn='Partikategori'] != 0]" mode="K05"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Template for kolonner i tabellen F04-->
<xsl:template match="liste" mode="F04">
	<!-- F04 -->
	<xsl:text>@TAB</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>

	<xsl:value-of select="data[@navn='Partikode']"/>
	<xsl:value-of select="$tab"/>
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '###.##0', 'no')"/>
	</xsl:if>
	<xsl:value-of select="$tab"/>
	<xsl:choose>
		<xsl:when test="$rapportnavn='K02'">
			<xsl:value-of select="data[@navn='ProSt']"/>
			<xsl:value-of select="$tab"/>
			<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
			<xsl:value-of select="$tab"/>
			<xsl:value-of select="data[@navn='ProgAntMndt']"/>
			<xsl:if test="data[@navn='DiffProgAntMndt']!=''">
				<xsl:text> (</xsl:text>
				<xsl:value-of select="data[@navn='DiffProgAntMndt']"/>
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="data[@navn='ProgProSt']"/>
			<xsl:value-of select="$tab"/>
			<xsl:value-of select="data[@navn='DiffProgPropFFtv']"/>
			<xsl:value-of select="$tab"/>
			<xsl:value-of select="data[@navn='ProgAntMndtFtv']"/>
			<xsl:if test="data[@navn='DiffProgAntMndtFFtv']!=''">
				<xsl:text> (</xsl:text>
				<xsl:value-of select="data[@navn='DiffProgAntMndtFFtv']"/>
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for kolonner i tabellen F05-->
<xsl:template match="liste" mode="F05">
	<!-- F05 -->
	<xsl:text>@TAB</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>

	<xsl:value-of select="data[@navn='Partikode']"/>
	<xsl:value-of select="$tab"/>
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '###.##0', 'no')"/>
	</xsl:if>
	<xsl:value-of select="$tab"/>
	<xsl:value-of select="data[@navn='ProgProSt']"/>
	<xsl:value-of select="$tab"/>
	<xsl:value-of select="data[@navn='DiffProgPropFStv']"/>
	<xsl:value-of select="$tab"/>
	<xsl:value-of select="data[@navn='ProgAntMndtStv']"/>
	<xsl:if test="data[@navn='DiffProgAntMndtStv']!=''">
		<xsl:text> (</xsl:text>
		<xsl:value-of select="data[@navn='DiffProgAntMndtStv']"/>
		<xsl:text>)</xsl:text>
	</xsl:if>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for kolonner i tabellen se-->
<xsl:template match="liste" mode="se">
	<!-- F02 F03 K03 -->
	<xsl:text>@TAB</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>

	<xsl:value-of select="data[@navn='Partikode']"/>
	<xsl:value-of select="$tab"/>
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '###.##0', 'no')"/>
	</xsl:if>
	<xsl:value-of select="$tab"/>
	<xsl:value-of select="data[@navn='ProSt']"/>
	<xsl:value-of select="$tab"/>

	<xsl:choose>
		<xsl:when test="$rapportnavn='K03'">
			<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="data[@navn='DiffPropFFtv']"/>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for kolonner i tabellen K05-->
<xsl:template match="liste" mode="K05">
	<!-- K05 K04-->
	<xsl:text>@TAB</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>

	<xsl:value-of select="data[@navn='Partikode']"/>
	<xsl:value-of select="$tab"/>
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '###.##0', 'no')"/>
	</xsl:if>
	<xsl:value-of select="$tab"/>
	<xsl:value-of select="data[@navn='ProSt']"/>
	<xsl:value-of select="$tab"/>
	<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
	<xsl:value-of select="$tab"/>
	<xsl:value-of select="data[@navn='DiffPropFStv']"/>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for tabellhode i tabellen -->
<xsl:template name="table_header">
	<xsl:text>@TABHODE</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>

	<xsl:text>Parti</xsl:text>
	<xsl:value-of select="$tab"/>
	<xsl:text>Stemmer</xsl:text>
	<xsl:value-of select="$tab"/>
	<xsl:text>Andel %</xsl:text>
	<xsl:value-of select="$tab"/>

	<xsl:choose>
		<xsl:when test="$rapportnavn='K05' or $rapportnavn='K04'">
			<xsl:text>03-99</xsl:text>
			<xsl:value-of select="$tab"/>
			<xsl:text>03-01</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn='F05'">
			<xsl:text>03-01</xsl:text>
			<xsl:value-of select="$tab"/>
			<xsl:text>Mand. tenkt St.valg</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>03-99</xsl:text>
			<xsl:if test="$tabelltype='ov'">
				<xsl:value-of select="$tab"/>
				<xsl:text>Mand.</xsl:text>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>

	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sum">
	<xsl:text>@TABSUM</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>

	<xsl:text>Sum</xsl:text>
	<xsl:value-of select="$tab"/>

	<!--<xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikode'] != 'Andre']/data[@navn='AntStemmer']), '###.##0', 'no')"/>-->
	<xsl:value-of select="format-number(/respons/rapport/data[@navn='AntFrammotte'], '###.##0', 'no')"/>

	<xsl:value-of select="$tab"/>
	<xsl:value-of select="$tab"/>
	<xsl:if test="$tabelltype='ov'">
		<xsl:value-of select="$tab"/>
	</xsl:if>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sumandre">
	<xsl:text>@TAB</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>

	<xsl:text>Andre</xsl:text>
	<xsl:value-of select="$tab"/>

	<xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikategori'] = '3']/data[@navn='AntStemmer']), '###.##0', 'no')"/>
	
	<xsl:value-of select="$tab"/>
	<xsl:value-of select="$tab"/>
	<xsl:if test="$tabelltype='ov'">
		<xsl:value-of select="$tab"/>
	</xsl:if>

	<xsl:value-of select="$crlf"/>
</xsl:template>

<xsl:variable name="crlf">
	<xsl:text>&#13;&#10;</xsl:text>
</xsl:variable>

<xsl:variable name="tab">
	<xsl:text>&#9;</xsl:text>
</xsl:variable>

<xsl:variable name="date">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
</xsl:variable>

<xsl:variable name="time">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegTid']"/>
</xsl:variable>

<xsl:variable name="dato">
	<xsl:value-of select="substring($date, 7, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 5, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 1, 4)"/>
</xsl:variable>

<xsl:variable name="tid">
	<xsl:value-of select="substring($time, 1, 5)"/>
</xsl:variable>

<xsl:variable name="nitfdate">
	<xsl:value-of select="$date"/>
	<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($time, 1, 2)"/>
	<xsl:value-of select="substring($time, 4, 2)"/>
	<xsl:value-of select="substring($time, 7, 2)"/>
</xsl:variable>

<xsl:variable name="rapport">
	<xsl:value-of select="substring-after(/respons/rapport/rapportnavn, '0')"></xsl:value-of>
</xsl:variable>

<xsl:variable name="rapporttype">
	<xsl:choose>
		<xsl:when test="$rapport='2'">KOM</xsl:when>
		<xsl:when test="$rapport='3'">KRE</xsl:when>
		<xsl:otherwise>OVS</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="urgency">
	<xsl:choose>
		<xsl:when test="$rapport ='5' or $rapport ='4'">4</xsl:when>
		<xsl:otherwise>6</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="fylke">
	<xsl:value-of select="/respons/rapport/data[@navn='FylkeNavn']"/>
</xsl:variable>

<xsl:variable name="kommune">
	<xsl:value-of select="/respons/rapport/data[@navn='KommNavn']"/>
</xsl:variable>

<xsl:variable name="kommunenr">
	<xsl:value-of select="/respons/rapport/data[@navn='KommNr']"/>
</xsl:variable>

<xsl:variable name="krets">
	<xsl:value-of select="/respons/rapport/data[@navn='KretsNavn']"/>
</xsl:variable>

<xsl:variable name="kretsnr">
	<xsl:value-of select="/respons/rapport/data[@navn='KretsNr']"/>
</xsl:variable>

<xsl:variable name="statusind">
	<xsl:value-of select="/respons/rapport/data[@navn='StatusInd']"/>
</xsl:variable>

<xsl:variable name="valgtype">
	<xsl:choose>
		<xsl:when test="substring-before($rapportnavn, '0') = 'F'">
			<xsl:text>Fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>Kommunestyrevalg</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="stedsnavn">
	<!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
	<xsl:choose>
		<xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
			<xsl:value-of select="$kommune"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
			<xsl:value-of select="$krets"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:otherwise>Landsoversikt</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="rapport-type">
	<!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
	<xsl:choose>
		<xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
			<!--<xsl:text>komm-</xsl:text>-->
			<xsl:value-of select="$kommunenr"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
			<!--<xsl:text>fylke-</xsl:text>-->
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
			<!--<xsl:text>krets-</xsl:text>-->
			<xsl:value-of select="$kommune"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="$krets"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='K05' or $rapportnavn='F05'">
			<xsl:text>landsoversikt</xsl:text>
		</xsl:when>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="tabelltype">
	<xsl:choose>
	<!--
		<xsl:when test="$rapportnavn = 'K02'">
			<xsl:text>ov</xsl:text>
		</xsl:when>
	-->
		<xsl:when test="substring($rapportnavn, 2, 2) = '02' or substring($rapportnavn, 2, 2) = '03'">
			<xsl:text>se</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>ov</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="infolinje">
	<xsl:value-of select="$valgtype"/>
	<xsl:text>: </xsl:text>
	<xsl:choose>
		<!--
		<xsl:when test="$rapportnavn = 'F01' or $rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner</xsl:text>
		</xsl:when>
		-->
		<xsl:when test="$rapportnavn = 'F02' or $rapportnavn = 'K02'">
			<xsl:text></xsl:text>
			<xsl:value-of select="$fylke"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F03' or $rapportnavn = 'K03'">
			<xsl:value-of select="$kommune"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="$krets"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F04' or $rapportnavn = 'K04'">
			<xsl:text>Fylkesoversikt, </xsl:text>
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F05' or $rapportnavn = 'K05'">
			<xsl:text>Landsoversikt</xsl:text>
		</xsl:when>
		<!--
		<xsl:when test="$rapportnavn = 'F07' or $rapportnavn = 'K07'">
			<xsl:text>Landsoversikt pr. fylke</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo</xsl:text>
		</xsl:when>
		-->
		<xsl:when test="$rapportnavn = 'K09'">
			<xsl:text>Bystyreoversikt</xsl:text>
		</xsl:when>
	</xsl:choose>
	<xsl:text>, kl. </xsl:text>
	<xsl:value-of select="$tid"/>
	<xsl:if test="$statusind &gt; 5">
		<xsl:text>. Korrigert resultat</xsl:text>
	</xsl:if>
</xsl:variable>

<xsl:template match="rapport" mode="status">
	<!--
		435 av 435 kommuner (435 ferdig opptalt).€
		100,0 prosent av ant. stemmeberettigede.€
		2521781 opptalte stemmer.€
		Frammøte: 39,3 prosent.€
	-->
	<xsl:if test="data[@navn='TotAntKomm']">
		<xsl:text>@TXT</xsl:text>
		<xsl:value-of select="$kolonner"/>
		<xsl:text>:</xsl:text>
		<xsl:value-of select="data[@navn='AntKommFhstOpptalt'] + data[@navn='AntKommVtstOpptalt'] + data[@navn='AntKommAltOpptalt']"/>
		<xsl:text> av </xsl:text>
		<xsl:value-of select="data[@navn='TotAntKomm']"/>
		<xsl:text> kommuner (</xsl:text>
		<xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
		<xsl:text> ferdig opptalt).</xsl:text>
		<xsl:value-of select="$crlf"/>
	</xsl:if>

	<xsl:text>@TXT</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>
	<xsl:text>Omfatter </xsl:text>
	<xsl:if test="data[@navn='AntStBerett']">
		<xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStBerett'] * 100), '0,0', 'pros')"/>
	</xsl:if>
	<xsl:if test="data[@navn='AntStberett']">
		<xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStberett'] * 100), '0,0', 'pros')"/>
	</xsl:if>
	<xsl:text> prosent av </xsl:text>
	<xsl:choose>
		<xsl:when test="data[@navn='AntStBerett']">
			<xsl:value-of select="format-number(data[@navn='AntStBerett'], '###.###.###', 'no')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="format-number(data[@navn='AntStberett'], '###.###.###', 'no')"/>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text> stemmeberettigede.</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text>@TXT</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>
	<xsl:value-of select="format-number(data[@navn='AntFrammotte'], '###.###.###', 'no')"/>
	<xsl:text> opptalte stemmer.</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text>@TXT</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>
	<xsl:text>Frammøte: </xsl:text>
	<xsl:value-of select="data[@navn='ProFrammotte']"/>
	<xsl:text> prosent.</xsl:text>
	<xsl:value-of select="$crlf"/>

</xsl:template>

<xsl:template match="rapport" mode="status-bunn">
	<!--
		Antall st.ber.: 1234.€
		Frammøte: 39,3 prosent.€
		Alle forhåndsstemmer fordelt.
	-->

	<xsl:text>@TXT</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>
	<xsl:text>Antall st.ber.: </xsl:text>
	<xsl:if test="data[@navn='AntStBerett']">
		<xsl:value-of select="data[@navn='AntStBerett']"/>
	</xsl:if>
	<xsl:if test="data[@navn='AntStberett']">
		<xsl:value-of select="data[@navn='AntStberett']"/>
	</xsl:if>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text>@TXT</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>
	<xsl:text>Frammøte: </xsl:text>
	<xsl:value-of select="data[@navn='ProFrammotte']"/>
	<xsl:text> prosent.</xsl:text>
	<xsl:value-of select="$crlf"/>
	
	<xsl:text>@TXT</xsl:text>
	<xsl:value-of select="$kolonner"/>
	<xsl:text>:</xsl:text>
	<xsl:choose>
		<xsl:when test="$statusind = '5'">
			<xsl:text>Foreløpig resultat.</xsl:text>
		</xsl:when>
		<xsl:when test="$statusind = '6'">
			<xsl:text>Alle forhåndsstemmer opptalt.</xsl:text>
		</xsl:when>
		<xsl:when test="$statusind = '7'">
			<xsl:text>Alt opptalt untatt enkelte forhåndsstemmer.</xsl:text>
		</xsl:when>
		<xsl:when test="$statusind = '8'">
			<xsl:text>Alt opptalt.</xsl:text>
		</xsl:when>
		<xsl:otherwise></xsl:otherwise>		
	</xsl:choose>
	<xsl:value-of select="$crlf"/>

</xsl:template>

<xsl:variable name="tabelltype-bt">
	<xsl:choose>
		<xsl:when test="$tabelltype = 'ov'">
			<xsl:text>@STED5</xsl:text>
			<xsl:value-of select="substring($rapportnavn, 1, 1)"/>
			<xsl:text>:</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>@STED4</xsl:text>
			<xsl:value-of select="substring($rapportnavn, 1, 1)"/>
			<xsl:text>:</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="kolonner">
	<xsl:choose>
		<xsl:when test="$tabelltype = 'ov'">
			<xsl:text>5</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>4</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

</xsl:stylesheet>