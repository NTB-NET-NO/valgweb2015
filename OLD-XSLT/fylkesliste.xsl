<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="no"/>

<xsl:param name="FylkeNr">00</xsl:param>
<xsl:variable name="Form">Default.aspx</xsl:variable>
<xsl:param name="Expand"></xsl:param>

<xsl:template match="respons">
<table width="100%">
<tr><td class='status'>Fylkesoversikt</td><td align="right" class='status'>Samleoversikt</td></tr>
<xsl:apply-templates select="rapport"/>
</table>
</xsl:template>

<!--
<xsl:template match="rapport/liste/data[@navn='FylkeNavn']">

<xsl:template match="rapport/data[@navn='FylkeNavn']">
-->
<xsl:template match="rapport">
<tr><td>
	<a>
	<xsl:attribute name="href">
		<xsl:value-of select="$Form"/>
    <xsl:text>?FylkeNr=</xsl:text>
    <xsl:value-of select="data[@navn='FylkeNr']"/>
    <xsl:text>&amp;VisningIndeks=3</xsl:text>
    <xsl:if test="data[@navn='FylkeNr']=$FylkeNr and $Expand=''">
			<xsl:text>&amp;Expand=-1</xsl:text>
		</xsl:if>
		<!--
		<xsl:if test="data[@navn='FylkeNr']!=$FylkeNr">
			<xsl:value-of select="data[@navn='FylkeNr']"/>
		</xsl:if>
		-->

		<xsl:text>&amp;Navn=</xsl:text>
		<xsl:value-of select="data[@navn='FylkeNavn']"/>
	</xsl:attribute>
	<xsl:value-of select="data[@navn='FylkeNavn']"/>
	</a>
  </td>
  
	<td class="status" align="right" title="Klikk for fylkesoversikt pr. kommune! (Antall kommuner i fylket: fhst. opptalt + vtst. opptalt + alt opptalt)">
	<a>
	<xsl:attribute name="href">
		<xsl:value-of select="$Form"/>
		<xsl:text>?OversiktFylkeNr=</xsl:text>
		<xsl:value-of select="data[@navn='FylkeNr']"/>		
	</xsl:attribute>
	<xsl:value-of select="data[@navn='TotAntKomm']"/>
	<xsl:text>:</xsl:text>
	<xsl:value-of select="data[@navn='AntKommFhstOpptalt']"/>
	<xsl:text>+</xsl:text>
	<xsl:value-of select="data[@navn='AntKommVtstOpptalt']"/>
	<xsl:text>+</xsl:text>
	<xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
	</a>
</td>
</tr>

  
  
  <xsl:if test="data[@navn='FylkeNr']=$FylkeNr and $Expand=''">
    
    <tr>
      <td colspan="2">
        <table width="100%">
          <tr>
            <td colspan="2" align="left">
              <a>
              <xsl:attribute name="href">
                <xsl:value-of select="$Form"/>
                <xsl:text>?FylkeNr=</xsl:text>
                <xsl:value-of select="$FylkeNr"/>
                <xsl:text>&amp;VisningIndeks=3</xsl:text>
                <xsl:text>&amp;Alle=1</xsl:text>
                 <xsl:text>&amp;Navn=</xsl:text>
                <xsl:value-of select="data[@navn='FylkeNavn']"/>
              </xsl:attribute>
              - Alle
            </a>
             </td>
            <td width="20"></td>

          </tr>
            <xsl:choose>
              <xsl:when test="$FylkeNr=03">
                <xsl:call-template name="oslo"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:apply-templates select="tabell[@navn='ST04tabell3']/liste">
                  <xsl:sort select="data[@navn='KommNavn']"></xsl:sort>
                </xsl:apply-templates>
              </xsl:otherwise>
            </xsl:choose>
  </table>
	</td>
</tr>
  

</xsl:if>	
</xsl:template>


  <xsl:template match="tabell/liste">
    <tr>
	<td width="20">
    
  </td>
	<td>
		<a>
      
		<xsl:attribute name="href">
			<xsl:value-of select="$Form"/>
			<xsl:text>?FylkeNr=</xsl:text>
			<xsl:value-of select="$FylkeNr"/>
      <xsl:text>&amp;VisningIndeks=3</xsl:text>
      <xsl:text>&amp;KommuneNr=</xsl:text>
			<xsl:value-of select="data[@navn='KommNr']"/>
			<xsl:text>&amp;Navn=</xsl:text>
			<xsl:value-of select="data[@navn='KommNavn']"/>
		</xsl:attribute>
			<xsl:value-of select="data[@navn='KommNavn']"/>
		</a>
	</td>
	<td class="status" align="center">
    <xsl:if test="data[@navn='StatusInd'] != ''">
			(<xsl:value-of select="data[@navn='StatusInd']"/>)
    </xsl:if>
    <xsl:if test="data[@navn='StatusInd'] = ''">(-)</xsl:if>
  </td>
	</tr>
</xsl:template>

<xsl:template name="oslo">
	<tr>
	<td width="20"></td>
	<td>
		<a>
		<xsl:attribute name="href">
			<xsl:value-of select="$Form"/>
			<xsl:text>?FylkeNr=03</xsl:text>
      <xsl:text>&amp;VisningIndeks=3</xsl:text>
      <xsl:text>&amp;KommuneNr=0301</xsl:text>
			<xsl:text>&amp;Navn=Oslo</xsl:text>
		</xsl:attribute>
		<xsl:text>Oslo kommune</xsl:text>
		</a>
	</td>
	<td class="status">
			<!--(<xsl:value-of select="data[@navn='StatusInd']"/>)-->
	</td></tr>
</xsl:template>


</xsl:stylesheet>